<?php 
/******************************************************
 * File Name	: db.class.php
 *******************************************************/	
	include("includes/config.php");

	#FUNCTION TO CLEAR INPUT VARIABLES 
	function cleanVar($input)  {
	   //$input = mysql_real_escape_string($input);
		if(!is_array($input))
		{
		$input = htmlspecialchars($input, ENT_IGNORE, 'utf-8');
	   	$input = strip_tags($input);
	   	$input = stripslashes($input);
	   	$input = preg_replace("/<script.*?\/script>/s", "", $input);	
		}
	   return $input;
	}
	function removeSpecial($string){
		$newString = preg_replace("/[^a-zA-Z]/", "", $string);
		return $newString;
	}
#class DB extends PDO Class
class DB extends PDO
{
	private $dbname   	= 	WEBSITE_DB_DATABASE;
	private $hostname 	= 	DB_HOST;
	private $username 	=	DB_USERNAME;
    private $password 	=	DB_PASSWORD;
	private $connection = 	NULL;
	
    #make a connection
    public function __construct($DB = '') {
    	if($DB)
			$this->dbname = $DB;
		$this->engine = 'mysql';
        $dns = $this->engine.':dbname='.$this->dbname.";host=".$this->hostname; 
		parent::__construct( $dns, $this->username, $this->password );
        try 
        { 
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
        }
        catch (PDOException $e) 
        {
            die($e->getMessage());
        }
    }
	
	
	#select Functions
	
	public function add_order($order_no, $order_date, $order_by_id, $lat, $long, $shop_id, $shop_order_status,$super_stockist_id,$dist_id) {
		$sth = $this->prepare("INSERT INTO `tbl_orders` 
		(`order_no`, `ordered_by`, `order_date`, `shop_id`, `superstockistid`,`distributorid`,
		`total_items`, `total_cost`, `lat`, `long`, `shop_order_status`)
		VALUES (:order_no, :ordered_by, :order_date, :shop_id, :superstockistid, :distributorid, 
		:total_items, :total_cost, :lat, :long, :shop_order_status)");

		$sth->bindParam('order_no', $order_no);
		$sth->bindParam('ordered_by', $order_by_id);
		$sth->bindParam('order_date', $order_date);
		$sth->bindParam('shop_id', $shop_id);
		$sth->bindParam('superstockistid', $super_stockist_id);
		$sth->bindParam('distributorid', $dist_id);
		$sth->bindParam('total_items', $total_items);
		$sth->bindParam('total_cost', $total_cost);
		$sth->bindParam('lat', $lat);
		$sth->bindParam('long', $long);
		$sth->bindParam('shop_order_status', $shop_order_status);
		
		$sth->execute();
		return $this->lastInsertId();
	}
	public function add_order_details($order_product_details) {
		$fields = "";
		$values = "";
		if(isset($order_product_details['producthsn']) && $order_product_details['producthsn'] != '')
		{			
			$fields.= ", producthsn";
			$values.= ", :producthsn";			
		}
		if(isset($order_product_details['product_variant_weight2']) && $order_product_details['product_variant_weight2'] != '')
		{			
			$fields.= ", product_variant_weight2";
			$values.= ", :product_variant_weight2";			
		}
		if(isset($order_product_details['product_variant_unit2']) && $order_product_details['product_variant_unit2'] != '')
		{			
			$fields.= ", product_variant_unit2";
			$values.= ", :product_variant_unit2";			
		}
		if(isset($order_product_details['campaign_applied']) && $order_product_details['campaign_applied'] == 0)
		{			
			$fields.= ", campaign_applied, campaign_type, campaign_sale_type";
			$values.= ", :campaign_applied, :campaign_type, :campaign_sale_type";			
		}
		
		$sth = $this->prepare("INSERT INTO `tbl_order_details` 
		( `order_id`, `brand_id`, `cat_id`, `product_id`, `product_variant_id`, 
		`product_quantity`, `product_variant_weight1`, `product_variant_unit1`, 
		`product_unit_cost`, `product_total_cost`, `product_cgst`, 
		`product_sgst`, `p_cost_cgst_sgst`, `order_status`
		$fields) 
		VALUES (:order_id, :brand_id, :cat_id, :product_id , :product_variant_id, 
		:product_quantity, :product_variant_weight1, :product_variant_unit1, 
		:product_unit_cost, :product_total_cost, :product_cgst, 
		:product_sgst, :p_cost_cgst_sgst, :order_status $values)");
		/*echo "INSERT INTO `tbl_order_details` 
		( `order_id`, `brand_id`, `cat_id`, `product_id`, `product_variant_id`, 
		`product_quantity`, `product_variant_weight`, `product_variant_unit`, 
		`product_unit_cost`, `product_total_cost`, `product_cgst`, 
		`product_sgst`, `p_cost_cgst_sgst`, `order_status`
		$fields) 
		VALUES (".$order_product_details['order_record_id'].", ".$order_product_details['brand_id'].", ".
		$order_product_details['cat_id'].", ".$order_product_details['product_id'] .", ".$order_product_details['product_variant_id'].", ".
		$order_product_details['product_quantity'].", ". $order_product_details['product_variant_weight'].", ".
		$order_product_details['product_variant_unit'].", ".
		$order_product_details['product_unit_cost'].", ". $order_product_details['product_total_cost'].", ".
		$order_product_details['product_cgst'].", ". 
		$order_product_details['product_sgst'].", ". $order_product_details['p_cost_cgst_sgst'].", ".
		$order_product_details['order_status'] .$values.")";*/
		$sth->bindParam('order_id', $order_product_details['order_record_id']);
		$sth->bindParam('brand_id', $order_product_details['brand_id']);		
		$sth->bindParam('cat_id', $order_product_details['cat_id']);
		$sth->bindParam('product_id', $order_product_details['product_id']);		
		$sth->bindParam('product_variant_id', $order_product_details['product_variant_id']);
		$sth->bindParam('product_quantity', $order_product_details['product_quantity']);
		$sth->bindParam('product_variant_weight1', $order_product_details['product_variant_weight1']);
		$sth->bindParam('product_variant_unit1', $order_product_details['product_variant_unit1']);
		$sth->bindParam('product_unit_cost', $order_product_details['product_unit_cost']);
		$sth->bindParam('product_total_cost', $order_product_details['product_total_cost']);
		$sth->bindParam('product_cgst', $order_product_details['product_cgst']);
        $sth->bindParam('product_sgst', $order_product_details['product_sgst']);
		$sth->bindParam('p_cost_cgst_sgst', $order_product_details['p_cost_cgst_sgst']);
		$sth->bindParam('order_status', $order_product_details['order_status']);
		
		if(isset($order_product_details['producthsn']) && $order_product_details['producthsn'] != '')
		{	
			$sth->bindParam('producthsn', $order_product_details['producthsn']);	
		}
		if(isset($order_product_details['product_variant_weight2']) && $order_product_details['product_variant_weight2'] != '')
		{			
			$sth->bindParam('product_variant_weight2', $order_product_details['product_variant_weight2']);			
		}
		if(isset($order_product_details['product_variant_unit2']) && $order_product_details['product_variant_unit2'] != '')
		{			
			$sth->bindParam('product_variant_unit2', $order_product_details['product_variant_unit2']);			
		}
		if(isset($order_product_details['campaign_applied']) && $order_product_details['campaign_applied'] == 0)
		{
			$sth->bindParam('campaign_applied', $order_product_details['campaign_applied']);
			$sth->bindParam('campaign_type', $order_product_details['campaign_type']);
			$sth->bindParam('campaign_sale_type', $order_product_details['campaign_sale_type']);
		}
		
		$sth->execute();
		
		return $this->lastInsertId();
	}
	public function add_varient($orderappid, $variantweight, $weightquantity, $unit, $variantsize, $variantunit, $shopid, $shopnme, $orderid, $totalcost,$product_varient_id, $productid, $productnm, $campaign_applied=null, $campaign_type=null, $campaign_sale_type=null) {
		$fields = "";
		$values = "";
		
		if($product_varient_id=="") {
			$product_varient_id=0;
		}
		
		
		if($campaign_applied == 'yes')
		{			
			$fields.= ", campaign_type, campaign_sale_type";
			$values.= ", :campaign_type, :campaign_sale_type";
			
		}
		
		$sth = $this->prepare("INSERT INTO `tbl_variant_order` 
			( `orderappid`, `variantweight`, weightquantity, unit, `variantsize`, `variantunit`, `shopid`, `shopnme`, `orderid`, `totalcost`, `productid`, product_varient_id, `productnm`,`campaign_applied` $fields) 
			VALUES (:orderappid, :variantweight, :weightquantity, :unit , :variantsize, :variantunit, :shopid, :shopnme, :orderid, :totalcost, :productid, :product_varient_id, :productnm, :campaign_applied $values)");
		$sth->bindParam('orderappid', $orderappid);
		$sth->bindParam('variantweight', $variantweight);
		
		$sth->bindParam('weightquantity', $weightquantity);
		$sth->bindParam('unit', $unit);
		
		$sth->bindParam('variantsize', $variantsize);
		$sth->bindParam('variantunit', $variantunit);
		$sth->bindParam('shopid', $shopid);
		$sth->bindParam('shopnme', $shopnme);
		$sth->bindParam('orderid', $orderid);
		$sth->bindParam('totalcost', $totalcost);
		$sth->bindParam('productid', $productid);
        $sth->bindParam('productnm', $productnm);
		$sth->bindParam('product_varient_id', $product_varient_id);
				
		if($campaign_applied == 'yes')
		{
			$campaign_applied = '0';
			$sth->bindParam('campaign_applied', $campaign_applied);
			$sth->bindParam('campaign_type', $campaign_type);
			$sth->bindParam('campaign_sale_type', $campaign_sale_type);
		}
		else{
			$campaign_applied = '1';
			$sth->bindParam('campaign_applied', $campaign_applied);
		}
		
		$sth->execute();
		
		return $this->lastInsertId();
	}
	
	function fncheckssstockist($id){
		$sthsstockist = $this->prepare("SELECT external_id FROM tbl_user WHERE id = :id");
		$sthsstockist->bindParam('id', $id);
		$sthsstockist->execute();
		$resultss = $sthsstockist->fetch(PDO::FETCH_ASSOC);
		$superstockistid=$resultss['external_id'];

		//superstockist name
		$sthssname = $this->prepare("SELECT firstname FROM tbl_user WHERE id=".$resultss['external_id']."");
		$sthssname->execute();
		$resultsname = $sthssname->fetch(PDO::FETCH_ASSOC);
		$superstockistnm=$resultsname['firstname'];

		$sths = $this->prepare("SELECT external_id FROM tbl_user WHERE id=".$resultss['external_id']."");
		$sths->execute();
		$results = $sths->fetch(PDO::FETCH_ASSOC);
		$distributorid=$results['external_id'];

		$sthsname = $this->prepare("SELECT firstname FROM tbl_user WHERE id=".$results['external_id']."");
		$sthsname->execute();
		$resultsnm = $sthsname->fetch(PDO::FETCH_ASSOC);
		$distributornm=$resultsnm['firstname'];

		echo $superstockistid."--".$superstockistnm."--".$distributorid."--".$distributornm;
		exit;
	}
	function add_order_c_p_discount($campaign_id,$order_variant_id,$discount_amount,$discount_percent,$actual_amount)
	{
		$sth = $this->prepare("INSERT INTO `tbl_order_cp_discount` 
			( `campaign_id`, `order_variant_id`,  `discount_amount` ,  `discount_percent` , `actual_amount`) 
			VALUES (:campaign_id, :order_variant_id, :discount_amount, :discount_percent , :actual_amount)");
		$sth->bindParam('campaign_id', $campaign_id);
		$sth->bindParam('order_variant_id', $order_variant_id);		
		$sth->bindParam('discount_amount', $discount_amount);
		$sth->bindParam('discount_percent', $discount_percent);		
		$sth->bindParam('actual_amount', $actual_amount);
		
		$sth->execute();
		return $this->lastInsertId();
	}
	public function update_order($order_id, $total_items , $total_cost, $total_order_gst_cost,$offer_provided) {
		$sth = $this->prepare("UPDATE `tbl_orders` SET `total_items` = :total_items , 
		`total_cost` = :total_cost, `total_order_gst_cost`= :total_order_gst_cost, `offer_provided` = :offer_provided WHERE `id` = :order_id");
		$sth->bindParam('order_id', $order_id);
		$sth->bindParam('total_items', $total_items );
		$sth->bindParam('total_cost', $total_cost);
		$sth->bindParam('total_order_gst_cost', $total_order_gst_cost);
		$sth->bindParam('offer_provided', $offer_provided);
		$sth->execute();
		return $this->lastInsertId();
	}
	function add_order_c_n_f_product($campaign_id,$c_p_order_variant_id,$f_p_order_variant_id)
	{
		$sth = $this->prepare("INSERT INTO `tbl_order_c_n_f_product` 
			( `campaign_id`, `c_p_order_variant_id`,  `f_p_order_variant_id`) 
			VALUES (:campaign_id, :c_p_order_variant_id, :f_p_order_variant_id)");
		$sth->bindParam('campaign_id', $campaign_id);
		$sth->bindParam('c_p_order_variant_id', $c_p_order_variant_id);		
		$sth->bindParam('f_p_order_variant_id', $f_p_order_variant_id);
				
		$sth->execute();
		return $this->lastInsertId();
	}
	function add_order_purchase_p_weight($campaign_id,$order_variant_id,$total_wt_purchase,$total_wt_purchase_unit)
	{
		$sth = $this->prepare("INSERT INTO `tbl_order_purchase_p_weight` 
			( `campaign_id`, `order_variant_id`,  `total_wt_purchase`,`total_wt_purchase_unit`) 
			VALUES (:campaign_id, :order_variant_id, :total_wt_purchase, :total_wt_purchase_unit)");
		$sth->bindParam('campaign_id', $campaign_id);
		$sth->bindParam('order_variant_id', $order_variant_id);		
		$sth->bindParam('total_wt_purchase', $total_wt_purchase);
		$sth->bindParam('total_wt_purchase_unit', $total_wt_purchase_unit);
				
		$sth->execute();
		return $this->lastInsertId();
	}
}
?>