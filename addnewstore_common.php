<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors',1);
for($i=0;$i<5;$i++){
	if($i==0){
		include 'db.php';
		$con = new Connection();
	}else{
		include 'db'.$i.'.php';
		$str='Connection'.$i;
		$con = new $str();		
	}
	require_once('wsJSON.php');
	$JSONVar = new wsJSON($con);
	$salespersonid	= $_POST['userid'];
	$store_name		= fnEncodeString($_POST['store_name']);
	$owner_name		= fnEncodeString($_POST['owner_name']);
	$contact_no		= $_POST['contact_no'];
	$store_Address	= fnEncodeString($_POST['store_Address']);
	$lat			= $_POST['lat'];
	$lng			= $_POST['lon'];
	$city			= $_POST['city'];
	$state			= $_POST['state'];
	$shop_type_id	= $_POST['shop_type_id'];

	$distributorid	= $_POST['distributorid'];
	$suburbid		= $_POST['suburbid'];
	$subarea_id		= $_POST['subareaid'];

	$jsonOutput 	= $JSONVar->fnAddNewStorecommon($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$shop_type_id,$distributorid,$suburbid,$subarea_id);
}
echo $jsonOutput;