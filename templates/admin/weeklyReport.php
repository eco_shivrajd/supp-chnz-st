<?php
//ini_set("display_errors", "1"); error_reporting(E_ALL);

include ("../../includes/config.php");
extract($_POST);
$arrWeeklyOption  = explode("::",$drpWeeklyOption);
$arrWeeklyOption[1] = date('d-m-Y', strtotime($arrWeeklyOption[1] . ' +1 day'));

$maxDays=7;
$StartDate = $arrWeeklyOption[0];
$MonthName = date('F', strtotime($StartDate . ' +0 day'));
$endDate = date('d-m-Y', strtotime($arrWeeklyOption[1] . ' -1 day'));


function getProductData($con, $counter , $dateToShow , $StartDate, $endDate, $arrProductInfo,$arrPOST) 
{	
	extract($arrPOST);
	$returnString = "";
	$AllCat_totalquantity = 0;
	$AllCat_totalcost = 0;

	for($i=0;$i<$counter;$i++)
	{
		$sql = "SELECT o.order_date,o.superstockistid,o.distributorid,o.total_order_gst_cost,od.product_id,
			s.name,od.product_quantity FROM	tbl_orders o 
			LEFT JOIN tbl_order_details od ON o.id = od.order_id 
			LEFT JOIN tbl_shops s ON s.id= o.shop_id where date_format(o.order_date, '%d-%m-%Y') BETWEEN '".$StartDate."' AND '".$endDate."'";
        $sql1 = "SELECT o.order_date,o.total_order_gst_cost,od.product_id,
		s.name,od.product_quantity FROM tbl_orders o 
		LEFT JOIN tbl_order_details od ON o.id = od.order_id 
		LEFT JOIN tbl_shops s ON s.id= o.shop_id
        where date_format(o.order_date, '%d-%m-%Y') = '".$dateToShow."' ";         
//	while ($rowrw = mysqli_fetch_array ($result1)){
 // echo "<pre>";
 //   print_r($rowrw);
  //}		//LEFT JOIN tbl_shops shops ON shops.id= od.shopid		
		$condition = " AND od.cat_id = " . $arrProductInfo[$i]['catid'];
		if($dropdownSalesPerson!="")
		{
			$condition .= " AND o.ordered_by = " . $dropdownSalesPerson;
		}
		if($cmbSuperStockist!="")
		{
			$condition .= " AND o.superstockistid = " . $cmbSuperStockist;
			//echo $condition;
		} 
		if($dropdownStockist !="")
		{
			$condition .= " AND o.distributorid = " . $dropdownStockist;
		} 		
		if($dropdownshops !="")
		{
			$condition .= " AND o.shop_id = " . $dropdownshops;
		}		
		if($dropdownbrands  !="")
		{
			$condition .= " AND od.brand_id = " . $dropdownbrands;
		} 		
		if($dropdownProducts  !="")
		{
			$condition .= " AND od.product_id = " . $dropdownProducts;
		}		
		if($dropdownSuburbs !="")
		{
			$condition .= " AND s.suburbid = " . $dropdownSuburbs;
		}		
		if($dropdownCity !="")
		{
			$condition .= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$condition .= " AND s.state = " . $dropdownState;
		}
        $sql1 .= $condition;
		$sql .= $condition;
		  $result1 = mysqli_query($con,$sql); 
           $result11 = mysqli_query($con,$sql1); 
		//$sql .= "  group by od.product_variant_weight1 ";
		//print_r($sql);		
		$Cat_totalquantity = 0; $Cat_totalcost = 0;
		while($row = mysqli_fetch_array($result11))
		{


			$Cat_totalcost = floatval( $Cat_totalcost) + floatval($row["total_order_gst_cost"]);

			$AllCat_totalcost = floatval($AllCat_totalcost) + floatval($row["total_order_gst_cost"]);
			
			if(SALESREPORT_TYPE=="KG") {
				$totalquantity = floatval($row["product_quantity"]);

				if($row["product_variant_unit1"]=="g" || $row["product_variant_unit1"]=="gm") {
					$totalquantity = $totalquantity / 1000;
				}
				$Cat_totalquantity = floatval($Cat_totalquantity) + floatval($totalquantity);
				
				$AllCat_totalquantity = floatval($AllCat_totalquantity) + floatval($totalquantity);			
			}
			else 
			{
				$totalquantity = $row["product_quantity"];
				$Cat_totalquantity = $Cat_totalquantity + $totalquantity;
				$AllCat_totalquantity = $AllCat_totalquantity + $totalquantity;
			}
		}
		
		$returnString .= "<td style='text-align:right;'>". $Cat_totalquantity . "</td>";
		$arrReturn["Cat_totalquantity"][$i] = floatval($Cat_totalquantity);
		$arrReturn["Cat_totalcost"][$i] = floatval($Cat_totalcost);
	}
	$returnString .= "<th style='text-align:right;'>". floatval($AllCat_totalquantity) . "</th>"; 
	$arrReturn["returnString"] = $returnString;	
	return $arrReturn;


}
?>
<?php if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
	
.fa {
    height: 18px;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    transform: translate(0, 0);
}
</style>
<?php } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
	<?php 
	   if($_GET["actionType"]!="excel") 
		{ 
			?>
		<div class="caption"><i class="icon-puzzle"></i>Weekly Sales Report</div>		
		<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
		&nbsp;
		<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		<?php } ?>
	</div>


	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
		
			<?php

			if($dropdownshops !="")
			{
				$sql=" SELECT  name from tbl_shops where id =  $dropdownshops";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$MarketName = $row["name"];
			} else {
				$MarketName = "ALL";
			}
			
			if($dropdownStockist !="")
			{
				$sql="SELECT firstname FROM tbl_user where id='".$dropdownStockist."' order by firstname";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$DistributorName = fnStringToHTML($row["firstname"]);
			} else {
				$DistributorName = "ALL";
			}
			
			if($dropdownSalesPerson !="")
			{
				$sql="SELECT firstname FROM tbl_user where id='".$dropdownSalesPerson."' order by firstname";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SalesPersonName = fnStringToHTML($row["firstname"]);
			} else {
				$SalesPersonName = "ALL";
			}
			
			if($dropdownSuburbs !="")
			{
				$sql="SELECT suburbnm FROM tbl_surb WHERE id=$dropdownSuburbs ";
				$result1 = mysqli_query($con,$sql);
				$row = mysqli_fetch_array($result1);
				$SuburbName = fnStringToHTML($row["suburbnm"]);
			} else {
				$SuburbName = "ALL";
			} 
			
			$sql=" SELECT 
				tbl_brand.name, 
				tbl_brand.type, 
				tbl_brand.id, 
				tbl_category.categorynm,
				tbl_product.catid,
				count(tbl_category.brandid) as total_categories, 
				count(tbl_product.catid) as total_products 
			FROM 
				tbl_category 
				LEFT JOIN tbl_brand on tbl_brand.id = tbl_category.brandid 
				LEFT JOIN tbl_product on tbl_product.catid = tbl_category.id  ";
				
			$condition = " Where 1=1 ";
			if($dropdownbrands  !="")
			{
				$condition .= " AND tbl_category.brandid = " . $dropdownbrands;
			} 
			if($dropdownCategory  !="")
			{
				$condition .= " AND tbl_product.catid = " . $dropdownCategory;
			}
			if($dropdownProducts  !="")
			{
				$condition .= " AND tbl_product.id = " . $dropdownProducts;
			}
			
			$sql .= $condition;
			
			$sql .= " GROUP By categorynm having total_products > 0  ORDER BY  tbl_brand.type,  tbl_brand.name,  tbl_category.categorynm";
			
			$result1 = mysqli_query($con,$sql);

			$type = ""; 
			$i=0;
			$counter = 0;
			
			$totalColumn=mysqli_num_rows($result1);
			
			$totalColumn += 4;
			$firstColumn = round($totalColumn/2);
			$SecondColumn = $totalColumn - $firstColumn;
			$SecondAColumn = round($SecondColumn/2);
			$SecondBColumn = $SecondColumn - $SecondAColumn;
			
			?>
			
			<table class="table table-striped table-hover table-bordered" width="100%">
				<thead>
				<tr>
					<th colspan="<?php echo $firstColumn;?>">Regional Head: <?php echo $SalesPersonName;?></th>
					<th colspan="<?php echo $SecondColumn;?>">From Date: <?php echo $StartDate;?> To Date: <?php echo $endDate;?> </th>
				</tr>
				<tr>
					<th colspan="<?php echo $firstColumn;?>">Distributor Name: <?php echo $DistributorName;?></th>
					<th colspan="<?php echo $SecondAColumn;?>">Region: <?php echo $SuburbName;?></th>
					<th colspan="<?php echo $SecondBColumn;?>">Month: <?php echo $MonthName;?></th>
				</tr>
				 
				 
					<tr>
						<th>Date</th>
						<th>Day</th>
						<th>SHOP</th>
						<?php
						while($row = mysqli_fetch_array($result1))
						{
							if($type != $row["type"] && $i!=0) {
							?><th style="text-align:center;" colspan="<?php echo $i;?>"><?php echo $type;?></th><?php	
							$i=0;
							}
							$type = $row["type"];
							$arrProductInfo[$counter]['brand'] = fnStringToHTML($row["name"]);
							$arrProductInfo[$counter]['category'] = fnStringToHTML($row["categorynm"]);
							$arrProductInfo[$counter]['catid'] = $row["catid"];
							$i++;
							$counter++;
						}
						?>
						<?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?php echo $arrProductInfo[$i]['brand'];?></th><?php } ?>
						<th>&nbsp;</th>
					</tr>
					
				
                     <tr>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<?php
							for($i=0;$i<$counter;$i++)
							{ ?><th style="text-align:center;"><?php echo $arrProductInfo[$i]['category'];?></th><?php } ?>
						<th>Total(Pcs)</th>
					</tr>

								
					
				</thead>
				<tbody>
					<?php 
					for($i=0;$i<$maxDays;$i++) { ?>
					<tr>
						<?php
						 $dateToShow =  date('d-m-Y', strtotime($StartDate . ' +'.$i.' day')); 
						 ?>
						<td><?php echo $dateToShow;?></td>
						<td><?php echo date('D', strtotime($StartDate . ' +'.$i.' day'));?></td>
						<td><?php echo $MarketName;?></td>
						<?php 
						$arrReturn =  getProductData($con, $counter , $dateToShow, $StartDate, $endDate , $arrProductInfo, $_POST);
							echo $arrReturn["returnString"];
							$AllarrReturn[$i] = $arrReturn;
						?>
					</tr>
					<?php } ?> 
					<tr>
						<td>&nbsp;</td>
						<th>Total(Pcs)</th>
						<td>&nbsp;</td>
						<?php
						$OverAllTotal = 0;
						for($i=0;$i<$counter;$i++)
						{ ?><th style="text-align:right;">
							<?php
							$catSum = 0;
							for($j=0;$j<$maxDays;$j++) {
								$catSum = $catSum + $AllarrReturn[$j]["Cat_totalquantity"][$i];
							}
							$OverAllTotal = $OverAllTotal + $catSum;
							echo $catSum;
							?>
							</th><?php
						} ?>
						<th style="text-align:right;"><?php echo $OverAllTotal;?></th>
					</tr>
					
					<tr>
						<td>&nbsp;</td>
						<th>VALUE <?php if($_GET["actionType"]=="excel") { ?>RS<?php }else{?>₹<?php }?></th>
						<td>&nbsp;</td>
						<?php
						$OverAllTotal = 0;
						for($i=0;$i<$counter;$i++)
						{ ?><th style="text-align:right;">
							<?php
							$catSum = 0;
							for($j=0;$j<$maxDays;$j++) {
								$catSum = $catSum + $AllarrReturn[$j]["Cat_totalcost"][$i];
							}
							$OverAllTotal = $OverAllTotal + $catSum;
							echo $catSum;
							?>
							</th><?php 
						} ?>
						<th style="text-align:right;"><?php echo $OverAllTotal;?></th>
					</tr>
				</tbody>
			</table>
		</div>
	</div>


</div> 
<?php
if($_GET["actionType"]=="excel") {
	$exportFileName  = str_replace("::","-",$drpWeeklyOption);
	header( "Content-Type: application/vnd.ms-excel");
	header( "Content-disposition: attachment; filename=Report_".$exportFileName.".xls");
}
?>