<!-- BEGIN HEADER -->
<?php 
//phpinfo();die();
include "../includes/grid_header.php";

include "../includes/userManage.php";	
include "../includes/shopManage.php";
include "../includes/productManage.php";
include "../includes/orderManage.php";
$userObj 	= 	new userManager($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$prodObj 	= 	new productManage($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
?>
<!-- END HEADER -->
<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>

</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "ShopOrders";	
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->

	<div class="page-content-wrapper">
		<div class="page-content">			
			<h3 class="page-title">
			Orders
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Orders</a>
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Order Received
							</div>
                            <div class="clearfix"></div>
						</div>						
						<div class="portlet-body">

							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							<div class="form-group">
							  <label class="col-md-3">Order Status:</label>
							  <div class="col-md-4">
							  <select name="order_status" id="order_status"  data-parsley-trigger="change" class="form-control">
								<option value="">-Select-</option>
								<option value="1" selected>Received</option>								
								<option value="2">Assigned for Delivery</option>
								<option value="3">Assigned to Transport office </option>								
								<option value="4">Delivered</option>
								<option value="6">Payment Received</option>
								<option value="8">Partial Payment Received</option>								
								</select>
							  </div>
							</div><!-- /.form-group -->	
												
							<div class="form-group" id="divDaily">
								<label class="col-md-3">Select Order Date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->	
					
													<?php if($_SESSION[SESSION_PREFIX."user_type"]=="Admin") { ?>
							<div class="form-group">
								<label class="col-md-3">Superstockist:</label>
								<div class="col-md-4">
								
									<select name="cmbSuperStockist" id="cmbSuperStockist" onchange="fnShowStockist(this)"
									class="form-control">
									<option value="">-Select-</option>
									<?php
									$user_type="Superstockist";
									$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' order by firstname";
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										$cat_id=$row['id'];
										echo "<option value='$cat_id'>" . fnStringToHTML($row['firstname']) . "</option>";
									} ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<?php } else if($_SESSION[SESSION_PREFIX."user_type"]=="Superstockist") {  ?>
							
							<input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="<?=$_SESSION[SESSION_PREFIX."user_id"];?>">
							<?php } else { ?>
							<input type="hidden" name="cmbSuperStockist" id="cmbSuperStockist" value="">
							<input type="hidden" name="dropdownStockist" id="dropdownStockist" value="<?=$_SESSION[SESSION_PREFIX."user_id"];?>"> 
							<?php } ?>
							
							<?php if($_SESSION[SESSION_PREFIX."user_type"]!="Distributor") { ?>
							<div class="form-group">
							
								<label class="col-md-3">Stockist:</label>
								<div class="col-md-4" id="divStocklistDropdown">
								 <select name="dropdownStockist" id="dropdownStockist" class="form-control" onchange="fnShowSalesperson(this)">
									<option value="">-Select-</option>
									<?php
									$user_type="Distributor";
									switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
											$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' order by firstname";									
										break;
										case "Superstockist":
											$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' AND external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
										break;
										case "Distributor":
											//$condn=" AND o.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
										break;
									}
									$result1 = mysqli_query($con,$sql);
									while($row = mysqli_fetch_array($result1))
									{
										$assign_id=$row['id'];
										echo "<option value='$assign_id' >" . fnStringToHTML($row['firstname']) . "</option>";
									}
									?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<?php } ?>
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<div class="col-md-4" id="divsalespersonDropdown">
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php
										
										/*$sql = "SELECT city,state,suburb_ids, subarea_ids, city_ids, state_ids 
										FROM tbl_user left JOIN tbl_user_working_area ON tbl_user.id = tbl_user_working_area.user_id WHERE id = '".$_SESSION[SESSION_PREFIX."user_id"]."'";

										$result1 	= mysqli_query($con,$sql);
										$row 		= mysqli_fetch_array($result1);
										$suburb_ids 	= $row["suburb_ids"];
										$subarea_ids 	= $row["subarea_ids"];
										$city 		= $row["city"];
										$state 		= $row["state"];

										if($city=='0') {
										  $city='';
										}
										if($state=='0') {
										  $state='';
										} */
										
										$user_type="SalesPerson";
										switch($_SESSION[SESSION_PREFIX.'user_type']){
										case "Admin":
												$sql="SELECT DISTINCT(u.firstname),u.id FROM `tbl_user` as u INNER JOIN tbl_orders as o ON o.order_by= u.id where u.user_type ='$user_type' order by firstname";									
											break;
											case "Superstockist":
											
												/*if($city=="") 
													$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' order by firstname";
												else 
													$sql =	"SELECT firstname,id FROM tbl_user where user_type ='$user_type' AND city='".$city."' ORDER BY firstname";*/
												$sql="SELECT DISTINCT(u.firstname),u.id FROM `tbl_user` as u INNER JOIN tbl_orders as o ON o.order_by= u.id where u.user_type ='$user_type' AND o.superstockistid='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
											break;
											case "Distributor":
												
												/*if($city=="") 
													$sql="SELECT firstname,id FROM `tbl_user` where user_type ='$user_type' AND external_id='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
												else 
													$sql =	"SELECT firstname,id FROM tbl_user where user_type ='$user_type' AND city='".$city."' ORDER BY firstname";*/
												$sql="SELECT DISTINCT(u.firstname),u.id FROM `tbl_user` as u INNER JOIN tbl_orders as o ON o.order_by= u.id where u.user_type ='$user_type' AND o.distributorid='".$_SESSION[SESSION_PREFIX."user_id"]."' order by firstname";
											break;
										} 
										//echo $sql;
										$result1 = mysqli_query($con,$sql);
										while($row = mysqli_fetch_array($result1))
										{
											$assign_id=$row['id'];
											echo "<option value='$assign_id'>" . fnStringToHTML($row['firstname']) . "</option>";
										}?>
									</select>
								</div>
							</div><!-- /.form-group -->
						<!-- 	<div class="form-group">
								<label class="col-md-3">State</label>
								<div class="col-md-4">
								<?php
									$sql_state="SELECT * FROM tbl_state where country_id=101";					
									$result_state = mysqli_query($con,$sql_state);		
								?>
									<select id="dropdownState" name="dropdownState" size="1" class="form-control" onChange="fnShowCity(this.value)">
									<option value="">-Select-</option>
									<?php
									while($row_state = mysqli_fetch_array($result_state))
									{?>
										<option value=<?=$row_state['id'];?>><?=fnStringToHTML($row_state['name']);?></option>
									<?php }	?>												
									</select>
								</div>
							</div>
							<div class="form-group" id="city_div">
								<label class="col-md-3">City</label>
								<div class="col-md-4" id="div_select_city">
									<select id="citySelect" size="1" class="form-control">
									<option value="">-Select-</option>									
									</select>
								</div>
							</div>
							 <div class="form-group" id="area_div">
								<label class="col-md-3">Region</label>
								<div class="col-md-4" id="div_select_area">
								 <select class="form-control"  id="subarea">
									<option value="">-Select-</option>									
								 </select>
								 </div>
							</div>  -->

							 <div class="form-group">
				<label class="col-md-3">State:</label>
				<div class="col-md-4">
				<select name="dropdownState" id="dropdownState"              
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please select state"
				class="form-control" onChange="fnShowCity(this.value)">
				<option selected disabled>-select-</option>
				<?php
				$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					echo "<option value='$cat_id'>" . $row['name'] . "</option>";
				} ?>
				</select>
				</div>
				</div>			
				<div class="form-group" id="city_div" style="display:none;">
				  <label class="col-md-3">City:</label>
				  <div class="col-md-4" id="div_select_city">
				  <select name="dropdownCity" id="dropdownCity" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>										
					</select>
				  </div>
				</div><!-- /.form-group -->
				<div class="form-group" id="area_div" style="display:none;">
				  <label class="col-md-3">Region:</label>
				  <div class="col-md-4" id="div_select_area">
				  <select name="area" id="area" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div><!-- /.form-group --> 						
				<!-- <div class="form-group" id="subarea_div" style="display:none;">
				  <label class="col-md-3">Subarea:</label>
				  <div class="col-md-4" id="div_select_subarea">
				  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div> -->
				<!-- /.form-group --> 
							<div class="form-group" id="shop_div">
								<label class="col-md-3">Shops:</label>
								<div class="col-md-4">
								<?php $shop_result = $shopObj->getAllShops(); ?>									
								 <select name="divShopdropdown" id="divShopdropdown" class="form-control">
									<option value="">-Select-</option>
									<?php while($row_shop = mysqli_fetch_assoc($shop_result))
									{ ?>									
									<option value="<?=$row_shop['id'];?>"><?=$row_shop['name'];?></option>
									<?php } ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">Category:</label>
								<div class="col-md-4" id="divCategoryDropDown">
								<?php $cat_result = $prodObj->getAllCategory(); ?>
								 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
									<option value="">-Select-</option>
									<?php while($row_cat = mysqli_fetch_assoc($cat_result))
									{ ?>									
									<option value="<?=$row_cat['id'];?>"><?=$row_cat['categorynm'];?></option>
									<?php } ?>								
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">Product:</label>
								<div class="col-md-4" id="divProductdropdown">
								<?php $prod_result = $prodObj->getAllProducts(); ?>
								 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
									<option value="">-Select-</option>
									<?php while($row_prod = mysqli_fetch_assoc($prod_result))
									{ ?>									
									<option value="<?=$row_prod['id'];?>"><?=$row_prod['productname'];?></option>
									<?php } ?>	
								</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>	

<br>

			<form class="form-horizontal" role="form" name="form" method="post" action="">	
							<div id="order_list">
						<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr id="main_th">
							<th width="20%">Order Date</th>
							<th width="25%">Shop Name</th>
							<th width="25%">Orders</th>
							<th width="10%"> Quantity</th>
							<th width="10%">Unit Cost</th>
							<th width="10%">Total Cost</th>							
							</tr>
						</thead>
						<tbody>	
							<?php
							$order_status = 1;//received;
							$result1 = $orderObj->getOrders_shopwise($order_status);//print"<pre>";print_R($orders);
							$order_count = count($result1);
							//echo $sql;
														
									while($row1 = mysqli_fetch_array($result1)) 
									{ 
										$newarr['order_date']=$row1['order_date'];
										$newarr['shopnm']=$row1['shopnm'];
										$newarr['order_no']=$row1['order_no'];
										$order_noss=$row1['order_no'];
										$newarr['product_quantity']=$row1['product_quantity'];
										$newarr['p_cost_cgst_sgst']=$row1['p_cost_cgst_sgst'];
										$newarr1[]=$newarr;										
									}
									
									$out = array();
									foreach ($newarr1 as $key => $value)
									{
										$countval=0;
										foreach ($value as $key2 => $value2){
											if($key2=='order_no'){
												 $index = $key2.'-'.$value2;
												if (array_key_exists($index, $out)){
													$out[$index]++;
												} else {
													$out[$index] = 1;
												}
											}
											
										}
									}
									$out1 = array_values($out);$j=0;$temp=$newarr1[0]["order_no"];$sum=0;
									
									for($i=0;$i<count($newarr1);$i++){
										$total=$newarr1[$i]['product_quantity']*$newarr1[$i]['p_cost_cgst_sgst'];	?>
										
										<tr class="odd gradeX">
											<?php if($temp==$newarr1[$i]["order_no"]){ ?>											
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['order_date'];?></td>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['shopnm'];?></td>

										<td rowspan='<?php echo $out1[$j];?>'>
											<a onclick="showInvoice(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)">Show Invoice</a></br>
                                            <?php echo $newarr1[$i]['order_no'];?></br>
											<!-- <a onclick="showInvoice1(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)">
											Generate PDF</a> -->
											<a href="invoicedemo2.php?order_id=<?php echo $newarr1[$i]["order_no"];?>" title="PDF Generator">View PDF</a>
											</a>
										</td>
										<?php  
										     $sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["order_no"]; $j++; 
											} 
											else
											{
											?>
											 <td style="display: none;"><?php echo $newarr1[$i]['order_date'];?></td> 
											 <td style="display: none;"><?php echo $newarr1[$i]['shopnm'];?></td> 
											  <td style="display: none;"><a onclick="showInvoice(<?php echo "'".$newarr1[$i]["order_no"]."'";?>)"><?php echo $newarr1[$i]['order_no'];?></a></td>
											<?php } ?>
										<td align="right"><?php echo $newarr1[$i]['product_quantity'];?></td>
										<td align="right"><?php echo $newarr1[$i]['p_cost_cgst_sgst'];?></td>
										<td align="right"><?php echo $total;?></td>
										</tr>
									<?php } ?>	
							
						</tbody>
						</table>
                        </form>
						</div>
						</div>
					
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<div class="modal fade" id="view_invoice" role="dialog">
	<div class="modal-dialog" style="width: 980px !important;">    
		<!-- Modal content-->
		<div class="modal-content" id="view_invoice_content">      
		</div>      
	</div>
</div>
<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
$(document).ready(function() {
	$("#select_th").removeAttr("class");
	$("#main_th th").removeAttr("class");
	if ( $.fn.dataTable.isDataTable( '#sample_2' ) ) {
    table = $('#sample_2').DataTable();
	table.destroy();
		table = $('#sample_2').DataTable( {
			"aaSorting": [],
		} );
	}
});
$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
function fnReset() {
	location.reload();
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();		
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
	CallAJAX(url,"div_select_area");
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();	
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";
	CallAJAX(url,"div_select_subarea");
		
}
function FnGetShopsDropdown(id) {
	$("#shop_div").show(); 	
	$("#divShopdropdown").html('<option value="">-Select-</option>');
	var param = "";
	var state_id = $("#dropdownState").val();
	var city_id = $("#city").val();
	var suburb_id = $("#area").val();
	if(state_id != '')
		param = param + "&state_id="+state_id ;
	if(city_id != '')
		param = param + "&city_id="+city_id ;
	if(suburb_id != ''){
		if(suburb_id != undefined){
			param = param + "&suburb_id="+suburb_id ;
		}
	}
	if(id != ''){
		if(id != undefined)
			param = param + "&suburb_id="+id.value ;
	}
	
	var url = "getShopDropdownByAddress.php?param=param"+param;
	CallAJAX(url,"divShopdropdown");
}
function fnShowProducts(id) {
	var url = "getProductDropdown.php?cat_id="+id.value;
	CallAJAX(url,"divProductdropdown");
}
function fnShowStockist(id) { 	
	var url = "getStockistDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divStocklistDropdown");	
	document.getElementById("divsalespersonDropdown").innerHTML = "<select name='dropdownSalesPerson' id='dropdownSalesPerson' class='form-control'><option value=''>-Select-</option></select>";
}
function fnShowSalesperson(id) {
	var url = "getSalesPersonDropDown.php?cat_id="+id.value;
	CallAJAX(url,"divsalespersonDropdown");	 
}



function ShowReport() {	
	
	var order_status = $('#order_status').val();
	//alert(order_status);
	if(order_status!='1'){
		$("#statusbtn").hide();
	}else{
		$("#statusbtn").show();
	}
	if(order_status!='3'){
		$("#statusDbtn").hide();
	}else{
		$("#statusDbtn").show();
	}
	var url = "ajax_show_orders_shopwise.php"; 	
	var data = $('#frmsearch').serialize();
		//alert(order_status);
   // console.log(data);	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {
	    console.log(response);		 
		$('#order_list').html(response);
		var table = $('#sample_2').dataTable();      
		table.fnFilter('');
		$("#select_th").removeAttr("class");
	}).fail(function () { });
	return false;
}

function showInvoice(id) {	
	var url = "invoice.php"; 
	//alert(url);

    jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_id='+id,
		async: false
	}).done(function (response) {
		//console.log(response);
		//showInvoice1(id,response);
		$('#view_invoice_content').html(response);
		$('#view_invoice').modal('show');
	}).fail(function () { });
	return false;
}

function showInvoice1(id) {
		//console.log(response);

	var url = "invoicedemo2.php"; 
    jQuery.ajax({
		url: url,
		/*method: 'GET',
		data: 'oids='+id,*/
		method: 'POST',
		data: 'order_id='+id,
		async: false
	}).done(function (response) 
	{
		//console.log(response);
	 alert("PDF Generated Successfully..!!");
	}).fail(function () { });
	return false;
}
/* function showInvoice1(id,response) {
		//console.log(response);
	var url = "invoicedemo3.php"; 
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_id='+id+'&htmlcode='+response,
		async: false
	}).done(function (response) {
		console.log(response);
	}).fail(function () { });
	return false;
} */
/*function showInvoice1(id,response) {
		//console.log(response);
	var url = "invoicedemo2.php"; 
    jQuery.ajax({
		url: url,
		method: 'GET',
		data: 'oids='+id,
		async: false
	}).done(function (response) {
		//console.log(response);
	}).fail(function () { });
	return false;
}*/

function takeprint_invoice() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\
	.darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
	.fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
	.fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
	.font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
	.font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
	.table-bordered-popup {    border: 1px solid #364622;}\
	.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
		border: 1px solid #364622;	color:#4a5036;}\
	.blue{	color:#010057;}\
	.blue1{	color:#574960;	font-size:16px;}\
	.buyer_section{	color:#574960;	font-size:14px;}\
	.pad-5{	padding-left:10px;}\
	.pad-40{	padding-left:40px;}\
	.np{	padding-left:0px;	padding-right:0px;}\
	.bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
	 background-size: 200px 200px;}\
	</style>' + $("#divPrintArea").html();
	if(isIE == true){
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>