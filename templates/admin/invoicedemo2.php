<?php
ini_set('display_errors', 1); 
error_reporting(E_ALL);
include ("../../includes/config.php");
include ("../includes/common.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
include "../includes/shopManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
//$order_id = $_GET['oids'];
$order_id = $_POST['order_id'];
if ($order_id=='') 
{
	$order_id = $_GET['order_id'];
}
/*echo $order_id;
die();*/
$order_details = $orderObj->getShopOrdersbyorderid($order_id);
$admin_details_basic = $userObj->getLocalUserDetails($_SESSION[SESSION_PREFIX.'user_id']);
$admin_details = $userObj->getAdminDetails($_SESSION[SESSION_PREFIX.'user_id']);
 $shop_details="";
 if(!empty($order_details)&&(count($order_details)>0)){
	$shop_details = $shopObj->getShopDetails($order_details[0]['shop_id']);
}  
//echo "<pre>";print_r($shop_details);
//echo "<pre>";print_r($order_details);die();
$colspan3=3;
$colspan2=2;
$html=
"<style>
.darkgreen{
	background-color:#364622; color:#fff!important; font-size:24px;font-weight:600;
}
.fentgreen1{
	background-color:#b0b29c;
	color:#4a5036;
	font-size:12px;
}
.fentgreen{
	background-color:#b0b29c;
	color:#4a5036;
}
.font-big{
	font-size:20px;
	font-weight:600;
	color:#364622;
}
.font-big1{
	font-size:14px;
	font-weight:600;
	color:#364622;
}
.table-bordered-popup {
    border: 1px solid #364622;
}
.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {
    border: 1px solid #364622;
	color:#4a5036;
}
.blue{
	color:#010057;
}
.blue1{
	color:#574960;
	font-size:16px;
}
.buyer_section{
	color:#574960;
	font-size:14px;
}
.pad-5{
	padding-left:10px;
}
.pad-40{
	padding-left:40px;
}
.np{
	padding-left:0px;
	padding-right:0px;
}
.bg
{
	background-image:url(../../assets/global/img/watermark1.png); background-repeat:no-repeat;
	 background-size: 50px 50px;
}
.col-md-8 {
    width: 66.66666667%;
}
</style>
<div id='contentpdf'>
   <table class='table table-bordered-popup'>
				<tbody>
				<tr>
				<td colspan='4' width='70%' class='darkgreen' valign='top'><img src='../../assets/global/img/logo-fh-invoice.jpg' style='width:60px;'> SRI JAYA SHREE FOOD PRODUCTS</td>
				<td colspan='3' class='font-big text-center' valign='top'>Tax Invoice</td>
				</tr>";
				
				$html.="<tr>
					<td colspan='4' class='fentgreen1'><br/>
					Address: <b>".$admin_details_basic['address']."</b> 
					Tel: <b>".$admin_details['phone_no']."</b> 
					Tollfree: <b>".$admin_details['tollfree_no']."</b> 
					State Code: <b>".$admin_details_basic['state']."</b> 
					GSTIN :<b>".$admin_details['gst_number_sss']."</b></td>
					<td colspan='3' rowspan='2'>

					<div class='col-md-8 np'>Invoice No.: ".$order_details['invoice_no']."&nbsp;<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Dated: &nbsp;-<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>D. C. No.: &nbsp;-<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Vehicle No.: &nbsp;-<span class='blue'></span></div><br/>
					<div class='col-md-8 np'>Transportation Mode: &nbsp;-<span class='blue'></span></div> <br/>
					<div class='col-md-8 np'>Date & Time of Supply: ".$order_details['date_time_supply']."&nbsp;<span class='blue'></div></span><br/>
					<div class='col-md-8 np'>Place of Supply: ".$order_details['place_of_supply']."&nbsp;<span class='blue'></span></div>
					</td>
				</tr>";
				
				$html.="<tr>
					<td colspan='4' valign='top'>Buyer 
					<span class='buyer_section'><b>".$shop_details['name']."</b><br/></span>
					<span class='buyer_section pad-40'>".$shop_details['address'].",<br/></span>
					<span class='buyer_section pad-40'>".$shop_details['city_name']."<br/></span>
					<span class='buyer_section pad-40'>".$shop_details['state_name']."<br/></span>
					<span class='buyer_section pad-40'>GSTIN NO. ".$shop_details['gst_number']."</span>
					</td>                        
				</tr>
				<tr class='fentgreen'>
				<th width='5%' class='text-center'>SI No.</th>
				<th class='text-center'>Name of Goods</th>
				<th class='text-center'>HSN Code</th>
				<th class='text-center'>Qty</th>
				<th class='text-center'>Rate</th>
				<th class='text-center'>UOM</th>
				<th class='text-center'>Value</th>
				</tr>";

				$html.="
				<tr style='height:214px;margin_bottom:30%;'>";
				
				$i = 1;
				$final_qty = 0;
				$final_cost = 0;
				
				
				$total_amount = 0;
				
				foreach($order_details as $val){
					$sr_no.=$i.'<br><br>';
					$product_name.=$val['productname'].'<br><br>';
					//$hsn.=$val['producthsn'].'<br><br>';
					$c_o_d = $val['c_o_d'];
					$cod_percent = $val['cod_percent'];
					$qty.=$val['variantunit'].'<br><br>';
					$final_qty = $final_qty + $val['variantunit'];
					$unit_cost.=$val['unitcost'].'<br><br>';
					$nos.='nos<br><br>';
					//$total_cost.=$total_cost+($unit_cost*$val['variantunit']).'<br><br>';
					$total_cost.= ($val['unitcost'] * $val['variantunit'])."<br><br>";
					$final_cost = $final_cost + ($val['unitcost'] * $val['variantunit']);

					 $cod_final_cost = ($cod_percent / 100) * $final_cost;
                                   $fcod_final_cost = $final_cost -$cod_final_cost;
					$i++; 
					} 
				
				$html.="<style>
th, td {
    border: 1px solid #364622;
    padding:7px;
}
color: #4a5036;
</style>";
				$html.="<td class='text-center' width='5%' valign='top'><span class='blue'>".$sr_no."</span></td>
				<td class='bg' width='20%' height='20%' style='display:block;'  background='../../assets/global/img/watermark1.png' ><span class='blue'>".$product_name."</span></td>
				<td class='text-right' valign='top'><span class='blue'></span></td>
				<td class='text-right' valign='top'><span class='blue'>".$qty."</span></td>
				<td class='text-right' valign='top'><span class='blue'>".$unit_cost."</span></td>
				<td class='text-center' valign='top'><span class='blue'>".$nos."</span></td>
				<td class='text-right' align='right' valign='top'><span class='blue'>".$total_cost."</span></td>				
				</tr>";
				
				$html.="
				<tr>
				<td></td>
				<td class='text-right'><b>Total</b></td>
				<td class='fentgreen'></td>
				<td class='fentgreen text-right'>".$final_qty."</td>
				<td class='fentgreen'></td>
				<td class='fentgreen'></td>";
				
				$html.="<td class='fentgreen' align='right'>".$final_cost."</td>
				</tr>
				
				<tr>
				<td colspan='4' class='text-center blue'><b> ".$final_cost." only</b>
				
				<table class='table table-bordered-popup'>
				<tbody>
				<tr>
				<td class='text-center'><span class='blue'>HSN/SAC</span></td>
				<td class='text-center'><span class='blue'>Taxable Value</span></td>
				<td colspan='2' class='text-center'><span class='blue'>Central Tax</span> </td>
				<td colspan='2' class='text-center'><span class='blue'>State Tax</span></td>
				</tr>
				
				<tr>
				<td></td>
				<td></td>
				<td><span class='blue'>Rate</span> </td>
				<td><span class='blue'>Amount</span></td>
				<td><span class='blue'>Rate</span> </td>
				<td><span class='blue'>Amount</span></td>
				</tr>";
				 
				$total_amount = 0;
			
				foreach($order_details as $val){
				
					$total_amount = $total_amount + $val['unitcost'];
				
				
				$html.="<tr>
				<td><span class='blue'></span></td>
				<td class='text-right'><span class='blue'>".$val['unitcost']."</span></td>
				<td><span class='blue'> %</span></td>
				<td class='text-right'><span class='blue'></span></td>
				<td><span class='blue'> %</span></td>
				<td class='text-right'><span class='blue'></span></td>
				</tr>";
				 } 
				$html.="<tr>
				<td class='text-right'><span class='blue'>Total</span></td>
				<td class='text-right'><span class='blue'>".$total_amount."</td>
				<td></td>
				<td class='text-right'><span class='blue'></span></td>
				<td> </td>
				<td class='text-right'><span class='blue'></span></td>
				</tr>
				
				</tbody>
				</table>
				
				</td>
				<td colspan='".$colspan2."' rowspan='2' valign='top'>
				<span style='display:inline-block; height:40px;' class='blue'>CGST @  %</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>SGST @  %</span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>Rounding Off</span> <br/>
				<span style='display:inline-block; height:40px;' class='blue'><b>Grand Total</b> </span><br/>
				<span style='display:inline-block; height:40px;' class='blue'>Opening Balance</span> <br/>
				<span style='display:inline-block; height:40px;' class='blue'>Closing Balance </span>
				</td>
				<td rowspan='2' class='text-right' valign='top'>
				<span style='display:inline-block; height:40px;' class='blue'> </span><br/>
				<span style='display:inline-block; height:40px;' class='blue'></span><br/>
				
				
				<span style='display:inline-block; height:40px;' class='blue'>".$cod_percent." % </span><br/>
				<span style='display:inline-block; height:40px;' class='blue'><b>".$fcod_final_cost."</b></span><br/>
				<span style='display:inline-block; height:40px;' class='blue'></span><br/>
				<span style='display:inline-block; height:40px;' class='blue'></span>
				</td>
				</tr>
				
				<tr>
				<td colspan='2' width='25%' valign='top'>
				<u>Declaration:</u><br/>
				
				</td>
				<td width='25%'>
				<div class='text-center' width='40%' valign='top'><b><u>BANK DETAILS</u></b></div>			
						BANK NAME: ".$shop_details['bank_name']."<br/>
						BRANCH : ".$shop_details['bank_b_name']."<br/>
						CC A/C NO.: ".$shop_details['bank_acc_no']."<br/>
						IFSC CODE: ".$shop_details['bank_ifsc']."
						</td>  
				<td  class='fentgreen font-big1' width='20%' valign='top'>For <b>SRI JAYA SHREE FOOD PRODUCTS</b><br/><br/><br/>
				Authorised Signature
				</td>
				</tr>
				</tbody>
				
				</table>
</div>";
$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

require_once  $path.'/vendor/autoload.php';				
$mpdf = new \Mpdf\Mpdf([
	'mode' => 'c',
	'margin_left' => 12,
	'margin_right' => 12,
	'margin_top' => 12,
	'margin_bottom' => 12,
	'margin_header' => 1,
	'margin_footer' => 1
]);
//echo "gfadsfafd";
$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

// Load a stylesheet
$stylesheet = file_get_contents('assets/mpdfstyletables.css');
//$stylesheet = file_get_contents('../../assets/global/plugins/bootstrap/css/bootstrap.min.css');
$mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html);
$filename="uploadpdf/".$order_id.".pdf";
$mpdf->Output($filename, 'F');//in this dynamic file name should be there and seperate folder for upload pdf
$mpdf->Output();