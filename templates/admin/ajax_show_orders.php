<?php 
include ("../../includes/config.php");
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_status = $_POST['order_status'];
$orders = $orderObj->getOrders($order_status);//print_R($orders);
//print"<pre>";print_R($orders);
$order_count = count($orders);
?>
<div class="clearfix"></div>

<table class="table table-striped table-bordered table-hover" id="sample_2">
				<thead>
					<tr>
		<?php 
		$no_check_box = array(2,6);
		$delivered = 0;
		if($_SESSION[SESSION_PREFIX.'user_type']=="Admin" && $order_status == 4) {//delivered orders
			$delivered = 1;
		}
		if(!in_array($order_status,$no_check_box) && $order_count != 0 && $delivered != 1){//6 is payment done & 2 is assigned for delivery ?>
		<th id="select_th">
			<input type="checkbox" name="select_all[]" id="select_all" onchange="javascript:checktotalchecked(this)">
		</th>
		<?php 
		 } 
		?>
		<th>
			Region
		</th>
		<th>
			Regional Head
		</th>
		<th>
			Shop Name
		</th>
		<th>
			Order Id
		</th>								
		<th>
			Order Date 
		</th>	
		 <th>
			Category
		</th>	
		<th>
			Product
		</th>	
		<th>
			Quantity
		</th>
		<th>
			Price (₹)
		</th>
		<th>
			GST Price (₹)
		</th>
		<th>
			Action  
		</th>
	</tr>
				</thead>
				<tbody>
				<?php
				 $newarr=array();
				 $newarr1=array();
				foreach($orders as $val_order)
				{
					if(count($val_order['order_details']) > 0){
						foreach($val_order['order_details'] as $val){
							$product_variant = $orderObj->getSProductVariant($val['product_variant_id']);
							$newarr['region_name']=$val_order['region_name'];
							$newarr['order_by_name']=$val_order['order_by_name'];
							$newarr['shop_name']=$val_order['shop_name'];
							$newarr['order_no']=$val_order['order_no'];
							$newarr['order_date']=$val_order['order_date'];
							$newarr['cat_name']=$val['cat_name'];
							$newarr['product_name']=$val['product_name'].' '.$product_variant;
							$newarr['product_quantity']=$val['product_quantity'];							
							$newarr['product_total_cost']=$val['product_total_cost'];
							$newarr['p_cost_cgst_sgst']=$val['p_cost_cgst_sgst'];
							$newarr['odid']=$val['id'];
							$newarr['oid']=$val_order['id'];
							$newarr['ids']=$val_order["shop_id"];	
							$newarr1[]=$newarr;
						}
					}
				 } 
				// echo "<pre>";print_r($newarr1);
				$out = array();
				foreach ($newarr1 as $key => $value){
					$countval=0;
					foreach ($value as $key2 => $value2){
						if($key2=='ids'){
							 $index = $key2.'-'.$value2;
							if (array_key_exists($index, $out)){
								$out[$index]++;
							} else {
								$out[$index] = 1;
							}
						}
						
					}
				}
				$out1 = array_values($out);$j=0;$temp=$newarr1[0]["shop_name"];$sum=0;
				$gtotalq=0;$gtotalp=0;
				for($i=0;$i<count($newarr1);$i++){
				?>
					<tr class="odd gradeX">
					<?php 
					if($temp==$newarr1[$i]["shop_name"]){						
						$totalq=0;$totalp=0;
						if(!in_array($order_status,$no_check_box)  && $delivered != 1){//6 is payment done & 2 is assigned for delivery ?>
					<td rowspan='<?php echo $out1[$j];?>'>
						<input type="checkbox" name="select_all[]" id="select_all" value="ordersub_<?=$newarr1['oid'];?>_<?=$newarr1['odid'];?>"  onchange="javascript:checktotalchecked(this)">
					</td>
					<?php } ?>
							<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]["region_name"];?></td>
							<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]["order_by_name"];?></td>					
						<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]["shop_name"];?></td>
						
						<?php $sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["shop_name"]; $j++;  }else{
						if(!in_array($order_status,$no_check_box)  && $delivered != 1){//6 is payment done & 2 is assigned for delivery ?>
						<td style="display: none;"></td>
						<?php 
						 } ?>
							<td style="display: none;"></td>
							<td style="display: none;"></td>
							<td style="display: none;"></td>
						<?php } ?>
						<td><?php echo $newarr1[$i]["order_no"];?></td>
						<td><?php echo date('d-m-Y H:i:s',strtotime($newarr1[$i]["order_date"]));?></td>						
						<td><?php echo $newarr1[$i]["cat_name"];?></td>						
						<td><?php echo $newarr1[$i]["product_name"];?></td>
						<td align="right"><?php echo $newarr1[$i]["product_quantity"];?></td>
						<td align="right"><?php echo $newarr1[$i]["product_total_cost"];?></td>
						<td align="right"><?php echo $newarr1[$i]["p_cost_cgst_sgst"];?></td>
						<td>
						<a onclick="showInvoice(<?=$order_status;?>,<?=$newarr1[$i]['oid'];?>)" title="View Invoice">view</a>
						<a onclick="showOrderDetails(<?=$newarr1[$i]['odid'];?>,'Order Details')" title="Order Details">Details</a>
						</td>
						<!--". $row["orderid"]. "-->
					</tr>
					
					<?php
				}
				//echo"<pre>";print_r($out1);
				?>
				
				 </tbody>
			</table>
<script>
$(document).ready(function() {
	 $("#sample_2").dataTable().fnDestroy()

    $('#sample_2').dataTable( {
	order: [],
	columnDefs: [ { orderable: false, targets: [0] } ]
	});
});
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
</script>