<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php"?>
<!-- END HEADER -->

<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>

</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "StockistOrders";	
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<h3 class="page-title">Orders</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Orders</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
					<form class="form-horizontal">
						<div class="form-group">
							<div id="date-show" style="display:none;">
								<label for="inputEmail3" class="col-sm-2 control-label">From Date:</label>
								<div class="col-md-2">
									<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>

								<label for="inputEmail3" class="col-sm-2 control-label">To Date:</label>
								<div class="col-md-2">
									<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>  
						</div>
					</form>	
                          
					 <div class="clearfix"></div>   
					<form class="form-horizontal" method="post" name="place_order_form">
						<input type="hidden" name="action" value="place_order">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Order Received
							</div>
						</div>
						<div class="portlet-body">
			
						<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr>
							<th width="20%">Order Date</th>
							<th width="25%">Distributor Name</th>
							<th width="25%">Orders</th>
							<th width="10%"> Quantity</th>
							<th width="10%">Unit Cost</th>
							<th width="10%">Total Cost</th>
							</tr>
						</thead>
						<tbody>	
							<tr style="display: none">
								<td width="20%">Order Date</th>
								<td width="25%">Distributor Name</th>
								<td width="25%">Orders</th>
								<td width="10%"> Quantity</th>
								<td width="10%">Unit Cost</th>
								<td width="10%">Total Cost</th>
							</tr>
							<?php
							switch($_SESSION[SESSION_PREFIX.'user_type']) {
								case "Admin":
									 $sql = "SELECT * FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id ORDER BY OA.order_date,distributorid,orderid ";
									break;
									case "Superstockist":	
									$sql = "SELECT * FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id WHERE OV.status='2' AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ORDER BY OA.order_date,distributorid,orderid";
									break;
									case "Distributor":									
									$sql = "SELECT * FROM `tbl_order_app` AS OA LEFT JOIN `tbl_variant_order` AS OV ON OV.orderappid = OA.id WHERE OV.status = 1 AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ORDER BY OA.order_date,distributorid,orderid ";
									break;
							}
							//echo $sql;
									$result1 = mysqli_query($con,$sql);								
									while($row1 = mysqli_fetch_array($result1)) { 
										$newarr['order_date']=$row1['order_date'];
										$newarr['distributornm']=$row1['distributornm'];
										$newarr['orderid']=$row1['orderid'];
										$orderidss=$row1['orderid'];
										$newarr['variantunit']=$row1['variantunit'];
										$newarr['totalcost']=$row1['totalcost'];
										$newarr['stotalcost']=$row['variantunit']*$row['totalcost'];
										$valcounter[]=array_count_values($row1[$row1['orderid']]);
										$newarr1[]=$newarr;										
									}
									
									$out = array();
									foreach ($newarr1 as $key => $value){
										$countval=0;
										foreach ($value as $key2 => $value2){
											if($key2=='orderid'){
												 $index = $key2.'-'.$value2;
												if (array_key_exists($index, $out)){
													$out[$index]++;
												} else {
													$out[$index] = 1;
												}
											}
											
										}
									}
									$out1 = array_values($out);$j=0;$temp=$newarr1[0]["orderid"];$sum=0;
									
									for($i=0;$i<count($newarr1);$i++){
										$total=$newarr1[$i]['variantunit']*$newarr1[$i]['totalcost'];										
										?>
										<tr class="odd gradeX">
										<?php if($temp==$newarr1[$i]["orderid"]){ ?>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]["order_date"];?></td>
										<td rowspan='<?php echo $out1[$j];?>'><?php echo $newarr1[$i]['distributornm'];?></td>
										<td rowspan='<?php echo $out1[$j];?>'><a onclick="showInvoice(<?php echo "'".$newarr1[$i]['orderid']."'";?>)"><?php echo $newarr1[$i]['orderid'];?></a></td>
										<?php   $sum=$sum+$out1[$j]; $temp=$newarr1[$sum]["orderid"]; $j++; 
											} else { ?>
										<td style="display: none;"><?php echo $newarr1[$i]["order_date"];?></td>
										<td style="display: none;"><?php echo $newarr1[$i]['distributornm'];?></td>
										<td style="display: none;"><a onclick="showInvoice(<?php echo "'".$newarr1[$i]['orderid']."'";?>)"><?php echo $newarr1[$i]['orderid'];?></a></td>
											<?php } ?>
										<td align="right"><?php echo $newarr1[$i]['variantunit']?></td>
										<td align="right"><?php echo $newarr1[$i]['totalcost']?></td>
										<td align="right"><?php echo $total;?></td>
										</tr>
										<?php } ?>	
							
						</tbody>
						</table>

						</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<div class="modal fade" id="view_invoice" role="dialog">
	<div class="modal-dialog" style="width: 980px !important;">    
		<!-- Modal content-->
		<div class="modal-content" id="view_invoice_content">      
		</div>      
	</div>
</div>
<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function showInvoice(id) {	
//alert("skdfhgjs");
	var url = "invoice.php"; 
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_id='+id,
		async: false
	}).done(function (response) {
		//console.log(response);
		showInvoice1(id,response);
		$('#view_invoice_content').html(response);
		$('#view_invoice').modal('show');
	}).fail(function () { });
	return false;
}
function showInvoice1(id,response) {
		//console.log(response);
	var url = "invoicedemo2.php"; 
    jQuery.ajax({
		url: url,
		method: 'GET',
		data: 'oids='+id,
		async: false
	}).done(function (response) {
		//console.log(response);
	}).fail(function () { });
	return false;
}
function takeprint_invoice() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\
	.darkgreen{	background-color:#364622; color:#fff!important; font-size:24px; font-weight:600;}\
	.fentgreen1{background-color:#b0b29c;color:#4a5036;	font-size:12px;}\
	.fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
	.font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
	.font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
	.table-bordered-popup {    border: 1px solid #364622;}\
	.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
		border: 1px solid #364622;	color:#4a5036;}\
	.blue{	color:#010057;}\
	.blue1{	color:#574960;	font-size:16px;}\
	.buyer_section{	color:#574960;	font-size:14px;}\
	.pad-5{	padding-left:10px;}\
	.pad-40{	padding-left:40px;}\
	.np{	padding-left:0px;	padding-right:0px;}\
	.bg{	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;\
	 background-size: 200px 200px;}\
	</style>' + $("#divPrintArea").html();
	if(isIE == true){
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>