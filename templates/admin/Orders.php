<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
?>
<!-- END HEADER -->
<?php
$recieved = $_GET["recieved"];
if(isset($_POST['action']) && $_POST['action'] == 'place_order')
{
	$user_id = $_SESSION[SESSION_PREFIX.'user_id'];	
	foreach ($_POST['orderid'] as $key => $orderid) {
		$sql_update = mysqli_query($con,"UPDATE `tbl_orders` SET status = '3' WHERE superstockistid = '$user_id' and id = '$orderid' ");
	}
	header("location:Orders.php");
	//var_dump($_POST); exit;
} ?>

<style>
.form-horizontal .control-label {
    text-align: left;
}
</style>
<script>
function fnSelectionBoxTest()
{
	if(document.getElementById('selTest').value == '3')
	{
	   document.getElementById('date-show').style.display = "block";
	   document.getElementById('dvTestData').style.display = "none"; 
	}
	else if(document.getElementById('selTest').value == '2')
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "block"; 
	}
	else
	{
	   document.getElementById('dvTest').style.display = "none";
	   document.getElementById('dvTestData').style.display = "none";
	}
}
</script>
</head>
<!-- END HEAD -->
<body class="page-header-fixed page-quick-sidebar-over-content ">

<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "Orders";
	
	if($recieved=="admin") {
		$activeMenu = "OrderVisibility";
	}
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<h3 class="page-title">Orders</h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="javascript:;">Orders</a>	
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
					<form class="form-horizontal">
						<div class="form-group">
							<div id="date-show" style="display:none;">
								<label for="inputEmail3" class="col-sm-2 control-label">From Date:</label>
								<div class="col-md-2">
									<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>

								<label for="inputEmail3" class="col-sm-2 control-label">To Date:</label>
								<div class="col-md-2">
									<div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
										<input type="text" class="form-control">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
								</div>
							</div>  
						</div>
					</form>	
                          
					 <div class="clearfix"></div>   
					<form class="form-horizontal" method="post" name="place_order_form">
						<input type="hidden" name="action" value="place_order">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
							<? if($recieved=="admin") {?>
								Order Visibility
							<?} else { ?>
								Order Received
							<?}?>
							</div>
                          
						</div>
						<div class="portlet-body">
			
						<table class="table table-striped table-bordered table-hover" id="sample_2">
						<thead>
							<tr>
							<th width="20%">Brand</th>
							<th width="25%">Category</th>
							<th width="25%">Product</th>
							<th width="10%">Free Quantity</th>
							<th width="10%">Sale Quantity</th>
							<th width="10%">Total Quantity</th>
							</tr>
						</thead>
						<tbody>	
							<?php
							//$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
							$order_status=4;
							$orders = $orderObj->getOrdersSuppChnz($order_status);
							//print"<pre>";print_r($orders);
							$order_count = count($orders);
							?>	
							<?php
								if($order_count != 0) { 
							foreach($orders as $val_order){
								//echo "<pre>";print_r($val_order);//brand_name
								$dimentionDetails='';
								$dimentionDetails.=$val_order['product_name'];
								if(!empty($val_order['product_variant_weight1'])){
									$dimentionDetails.=" Weight:".$val_order['product_variant_weight1']."-".$val_order['product_variant_unit1']." " ;
								}
								if(!empty($val_order['product_variant_weight2'])){
									$dimentionDetails.=" Pcs:".$val_order['product_variant_weight2']."-".$val_order['product_variant_unit2']." " ;
								}
								$free_quantity=0;
								$sale_quantity=0;//product_quantity
								$sale_quantity=$val_order["product_quantity"];
								$total_quantity=$free_quantity+$sale_quantity;
								?>
								<tr class="odd gradeX">        
										<td><?php echo $val_order["brand_name"];?></td>
										<td><?php echo $val_order['cat_name'];?></td>
										
										<td><a href="Order1.php?id=<?php echo  $val_order['product_variant_id'];?>"><?php echo $dimentionDetails; ?></a></td>
										<td><?php echo $free_quantity; ?></td>
										<td><?php echo $sale_quantity; ?></td>
										<td><?php echo $total_quantity; ?></td>
										</tr>
							<?php } } ?>
							
						</tbody>
						</table>

						</div>
						</div>
					</form>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->

<div style="display:none;" class="modal-backdrop fade in"></div>
<div aria-hidden="false" style="display: none;" id="basicModal" class="modal fade in">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick="close_modal();" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 class="modal-title">Order Details</h3>
      </div>
      <div class="modal-body">

<div id="ajax_list_div">
</div>
      </div>
       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>

<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script type="text/javascript">
$(document).ready(function(){
	$('#orderidall').bind('click', function(){
  		$('.orderid').prop('checked', this.checked);
});
})

function confirm_place_order(){
	  var order_selected = false;
  $('.orderid').each(function(){
  	if($(this).is(':checked'))
  		order_selected = true;
  })
  if(!order_selected)
  {
  	alert('Please select order to place.');
  	return false;
  }
	var con = confirm("Are you sure, You want to place order?");
	if(con)
	{
		document.forms.place_order_form.submit();
	}
}
function get_order_info(id) {
  console.log('id',id)

  $.ajax
  ({
    type: "POST",
    url: "ajax_orderinfo.php",
   data: "action=get_order&id="+id,
    success: function(msg)
    {
      $("#ajax_list_div").html(msg);
      $('.modal-backdrop').show();

      $('#basicModal').show();

    }
  });
}
function close_modal(){
	$('.modal-backdrop').hide();
	$('#basicModal').hide();
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>