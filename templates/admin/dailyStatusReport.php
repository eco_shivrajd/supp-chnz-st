<?php
include ("../../includes/config.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
extract($_POST);
$record_sp = $userObj->getLocalUserDetails($dropdownSalesPerson);
$result_product = $orderObj->getSalesPOrdersProducts($frmdate,$dropdownSalesPerson);
$record_product_count = mysqli_num_rows($result_product);
$colspan_plus = 0;
if($record_product_count > 7)
	$colspan_plus = $record_product_count -7;

$result = $orderObj->getSalesPOrders($frmdate,$dropdownSalesPerson);
$record_count = mysqli_num_rows($result);
$start_location = "";	
$end_location = "";

$start_time = '';
$end_time = '';
if($record_count > 0)
{
	$result_start = $orderObj->getSalesPOrdersStart($frmdate,$dropdownSalesPerson);
	$result_end = $orderObj->getSalesPOrdersEnd($frmdate,$dropdownSalesPerson);	
	/*Get location using Latitude & Longgitude */
	//var_dump($result_start);
	//print_r($result_start);
	//print_r($result_end);   
    if ($result_end['v_shop_id']!='') 
    {
    	if(($result_start['oplacelat'] != 0.0 && $result_start['oplacelon'] != 0.0) OR ($result_start['oplacelat'] != '' && $result_start['oplacelon'] != ''))
		$start_location = "<br>".$orderObj->getLocation($result_start['oplacelat'],$result_start['oplacelon']);	
	if(($result_end['no_o_lat'] != 0.0 && $result_end['no_o_long'] != 0.0) OR ($result_end['no_o_lat'] != '' && $result_end['no_o_long'] != ''))
		$end_location = "<br>".$orderObj->getEndLocation($result_end['no_o_lat'],$result_end['no_o_long']);
    }
    else
    {
    	if(($result_start['oplacelat'] != 0.0 && $result_start['oplacelon'] != 0.0) OR ($result_start['oplacelat'] != '' && $result_start['oplacelon'] != ''))
		$start_location = "<br>".$orderObj->getLocation($result_start['oplacelat'],$result_start['oplacelon']);	
    }	

    $latitude1 = $result_start['oplacelat'];
    $longitude1 = $result_start['oplacelon'];
    $latitude2 = $result_end['no_o_lat'];
    $longitude2 = $result_end['no_o_long'];   

   // $distance_between_two_address = "Start Point And End Point Required..!!";

      if ($latitude1!='' && $latitude1!=0.0 && $longitude1!='' && $longitude1!=0.0 &&$latitude2!='' && $latitude2!=0.0 && $longitude2!='' && $longitude2!=0.0 ) 
      {
      	$distance = $orderObj->getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2);
      	$distance_between_two_address = $distance." KM";	
      }
      else
      {
      	$distance_between_two_address = "Start Point And End Point Required..!!";
      }	
	  

/*echo "hi";
echo "----".$latitude1."------".$longitude1."----".$latitude2."----".$longitude2."------";
echo "----".$start_location."------";
echo "----".$end_location."------";
echo "----".$distance_between_two_address."------";*/
	
	$start_time = "<br>(Time: ".date('H:i:s',strtotime($result_start['date'])).")";
	$end_time = "<br>(Time: ".date('H:i:s',strtotime($result_end['date'])).")";
}
//print"<pre>";print_r($records);
/*Get Sales Person's Start Day Time & End Day Time */
$records_sp_day_time = $orderObj->getSalesPStartEndDay($frmdate,$dropdownSalesPerson);
/*$start_time = '';
$end_time = '';
if($records_sp_day_time != 0){
	$start_time = "<br>(Time: ".date('H:i:s',strtotime($records_sp_day_time['tdate'])).")";
	$end_time = "<br>(Time: ".date('H:i:s',strtotime($records_sp_day_time['dayendtime'])).")";
}*/
?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Daily Status Report</div>
		<?  if($record_count > 0) { ?>
			<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<? } } ?>
	</div>
	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-bordered" id="report_table">
				<?  if($record_count > 0) { ?>
				<thead>
					<tr>
						<th colspan="<?php echo 14+$colspan_plus;?>" style="text-align:center">
							<b>DS EXPORTS INTERNATIONAL</b><br>
						<!--	<span style="font-size: 12px;">ARUDRAZ CUPS SAMBRANI</span><br> -->
							<span style="font-size: 12px;">CHENNAI,TAMILNADU</span>
						</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td>RDS NAME</td>
						<td colspan="2"></td>
						<td>FIELD STAFF</td>						
						<td colspan="<?php echo 9+$colspan_plus;?>" align="left"><?=$record_sp['firstname'];?>
						<input type="hidden" name="date" id="date" value="<?=$frmdate;?>">
						<input type="hidden" name="salespname" id="salespname" value="<?=$record_sp['firstname'];?>">
						</td>						
					</tr>
					<tr>
						<td></td>
						<td valign="top">TALUKA</td>
						<td valign="top" colspan="2" align="left"><?=$record_sp['suburbnm'];?></td>
						<td valign="top">DATE</td>						
						<td valign="top" colspan="3" align="left"><?=$frmdate;?></td>	
						<td valign="top" colspan="<?php echo 6+$colspan_plus;?>"></td>	
					</tr>
					<tr>
						<td valign="top" colspan="<?php echo 14+$colspan_plus;?>">ROUTE STARTING POINT: <?=$result_start['shopname'].$start_location.$start_time;?></td>						
					</tr>
					<tr>
						<td valign="top" colspan="2">(IN WALKING ORDER) </td>	
						<td colspan="<?php echo 12+$colspan_plus;?>"> </td>
					</tr>
					<tr>
						<td colspan="14" style="height: 30px;"></td>
					</tr>
					<tr>
						<td colspan="5"></td>
						<td valign="top" colspan="<?php echo 9+$colspan_plus;?>">End Point: <?=$result_end['shopname'].$end_location.$end_time;?>
							
							Total Distance Between Two Address : <?php echo $distance_between_two_address ;?> 
						</td>
					</tr>
					<tr>
						<td valign="top">S. No.</td>
						<td valign="top" style="text-align:center">OUTLET NAME</td>
						<td valign="top" style="text-align:center">TIME VISITED:  ADDRESS</td>
						<td valign="top" style="text-align:center">PUNCHED LOCATION</td>
						<td valign="top" style="text-align:center">CONTACT NO. OF SHOP</td>	
						<?php 
						$product_array = array();
						$product_cost = array();
						$i = 1;
						while($record = mysqli_fetch_array($result_product)){ 
						$product_array[$i] = $record['product_variant_id'];
						
						$dimension = '';
						$dimension = $orderObj->getSProductVariant($record['product_variant_id']);
						if($dimension != '')
							$dimension = " (".$dimension.")";
						?>						
						<td valign="top"><?=$record['brandnm'].">".$record['categorynm'].">".$record['productname'].$dimension."<br>(quantity/₹)";?></td>							
						<?php $i++;} 
						$pro_count = count($product_array);
						if($record_product_count < 6){
							$td_count = 6-$record_product_count;
							for($k=1;$k<=$td_count; $k++){
								echo "<td></td>";
							}
						}
						?>
						<td valign="top" style="text-align:center">Total ₹</td>	
						<td valign="top" style="text-align:center">REMARKS</td>	
					</tr>
					<?php 
					$i = 1;
					$shop_total = 0;
					$totalcost = 0;
					if($record_count > 0){						
					while($record = mysqli_fetch_array($result)){ 
						$shop_total = 0;
						$order_time = " (Time: ".date('H:i:s',strtotime($record['date'])).")";
						
						$order_location = "";
						$order_location = $orderObj->getLocation($record['oplacelat'],$record['oplacelon']);

						/*if($record['oplacelat']!='' && $record['oplacelon']!='')
						{
                           $order_location = $orderObj->getLocation($record['oplacelat'],$record['oplacelon']);
						}
						else
						{
                            $order_location = $orderObj->getLocation($record['no_o_lat'],$record['no_o_long']);
						}*/
						

					?>
					<tr>
						<td valign="top"><?=$i;?></td>
						<td valign="top"><?=$record['shopname'];?></td>
						<td valign="top" ><?=$order_time."<br>".$record['address']."<br>".$record['cityname']."<br>".$record['statename'];?></td>
						<td valign="top"><?=$order_location;?></td>		
						<td valign="top"><?=($record['mobile'] == 0) ? '-': $record['mobile'];?></td>						
						<?php 
						for($j=1; $j<=$pro_count; $j++){ 
							$quantity_row = array();
							$display_q_c = "";
							if($record['shop_id'] != ''){
								$quantity_row = $orderObj->getSalesPOrdersPQuantity($record['shop_id'],$product_array[$j],$record['order_date']);
								
								$cost = '';
								$totalcost = 0 ;
								if($quantity_row['variantunit'] !=''){
									$quantity_row['totalcost'] = ($quantity_row['totalcost'] * $quantity_row['variantunit']);
									$cost = "/ ".number_format($quantity_row['totalcost'],2, '.', '');
									$shop_total = $shop_total + $quantity_row['totalcost'];
									$totalcost = $quantity_row['totalcost'];
								}
								$product_cost[$i][$j] = $product_cost[$i-1][$j] + $totalcost;
								$display_q_c = $quantity_row['variantunit'].$cost;
							}else{
								$display_q_c = '-';
							}
						?>						
						<td valign="top" align="right"><?=$display_q_c;?></td>							
						<?php } 					
						if($record_product_count < 6){
							$td_count = 6-$record_product_count;
							for($k=1;$k<=$td_count; $k++){
								echo "<td></td>";
							}
						}
						?>
						<td><?=number_format($shop_total,2, '.', '');?></td>
						<td valign="top">
						<?php $remark = '';
						if($record['shop_visit_reason'] != '')
							$remark.= $record['shop_visit_reason']."<br>";
						if($record['shop_close_reason_type'] != '')
							$remark.= $record['shop_close_reason_type']."<br>";
						if($record['shop_close_reason_details'] != '')
							$remark.= $record['shop_close_reason_details']."<br>";
						
						echo $remark;
						?>
						</td>
					</tr>
					<?php $i++; }  ?>	
					<tr>
						<td valign="top" colspan="5" style="text-align:right;">Total ₹</td>											
						<?php 
						$total_count = count($product_cost);
						for($j=1; $j<=$pro_count; $j++){ //	print"<pre>";print_r($product_cost);						
						?>						
						<td valign="top" align="right"><?=number_format($product_cost[$total_count][$j],2, '.', '');?></td>							
						<?php } 
						if($record_product_count < 6){
							$td_count = 6-$record_product_count;
							for($k=1;$k<=$td_count; $k++){
								echo "<td></td>";
							}
						}
						?>
						<td></td>
						<td></td>
					</tr>
				<?php } ?>
					<tr>
					   <td colspan="<?php echo 14+$colspan_plus;?>" style="height: 30px;">
						

						</td>
					</tr>
					<tr>
						<td valign="top" colspan="<?php echo 14+$colspan_plus;?>">SM:              DS:                GM:                   CD:                    ORG:                 POOJA:                     PS:                      MED:                     MOD:          </td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">AVL O/L</td>
						<td valign="top" style="text-align:center">SERVICE O/L</td>
						<td valign="top" style="text-align:center">ARUDRAZ O/L</td>
						<td valign="top" style="text-align:center">NON SERVICE O/L</td>						
						<td valign="top" style="text-align:center">NEW PLACE O/L</td>	
						<td valign="top" colspan="<?php echo 8+$colspan_plus;?>" style="text-align:center">FOCUS PRODUCT</td>	
						<td></td>							
					</tr>					
					<tr>
						<td colspan="5"></td>	
						<td valign="top">KDS</td>	
						<td valign="top">AKB</td>	
						<td valign="top">9 FLVS</td>	
						<td valign="top">10 RS</td>	
						<td valign="top">20 RS</td>	
						<td valign="top">JAR</td>	
						<td></td>	
						<td colspan="<?php echo $colspan_plus+1;?>"></td>							
					</tr>
					<tr>
						<td colspan="5" style="height: 30px;"></td>	
						<td></td>	
						<td></td>	
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>	
						<td colspan="<?php echo $colspan_plus+1;?>"></td>							
					</tr>
				</tbody>
				<?php }else{
					echo "<tr><td>No Record available.</td></tr>";
				}?>
			</table>
		</div>
	</div>
</div> 