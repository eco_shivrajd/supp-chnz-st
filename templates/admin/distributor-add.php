<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") {
	header("location:../logout.php");
}  
include "../includes/userManage.php";	
$userObj 	= 	new userManager($con,$conmain);
if(isset($_POST['hidbtnsubmit']))
{	
	//print"<pre>";print_r($_POST);
	$user_type = "Distributor";

	/* Local user section */
	$userid_local = $userObj->addLocalUserDetails($user_type);
	$userObj->addLocalUserWorkingAreaDetails($userid_local);
	
	/* Common user section */
	$username	=	fnEncodeString($_POST['username']);
	$common_user_record = $userObj->getCommonUserDetailsByUsername($username);	
	if($common_user_record != 0)
	{
		$common_user_company_record = $userObj->getCommonUserCompanyDetails($userid_local);
		if($common_user_company_record != 0)
		{
			$userObj-addCommonUserCompanyDetails($userid_local);
		}
	}else{
		$userid_common = $userObj->addCommonUserDetails($user_type,$userid_local);
		$userObj->addCommonUserCompanyDetails($userid_local);
	}	
	$email		=	fnEncodeString($_POST['email']);
	if($email != '')
		$userObj->sendUserCreationEmail();
	
	echo '<script>alert("Stockist added successfully.");location.href="distributors.php";</script>'; 
}	
$default_user_details = $userObj->getDefaultUserDetails('Superstockist');
$default_state = '';
$default_city = '';
$default_area = '';//print_r($default_user_details);
if($default_user_details != 0){
	$default_state = $default_user_details['state_ids'];
	$default_city = $default_user_details['city_ids'];
	$default_area = $default_user_details['suburb_ids'];
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Stockist";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Stockist
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="distributors.php">Stockist</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Stockist</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Stockist
							</div>
							
						</div>
						<div class="portlet-body">
						 <span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>                       
						                       
                          
						<form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">
						<? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
						<div class="form-group">
						  <label class="col-md-3">Superstockist:<span class="mandatory">*</span></label>

						  <div class="col-md-4">
							<select name="assign" class="form-control" onchange="javascript:showStateCity(this);"> 
							<?php
							$user_type="Superstockist";
							$result1 = $userObj->getLocalUserDetailsByUserType($user_type);							
							while($row = mysqli_fetch_array($result1))
							{
								$assign_id = $row['id'];
								echo "<option value='$assign_id'>" . fnStringToHTML($row['firstname']) . "</option>";
							}
							?>
							</select>
						  </div>
						</div><!-- /.form-group -->   
						<? } else { ?>
							<input type="hidden" name="assign" id="assign" value="<?=$_SESSION[SESSION_PREFIX.'user_id']?>">
						<? }?>			
						 
			 
						<?php $page_to_add = 'stockist'; include "userAddCommEle.php";	//form common element file with javascript validation ?>    
					   
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">							
							<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
							<input type="hidden" name="hidAction" id="hidAction" value="distributor-add.php">
							<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
							<a href="distributors.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                              
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<script>
function showStateCity(obj){
	var ss_id = obj.value;
	jQuery.ajax({
			url: "../includes/ajax_getUserAssLocation.php",
			data:'ss_id='+ss_id,
			type: "POST",
			async:false,
			success:function(data){		
				var user_record = JSON.parse(data);
				if(user_record!=0) {
					var state = user_record.state;
					var city = user_record.city;
					$("#state").val(state);
					setTimeout(
					function() 
					{	
						fnShowCity(state);
						setTimeout(
						function() 
						{
							$("#city").val(city);	
						}, 1800);
					}, 600);
					setTimeout(function() 
					{
						FnGetSuburbDropDown($("#city").get(0));						
					}, 2800);
					
				} 
			},
			error:function (){}
		});
}
</script>
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>