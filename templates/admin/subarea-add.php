<!-- BEGIN HEADER -->
<?php include "../includes/header.php";

if(isset($_POST['submit']))
{
	$txtsubarea = fnEncodeString($_POST['txtsubarea']);	
	$state	 	= $_POST['state'];
	$city 	 	= $_POST['city'];
	$suburbnm 	= $_POST['suburbnm'];
	
	$sql1 = mysqli_query($con,"INSERT INTO tbl_subarea (subarea_name, city_id, state_id, suburb_id) VALUES('".$txtsubarea."','".$city."','".$state."','".$suburbnm."')");
	
	echo '<script>alert("Subarea added successfully."); location.href="subarea.php";</script>';
	
}

if($_SESSION[SESSION_PREFIX.'user_type']!="Admin") {
	header("location:../logout.php");
} ?>

<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Subarea";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->			
			<!-- /.modal -->			
			<h3 class="page-title">Subarea</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					
					<li>
						<i class="fa fa-home"></i>
						<a href="subarea.php">Subarea</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Subarea</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Subarea
							</div>							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
						
						<form class="form-horizontal" data-parsley-validate="" role="form" method="post">
						
						
						<div class="form-group">
							<label class="col-md-3">State:<span class="mandatory">*</span></label>
							<div class="col-md-4">
							<select name="state"              
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please select state"
							class="form-control" onChange="getCityDropCities(this.value)">
							<option selected disabled>-select-</option>
							<?php
							$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								echo "<option value='$cat_id'>" . $row['name'] . "</option>";
							} ?>
							</select>
							</div>
						</div><!-- /.form-group -->
						
						<div id="Subcategory"></div> 
						
						<div id="div_suburb"></div> 
						
						<div class="form-group">
						  <label class="col-md-3">Subarea Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text" 
							placeholder="Subarea Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter subarea name"
							data-parsley-maxlength="50"
							data-parsley-maxlength-message="Only 50 characters are allowed"
							name="txtsubarea" id="txtsubarea" class="form-control">
						  </div>
						</div><!-- /.form-group -->
						<div class="form-group">
						  <div class="col-md-4 col-md-offset-3">
						   <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
							<a href="subarea.php" class="btn btn-primary">Cancel</a>
						  </div>
						</div><!-- /.form-group -->
					  </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

<style>
.form-horizontal{
	font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function getCityDropCities(str)
{
	if (str=="")
	{
		document.getElementById("Subcategory").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("Subcategory").innerHTML = xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","fetch.php?cat_id="+str,true);
	xmlhttp.send();
}


function showSuburb(str,action)
{
	if (str=="")
	{
		document.getElementById("div_suburb").innerHTML="";
		return;
	}
	if (window.XMLHttpRequest)
	{
		xmlhttp=new XMLHttpRequest();
	}
	else
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200)
		{
			document.getElementById("div_suburb").innerHTML=xmlhttp.responseText;
		}
	}
	xmlhttp.open("GET","fetch_suburb.php?cat_id="+str,true);
	xmlhttp.send();
}
</script>            