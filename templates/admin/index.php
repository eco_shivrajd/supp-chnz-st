<?php include "../includes/header.php"?>
<!-- END HEADER -->
<script type="text/javascript">
function fnSelectionBoxTest()
{
	var str = $( "form" ).serialize();	
	document.getElementById('hdnSelrange').value=document.getElementById('selTest').value;
	if(document.getElementById('selTest').value == '3')
	{
	   document.getElementById('date-show').style.display = "block";
	}
	else
	document.frmSearch.submit();
}
 
</script>
<?php

switch($_SESSION[SESSION_PREFIX.'user_type']){
	case "Admin":
		//get Sales count for this month
		$sqltotalmonthsale	=	"SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '') AND date_format(order_date, '%m')=date_format(now(), '%m')";
		$resultmonthsale 	=	mysqli_query($con,$sqltotalmonthsale);
		$rowmonthsale 		=	mysqli_fetch_array($resultmonthsale);
		//end
		//get Sales count for all
		$sqltotaltsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO WHERE  (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'   OR VO.campaign_sale_type LIKE '' ) ";
		$resulttotaltsale = mysqli_query($con,$sqltotaltsale);
		$rowtotaltsale = mysqli_fetch_array($resulttotaltsale);
		//get new sales
		//get Sales count for all
		$sqlnewsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO ,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'   OR VO.campaign_sale_type LIKE '' ) AND VO.status='3'";
		$resultnewsale = mysqli_query($con,$sqlnewsale);
		$rownewsale = mysqli_fetch_array($resultnewsale);
	break;
	case "Superstockist":
		//get Sales count for this month
		$sqltotalmonthsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND date_format(order_date, '%m')=date_format(now(), '%m') AND (VO.status='2' OR VO.status='3') AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'   OR VO.campaign_sale_type LIKE '' ) AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resultmonthsale = mysqli_query($con,$sqltotalmonthsale);
		$rowmonthsale = mysqli_fetch_array($resultmonthsale);
		//end
		//get Sales count for all
		$sqltotaltsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND (VO.status='2' OR VO.status='3') AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'   OR VO.campaign_sale_type LIKE '' )  AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resulttotaltsale = mysqli_query($con,$sqltotaltsale);
		$rowtotaltsale = mysqli_fetch_array($resulttotaltsale);
		//get new sales
		//get Sales count for all
		$sqlnewsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND VO.status='2' AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '' )  AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resultnewsale = mysqli_query($con,$sqlnewsale);
		$rownewsale = mysqli_fetch_array($resultnewsale);
	break;
	case "Distributor":
		//get Sales count for this month
		$sqltotalmonthsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND date_format(order_date, '%m')=date_format(now(), '%m') AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '' )  AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resultmonthsale = mysqli_query($con,$sqltotalmonthsale);
		$rowmonthsale = mysqli_fetch_array($resultmonthsale);
		//end
		//get Sales count for all
		$sqltotaltsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '')  AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resulttotaltsale = mysqli_query($con,$sqltotaltsale);
		$rowtotaltsale = mysqli_fetch_array($resulttotaltsale);
		//get new sales
		//get Sales count for all
		$sqlnewsale="SELECT SUM(`totalcost`*`variantunit`) as total_amt FROM `tbl_variant_order` AS VO,tbl_order_app WHERE tbl_order_app.id=VO.orderappid AND VO.status='1' AND (VO.campaign_sale_type IS NULL OR VO.campaign_sale_type = 'sale'  OR VO.campaign_sale_type LIKE '')  AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'";
		$resultnewsale = mysqli_query($con,$sqlnewsale);
		$rownewsale = mysqli_fetch_array($resultnewsale);
	break;
}

?>
<!-- BEGIN BODY -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">

<? 
$activeMainMenu = "Dashboard"; $activeMenu = "";
include "../includes/sidebar.php"; 
?>

<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN PAGE HEADER-->
			<h3 class="page-title">Dashboard <small>Reports & Statistics</small></h3>
			<div class="page-bar">
				<ul class="page-breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Dashboard</a>
					</li>					
				</ul>
				<!--<a id="treeview" href="#" class="btn btn-sm btn-default pull-right mt5">
                                Treeview
				</a>-->
			</div>
			<!-- END PAGE HEADER-->		
			<!-- BEGIN DASHBOARD STATS -->
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-comments"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo number_format($rowmonthsale[0],2);?>&nbsp;<i aria-hidden="true" class="fa fa-inr fa-6"></i>
							</div>
							<div class="desc">Month Sales</div>
						</div>						
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-bar-chart-o"></i>
						</div>
						<div class="details">
							<div class="number">
								  <?php echo number_format($rowtotaltsale[0],2);?>&nbsp;<i aria-hidden="true" class="fa fa-inr fa-6"></i>
							</div>
							<div class="desc">
								 Total Sales
							</div>
						</div>						
					</div>
				</div>
				<div class="col-sm-4 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<i class="fa fa-shopping-cart"></i>
						</div>
						<div class="details">
							<div class="number">
							<?php echo number_format($rownewsale[0],2);?>&nbsp;<i aria-hidden="true" class="fa fa-inr fa-6"></i>
							</div>
							<div class="desc">
								 New Orders
							</div>
						</div>						
					</div>
				</div>
			</div>
			<!-- END DASHBOARD STATS -->
			<div class="row">
			<div class="col-sm-12">
				<form class="form-horizontal" name="frmSearch" id="frmSearch" method="post">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-1 control-label">Options:</label>
					<div class="col-sm-2">						
						<select class="form-control" name="selTest" id="selTest" onChange="fnSelectionBoxTest()">
						<option value='0'>-Select-</option>
						<option value="4" <?php if($_REQUEST['selTest']=="4")echo 'selected';?>>Today</option>
						<option value='1' <?php if($_REQUEST['selTest']=="1")echo 'selected';?>>Weekly</option>
						<option value='2' <?php if($_REQUEST['selTest']=="2")echo 'selected';?>>Current month</option>
						<option value='3' <?php if($_REQUEST['selTest']=="3")echo 'selected';?>>From specific date</option>
						</select>
						<input type="hidden" name="hdnSelrange" id="hdnSelrange">
					</div>
				</div>
				<div class="form-group">
					<?php 
					if($_REQUEST['selTest']=="3"){
						$dtdisp="display:block;";
						$frmdate = $_REQUEST['frmdate'];
						$todate = $_REQUEST['todate'];
					}
					else
					{
						$dtdisp="display:none;";
						$frmdate = "";
						$todate = "";
					} ?>
					<div id="date-show" style="<?php echo $dtdisp;?>">
						<label for="inputEmail3" class="col-sm-1 control-label">From Date:</label>
						<div class="col-md-2">
							<div class="input-group  date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
							<input type="text" class="form-control" name="frmdate" id="frmdate" value="<?php echo $frmdate;?>" readonly>
							<span class="input-group-btn">
							<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
							</span>
							</div>
							<!-- /input-group -->
						</div>
						<label for="inputEmail3" class="col-sm-1 control-label">To Date:</label>
						 
							<div class="col-md-2">
								<div class="input-group date date-picker" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
								<input type="text" class="form-control" name="todate" id="todate" value="<?php echo $todate;?>" readonly>
								<span class="input-group-btn">
								<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
								</span>
								</div>
							</div>
							<div class="col-sm-2">
								<button type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-primary">Submit</button>
								<!-- /input-group -->
							</div>
						 
					</div>
				</form> 
				
				 <div class="clearfix"></div>
				 
				<div class="col-md-12">&nbsp;</div>	
                
                <div class="clearfix"></div>
				
				
				<div class="col-md-4">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Category Trend 
							</div>
						
						</div>
						<div class="portlet-body">
							<div id="chartdiv3" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				
			    <? if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") { ?>
				<div class="col-sm-4">
					<!-- BEGIN PORTLET-->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>Superstockist Wise Trend
							</div>
						
						</div>
						<div class="portlet-body">
							<div id="chartdiv" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
					<!-- END PORTLET-->
				</div>
				<? } ?>
				
				<div class="col-sm-4">
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-bar-chart-o"></i>
								 <? if($_SESSION[SESSION_PREFIX.'user_type']=="Distributor") { ?>
								Sales Person Wise Trend
								 <? } else { ?>
								Stockist Wise Trend
								 <?} ?>
							</div>
						</div>
						<div class="portlet-body">
							<div id="chartdiv2" style="width: 100%; height: 300px; background-color: #FFFFFF;" ></div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		
		</div>
		
		</div>
		<div class="clearfix"></div>
	</div>
	<!-- END CONTENT -->
	<!--Treeview Modal -->
 
	<div id="dialog" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Modal Header</h4>
			  </div>
			  <div class="modal-body">
				<p>Some text in the modal.</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
	  </div>
<!--Treeview modal end-->
</div>
<!-- END CONTAINER -->

<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
</body>
<!-- END BODY -->
</html>