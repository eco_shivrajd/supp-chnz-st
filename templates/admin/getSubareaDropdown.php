<?php
include ("../../includes/config.php");
$area_id = $_GET["area_id"]; 
$select_name = "dropdownSubarea";
$select_id = "dropdownSubarea";
if(isset($_GET['select_name_id'])){
	$select_name = $_GET['select_name_id'];
	$select_id = $_GET['select_name_id'];
}

if(isset($_GET['multiple_id'])){
	$multiple_id = $_GET['multiple_id'];
	$sql="SELECT subarea_id, subarea_name FROM tbl_subarea WHERE suburb_id IN ($multiple_id) and tbl_subarea.isdeleted!='1' ";
}else{
	$sql="SELECT subarea_id, subarea_name FROM tbl_subarea WHERE suburb_id ='$area_id' and tbl_subarea.isdeleted!='1' ";
}
$result1 = mysqli_query($con,$sql);
$rowcount=mysqli_num_rows($result1);
if($rowcount > 10)
	$size = 11;
else
	$size = intval($rowcount) + 1;

$function_name = "";
if(isset($_GET['function_name']))
	$function_name = $_GET['function_name']."(this)";

$multiple = "";
if(isset($_GET['multiple']))
	$multiple = $_GET['multiple'];

if($multiple != '')
{
	$select_name = $_GET['select_name_id']."[]";
	$select_id = $_GET['select_name_id'];
}

?>
<select name="<?php echo $select_name; ?>" id="<?php echo $select_id; ?>" class="form-control" size="<?=$size;?>"  onchange="<?=$function_name;?>" <?php echo $multiple; ?>>
<?php	
if($multiple == '' OR $rowcount == 0)
	echo "<option value=''>-Select-</option>";

while($row = mysqli_fetch_array($result1))
{	
	$selected = "";
	if(in_array($row["subarea_id"],$selectedval))
		$selected = "selected";
	echo "<option value='".$row["subarea_id"]."' $selected>" . fnStringToHTML($row["subarea_name"]) . "</option>";
}
?>
</select>
<? mysqli_close($con); ?>