<?php
include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
if(isset($_POST['hidbtnsubmit']))
{
	$id=$_POST['id'];		
	$user_type	=	"Superstockist";
	$userObj->updateLocalUserDetails($user_type, $id);
	$userObj->updateCommonUserDetails($id);	
	$working_detail = $userObj->getLocalUserWorkingAreaDetails($id);
	//echo "<pre>";print_r($working_detail);die();
	if($working_detail == 0)
		$userObj->addLocalUserWorkingAreaDetails($id);
	else
		$userObj->updateLocalUserWorkingAreaDetails($id);	
	header('location:superstockist.php');
}
?>
<!DOCTYPE html>
<!-- BEGIN HEADER -->
 
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php 
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "Superstockist";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->			
			<h3 class="page-title">Superstockist</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="superstockist.php">Superstockist</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Superstockist</a>
					</li>
					
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Superstockist
							</div>
							<a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']);?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">										
							<?php
							$id=$_GET['id'];
							
							$user_details = $userObj->getLocalUserDetails($id);
							$working_area_details = $userObj->getLocalUserWorkingAreaDetails($id);
							if($working_area_details != 0)
							{
								$row1 = array_merge($user_details,$working_area_details);//print"<pre>";print_r($row1);
								if($row1['state_ids'] == '')
									$row1['state_ids'] = $user_details['state'];
								if($row1['city_ids'] == '')
									$row1['city_ids'] = $user_details['city'];
							}
							else{
								$row1 = $user_details;
								$row1['state_ids'] = $user_details['state'];
								$row1['city_ids'] = $user_details['city'];
							}
							?>                       
                          
							<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" method="post" action="">    
								<?php $page_to_update = 'superstockist';  include "userUpdateCommEle.php";	?>
								<div class="form-group">
								  <div class="col-md-4 col-md-offset-3">
									<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
									<input type="hidden" name="hidAction" id="hidAction" value="superstockist1.php">
									<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
									<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button> 
									&nbsp;
									<a href="superstockist.php" class="btn btn-primary">Cancel</a>
									<!-- <button name="submit" id="submit" class="btn btn-primary">Submit</button>
									<a href="superstockist.php" class="btn btn-primary">Back</a> -->
									<!--<a data-toggle="modal" href="#thankyouModal"  class="btn btn-primary">Delete</a>-->
								  </div>
								</div><!-- /.form-group -->
							   
							  </form>
							  
							  <div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
								<div class="modal-dialog" style="width:300px;">
									<div class="modal-content">
										<div class="modal-body">
											<p>
											<h4 style="color:red; text-align:center;">Do you want to delete this record ?</h4>
											</p>                     
										  <center><a href="superstockist_delete.php?id=<?php echo $row1['id']?>" ><button type="button" class="btn btn-success">Yes</button></a>
										  <button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">No</button>
										  </center>
										</div>    
									</div>
								</div>
							</div>	               
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->

</body>
<!-- END BODY -->
</html>