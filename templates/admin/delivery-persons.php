<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "DeliveryPerson";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Delivery Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Delivery Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Delivery Persons Listing
							</div>
                           <!-- <a href="delivery-person-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Delivery Person
                              </a>-->
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th width="15%">
									 Name
								</th>																
                                <th width="20%">
									 Email
								</th>
                                <th width="10%">
									 Mobile Number
								</th>
								<th width="10%">
                                	State
                                </th> 
								<th width="10%">
                                	City
                                </th> 
								<th width="10%">
                                	Region
                                </th>    
								</tr>
							</thead>
							<tbody>
							<?php
							$user_type='DeliveryPerson';
							$result1 = $userObj->getAllLocalUserDetails($user_type,1);						
							while($row = mysqli_fetch_array($result1))
							{
								$user_id=$row['id'];
								//echo $user_id;
								//exit(); 
								$resultarea = $userObj->getLocalUserWorkingRegionDetails($user_id);

								echo '<tr class="odd gradeX">
									<td>
										 <a href="delivery-person-edit.php?id='.$row['id'].'">'.fnStringToHTML($row['firstname']).'</a>
									</td>'; 
									echo '<td>'.$row['email'].'</td>
									<td>'.$row['mobile'].'</td>
									<td>'.$resultarea['state_name'].'</td>
									<td>'.$resultarea['city_name'].'</td>
									<td>'.$resultarea['region_name'].'</td>';
								echo '</tr>';
							} ?>
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<div class="modal fade" id="assignStockistModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Assign Stockist</h4>	   
      </div>		
    <form role="form" class="form-horizontal" onsubmit="return false;" action="delivery-persons.php" data-parsley-validate="" name="assign_stockist_form" id="assign_stockist_form">
      <div class="modal-body" >	  
	  <div class="clearfix"></div>
			<div class="form-group">
			<label class="col-md-3">Select Stockist:</label>
			<div class="col-md-4" id="show_stockist_dropdown">
				
			</div>
			</div><!-- /.form-group --> 			
			<div class="form-group">
				<div class="col-md-4 col-md-offset-3">					
					<button type="submit"  name="btnsubmit"  class="btn btn-primary">Submit</button>
					<a href="delivery-persons.php" class="btn btn-primary">Cancel</a>
				</div>
			</div><!-- /.form-group -->
			<input type="hidden" name="sales_p_id" id="sales_p_id" value="0"/>
			<input type="hidden" name="action" id="action" value="assign_stockist"/>
		</div><!-- /.form-group --> 				
      </div>
	   </form>	   	  
    </div>
  </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function assign_stockist(sales_p_id){	
	$('#sales_p_id').val(sales_p_id);
	var select_drop = $('#stockist_dropdown_'+sales_p_id).html();
	$('#show_stockist_dropdown').html(select_drop);
	$('#assignStockistModal').modal('show');
}
$('form#assign_stockist_form').submit(function(){
	var formData = new FormData($(this)[0]);	
	$.ajax({
		url:"../includes/assign_stockist.php",
		type: 'POST',
		data: formData,
		success: function (data) {
			if(data == 1){
				alert('Stockist assigned successfully');
				$('#assignStockistModal').modal('hide');
				document.forms.assign_stockist_form.reset();
				window.location.reload(true);
			}else if(data == 'select'){
				alert('Please select Stockist');
				return false;
			}else{
				alert('Unable to assign Stockist');
				return false;
			}					
		},
		cache: false,
		contentType: false,
		processData: false
	});
});
</script>
</body>
<!-- END BODY -->
</html>