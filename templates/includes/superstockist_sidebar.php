<div class="page-sidebar-wrapper">		
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler">
				</div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>
			<br/>
			<li class="active open">
				<a href="index.php">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				<span class="selected"></span>
				</a>
			</li>
			<li>
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Supply Chain</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li>
						<a href="distributors.php">
						 Stockist</a>
					</li>
					<li>
						<a href="sales.php">
						 Sales Person </a>
					</li>
					<li>
						<a href="shops.php">
						 Shops</a>
					</li>
				</ul>
			</li>
			<li class="">
				<a href="Orders.php">
				<i class="icon-home"></i>
				<span class="title">Orders</span>
				<span class="arrow "></span></a>
				<ul class="sub-menu">
					<li class="active">
						<a href="Orders.php">
						 Orders</a>
					</li>
					<li>
						<a href="PlacedOrders.php">
						 Placed Orders </a>
					</li>
				</ul>
			</li>			
			<li>
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Reports</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				   <li>
						<a href="reports.php">Sales Statistic</a>
					</li>
					<li>
						<a href="sales_report.php">Sales Report</a>
					</li> 
				</ul>
			</li>
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>