<?php
/***********************************************************
 * File Name	: orderManage.php
 ************************************************************/	
class orderManage
{	
	private $local_connection   	= 	'';		
	private $common_connection   	= 	'';
	public function __construct($con123,$conmain) {
		$this->local_connection = $con123;
		$this->common_connection = $conmain;
	}
	public function getReportTitle($frmdate=null,$todate=null){
		extract($_POST);
		$report_title = '';
		switch($report_type){
			case "superstockist":
					$report_title = 'Super Stockist';
				break;
			case "stockist":
					$report_title = 'Stockist';
				break;
			case "salesperson":	
					$report_title = 'Sales Person';
				break;
			case "shop":	
					$report_title = 'Shops';
				break;
			case "product":	
					$report_title = 'Products';
				break;
		}
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	//title for shop
	public function getReportTitleForShop($frmdate=null,$todate=null){
		extract($_POST);
		$where="";
		if(!empty($selshop)){
			$where=" WHERE id= '$selshop' ";
		}
		$report_title = 'Sale Report For Shop : ';
		$sql="SELECT id , name FROM tbl_shops  $where";
	
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$report_title.= $row['name'];
			}
		}else{
			$report_title ='No shop found';
		}
		
		return $report_title;
	}
	//new method for sp sales report productwise //ganeshfoods
	public function getAllOrders1() {
		
		extract($_POST);
		$selprod= $_POST['selprod'];
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(OA.order_date) = yearweek(curdate())";
					//$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(OA.order_date, '%m')=date_format(now(), '%m')";
					//$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(OA.order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				//	$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(OA.order_date, '%d-%m-%Y') = '".$date."' ";
				//	$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		$where="";
		if($selprod!=''){
			$where= " AND VO.product_id = $selprod ";
		}
		if($user_type=='Admin'){
			$external_id = '';
		}
		
		
		if($external_id != '')
		{
			$where = " AND salesman.external_id IN (". $external_id.")";
		}
		 $sql = "SELECT
					salesman.id,	
					salesman.firstname,
					sum(VO.product_quantity) as totalunit, 
					sum(p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user salesman 
				LEFT JOIN tbl_orders OA ON OA.ordered_by = salesman.id
				LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id
				LEFT JOIN tbl_product TP ON VO.product_id = TP.id				
				WHERE salesman.user_type='salesperson'
					$where $condnsearch 
					 group by salesman.id ";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //tbl_orders
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	//new method for shop sales report  //ganeshfoods
	public function getAllOrders2() {
		extract($_POST);
		$selshop= $_POST['selshop'];
		
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$luser_id = $_SESSION[SESSION_PREFIX.'user_id'];
		$where="";		
		if($user_type=='Superstockist'){
			$where=" AND OA.superstockistid ='$luser_id' ";
		}
		if($user_type=='Distributor'){
			$where=" AND OA.distributorid='$luser_id' ";
		}
		
		 $sql = "SELECT					
					TP.id as id,	
					TP.productname as productname,
					date_format(OA.order_date, '%M') as month,
					VO.product_variant_weight1,
					VO.product_variant_unit1 as unit, 
					VO.product_quantity as variantunit, 
					VO.product_unit_cost as unitcost,
					VO.product_variant_id
				FROM tbl_shops AS TS
				LEFT JOIN tbl_orders OA ON OA.shop_id = TS.id							
				LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id
				LEFT JOIN tbl_product TP ON VO.product_id = TP.id 
				WHERE OA.shop_id = $selshop
				$where ORDER BY `TP`.`productname`";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //FROM tbl_product TP 
			
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	
	public function getAllOrders() {
		extract($_POST);	
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($report_type){
			case "superstockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' $search_name
							GROUP BY u.firstname  ".$order_by.$limit;
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name
							GROUP BY u.firstname ".$order_by.$limit;
						break;
						 
						break;
					}
				break;
			case "stockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.distributorid = u.id ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Superstockist":						
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.distributorid = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' AND u.external_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						 
						break;
					}
				break;
			case "salesperson":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id, u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON OA.ordered_by = u.id ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE u.user_type='SalesPerson' $search_name GROUP BY u.firstname ".$order_by.$limit;	
							
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON OA.ordered_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (  '".$_SESSION[SESSION_PREFIX.'user_id']."') OR u.external_id IN (SELECT id FROM tbl_user where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."') ) 
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_orders oa 
							LEFT JOIN tbl_user u  ON OA.ordered_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv, tbl_shops AS ss WHERE ss.id=sv.shop_id AND u.id = sv.salesperson_id AND (ss.stockist_id = ".$_SESSION[SESSION_PREFIX.'user_id'].") $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON OA.ordered_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (".$_SESSION[SESSION_PREFIX.'user_id'].")  OR external_id LIKE ('%,". $_SESSION[SESSION_PREFIX.'user_id']."%'))
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_orders oa 
							LEFT JOIN tbl_user u  ON OA.ordered_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name   AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
					}
				break;
			case "shop":	
					if($search != '')
						$search_name = " AND s.name LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":						
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_orders oa ON shop_id = s.id
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) ".$condnsearch." 
							WHERE s.id!=0 AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_orders oa ON shop_id = s.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE s.id!=0 AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !=''  $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_orders oa ON shop_id = s.id  AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE s.id!=0  AND  s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
							
						break;
					}	
				break;
			case "product":	
					if($search != '')
						$search_name = " AND p.productname LIKE '%".$search."%'";
				switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":		
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							/*$sql="SELECT p.productname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_orders oa ON oa.catid   = p.catid  ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))  AND vo.productid = p.id 
							WHERE p.id!=0 AND p.productname !=''  $search_name 
							GROUP BY p.productname ".$order_by.$limit;	*/

							$sql="SELECT p.productname as Name,
	                        sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
                            LEFT JOIN tbl_order_details vo ON vo.product_id = p.id 
							LEFT JOIN tbl_orders oa ON oa.id   = vo.Order_id 
							WHERE p.id!=0 AND p.productname !='' ".$condnsearch." $search_name 
							GROUP BY p.productname ".$order_by.$limit;



						break;
						case "Superstockist":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							/*$sql="SELECT p.productname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_orders oa ON oa.catid   = p.catid AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name 
							GROUP BY p.productname ".$order_by.$limit;	*/
							$sql="SELECT p.productname as Name,
	                        sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
                            LEFT JOIN tbl_order_details vo ON vo.product_id = p.id 
							LEFT JOIN tbl_orders oa ON oa.id   = vo.Order_id AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'
							WHERE p.id!=0 AND p.productname !='' ".$condnsearch." $search_name 
							GROUP BY p.productname ".$order_by.$limit;	
						break;
						case "Distributor":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							/*$sql="SELECT p.productname as Name,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_orders oa ON oa.catid   = p.catid AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name  
							GROUP BY p.productname ".$order_by.$limit;	*/
							$sql="SELECT p.productname as Name,
	                        sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
                            LEFT JOIN tbl_order_details vo ON vo.product_id = p.id 
							LEFT JOIN tbl_orders oa ON oa.id   = vo.Order_id AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'
							WHERE p.id!=0 AND p.productname !='' ".$condnsearch." $search_name 
							GROUP BY p.productname ".$order_by.$limit;						
						break;
					}	
				break;
			default:
				break;
		}
		
		$result1 = mysqli_query($this->local_connection,$sql);
				
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			if($sql2 != ''){
				$result2 = mysqli_query($this->local_connection,$sql2);
				$j = 1;
				$row_count2 = mysqli_num_rows($result2);
				if($row_count2 > 0){
					$name = array_column($records,'Name') ;
					
					while($row2 = mysqli_fetch_assoc($result2))
					{
						if(!in_array($row2['Name'],$name))
						{
							$records2[$j] = $row2;
							$j++;
						}
					}				
				}else{
					$records2 =0;
				}
			}
			if($records2 != 0)
			{
				$records = array_merge($records,$records2);				
			}
			return $records;
		}else{
			return 0;
		}
	}
	
	
	
	public function getReportTitleForSP($selTest=null,$frmdate=null,$todate=null){	
		$report_title = '';		
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$frmdate = date('d-m-Y',$frmdate);
					$todate = date('d-m-Y',$todate);
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	function getSPShopOrdersSummary($sp_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user u 				
				LEFT JOIN tbl_orders oa ON OA.ordered_by = u.id ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Superstockist":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user u 
				LEFT JOIN tbl_orders oa ON OA.ordered_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Distributor":		
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.product_quantity * vo.p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user u  
				LEFT JOIN tbl_orders oa ON OA.ordered_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	function getSPShopOrders($sp_id, $s_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";					
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";					
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";					
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_variant_id, vo.order_id, vo.product_quantity, vo.p_cost_cgst_sgst, vo.id as variant_oder_id, oa.order_date, VO.order_id, vo.productid, vo.campaign_sale_type
				FROM tbl_orders oa 				
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
				WHERE OA.ordered_by = $sp_id AND oa.shop_id = $s_id ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Superstockist":
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_variant_id, vo.order_id, vo.product_quantity, vo.p_cost_cgst_sgst, vo.id as variant_oder_id, oa.order_date, VO.order_id, vo.productid, vo.campaign_sale_type
				FROM tbl_orders oa 		
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id )
				WHERE OA.ordered_by = $sp_id AND vo.status != 1 AND oa.shop_id = $s_id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Distributor":	
				$sql="SELECT oa.brandnm, oa.categorynm, vo.product_variant_id, vo.order_id, vo.product_quantity, vo.p_cost_cgst_sgst, vo.id as variant_oder_id, oa.order_date, VO.order_id, vo.productid, vo.campaign_sale_type
				FROM tbl_orders oa 		
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
				WHERE OA.ordered_by = $sp_id AND oa.shop_id = $s_id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = '".$row['productid'] ."'";										
				$product_result = mysqli_query($this->local_connection,$sql_product);
				$obj_product = mysqli_fetch_object($product_result);
				$product_variant_id = $row['product_variant_id'];
				$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_variant_id' ";
				$resultprd = mysqli_query($this->local_connection,$sqlprd);
				$rowprd = mysqli_fetch_array($resultprd);
				$exp_variant1 = $rowprd['variant_1'];
				$imp_variant1= explode(',',$exp_variant1);
				$exp_variant2 = $rowprd['variant_2']; $imp_variant2= explode(',',$exp_variant2);
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit1 = $rowunit['unitname'];
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit2 = $rowunit['unitname'];
				$dimentionDetails = "";
				if($variant_unit1!="") {
					$dimentionDetails = " - ".$imp_variant1[0] . " " . $variant_unit1;//" .  $imp_variant1[0] . " 
				}
				//$dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
				$row['prodname_variant'] = $obj_product->productname . $dimentionDetails;
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function no_order_history_details($sp_id=null,$filter_date=null,$date1=null,$date2=null) {
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');					
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;				
				default:
					$condnsearch_sp="";
				break;

			}
		}
		if($sp_id!=''){
			$where = " AND salesperson_id = $sp_id";
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
					$sql1="SELECT `id`, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit 
					WHERE shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Superstockist":
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Distributor":	
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
		}
		//echo $sql1; exit;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	function getSalesPOrders_old($date,$salespid){
		 $sql = "SELECT	DISTINCT(VO.order_id), OA.shop_id, shops.name as shopname, shops.address, shops.mobile,
				OA.order_date , OA.categorynm, oplacelat, oplacelon, VO.order_id,
				(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
				(SELECT name FROM tbl_state WHERE id = shops.state) AS statename
				FROM tbl_orders OA 
				LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
				LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id				
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.ordered_by = " . $salespid."
				GROUP BY VO.order_id,
				OA.order_date
				ORDER BY OA.order_date ASC";
//GROUP BY OA.shop_id
				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrders($date,$salespid){
			 $sql = "(
						SELECT  DISTINCT(VO.order_id), OA.shop_id, shops.name as shopname, 
						shops.address, shops.mobile, OA.order_date , c.categorynm, 
						VO.order_id,VO.cat_id,
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						OA.order_date AS date,
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
						FROM tbl_orders OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id
						LEFT JOIN tbl_category c ON c.id= VO.cat_id 		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = $salespid
						GROUP BY VO.order_id,
						OA.order_date
						ORDER BY OA.order_date ASC
					)
					UNION  ALL
					(
						SELECT DISTINCT(VO.order_id), OA.shop_id,VO.cat_id, 
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname,
						shops.address, shops.mobile,
						OA.order_date , c.categorynm, VO.order_id,
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						SV.shop_visit_date_time AS date,						
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id AND OA.order_date =SV.shop_visit_date_time					
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id
						LEFT JOIN tbl_category c ON c.id= VO.cat_id 	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid
						ORDER BY SV.shop_visit_date_time ASC
					)
					ORDER BY date ASC";
//GROUP BY OA.shop_id
				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrdersProducts($date,$salespid)
	{
		 $sql = "SELECT DISTINCT(OV.product_variant_id), OV.product_variant_id, b.name, c.categorynm,OV.brand_id,OV.cat_id,
		(SELECT  productname FROM `tbl_product` WHERE id = OV.product_id) AS productname
		FROM `tbl_orders` AS OA 
		LEFT JOIN `tbl_order_details` AS OV ON OV.order_id = OA.id 
		LEFT JOIN `tbl_brand` AS b ON b.id = OV.brand_id
		LEFT JOIN `tbl_category` AS c ON c.id= OV.cat_id 
		WHERE
		date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.ordered_by = " . $salespid;
									
		$result = mysqli_query($this->local_connection,$sql);
		return $result;		
	}
	function getSalesPOrdersPQuantity($shop_id,$product_variant_id,$order_date)
	{
	    $sql = "SELECT OV.product_quantity AS quantity, OV.p_cost_cgst_sgst AS totalcost, OV.product_quantity AS variantunit FROM `tbl_orders` AS OA LEFT JOIN `tbl_order_details` AS OV ON OV.order_id = OA.id
		WHERE
		OV.product_variant_id = ".$product_variant_id." AND OA.shop_id = " . $shop_id." AND OA.order_date = '".$order_date."'";
									
		$result = mysqli_query($this->local_connection,$sql);
		$row = mysqli_fetch_assoc($result);
		return $row;		
	}
	function getSalesPOrdersDetails($orderRecId){
		$sql = "SELECT	shops.name as shopname, shops.address, shops.mobile,
				OA.order_date , OA.categorynm,
				(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
				(SELECT name FROM tbl_state WHERE id = shops.state) AS statename
				FROM tbl_orders OA 
				LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.ordered_by = " . $salespid."
				ORDER BY OA.order_date ASC";

				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrdersStart_old($date,$salespid){
		$sql = "SELECT	OA.order_date , VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
				FROM tbl_orders OA 
				LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id 
				LEFT JOIN tbl_shops shops ON shops.id= VO.shopid			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.ordered_by = " . $salespid."
				ORDER BY OA.order_date ASC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 	
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersEnd_old($date,$salespid){
		$sql = "SELECT	OA.order_date, VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
				FROM tbl_orders OA 
				LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id 
				LEFT JOIN tbl_shops shops ON shops.id= VO.shopid			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.ordered_by = " . $salespid."
				ORDER BY OA.order_date DESC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 				
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersStart($date,$salespid){				
				 $sql = "(
						SELECT  VO.id, shops.name as shopname, shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
						OA.order_date AS date
						FROM tbl_orders OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = " . $salespid."
						GROUP BY VO.order_id,
						OA.order_date
						ORDER BY OA.order_date ASC
					)
					UNION  ALL
					(
						SELECT VO.id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
						SV.shop_visit_date_time AS date		
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time						
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = " . $salespid."
						ORDER BY SV.shop_visit_date_time ASC
					)
					ORDER BY date ASC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 	
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersEnd($date,$salespid){
					$sql = "(
						SELECT  OA.id AS oid, VO.id, shops.name as shopname, shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
						OA.order_date AS date
						FROM tbl_orders OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = $salespid
						GROUP BY VO.order_id,
						OA.order_date
						ORDER BY OA.id DESC, OA.order_date DESC
					)
					UNION  ALL
					(
						SELECT OA.id AS oid, VO.id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,
						SV.shop_visit_date_time AS date		
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time						
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid
						ORDER BY SV.id DESC, SV.shop_visit_date_time DESC
					)
					ORDER BY date DESC, oid DESC  LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 				
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPStartEndDay($date,$salespid){
		$sql_day_time = "SELECT	`sp_id`, `tdate`, `presenty`, `dayendtime`
				FROM tbl_sp_attendance
				WHERE
				date_format(tdate, '%d-%m-%Y') = '".$date."' AND sp_id = " . $salespid;

		$result = mysqli_query($this->local_connection,$sql_day_time); 
		$totalRecords=mysqli_num_rows($result);
		if($totalRecords > 0)
					return mysqli_fetch_array($result);
				else
					return $totalRecords;	
	}
	function getSProductVariant($product_variant_id){
		$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_variant_id' ";
		$resultprd = mysqli_query($this->local_connection,$sqlprd);
		$rowprd = mysqli_fetch_array($resultprd);
		$exp_variant1 = $rowprd['variant_1'];
		$imp_variant1= explode(',',$exp_variant1);
		$exp_variant2 = $rowprd['variant_2']; $imp_variant2= explode(',',$exp_variant2);
		$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
		$resultunit = mysqli_query($this->local_connection,$sql);
		$rowunit = mysqli_fetch_array($resultunit);
		$variant_unit1 = $rowunit['unitname'];
		$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
		$resultunit = mysqli_query($this->local_connection,$sql);
		$rowunit = mysqli_fetch_array($resultunit);
		$variant_unit2 = $rowunit['unitname'];
		$dimentionDetails = "";
		/*if($imp_variant1[0]!="") {
			$dimentionDetails = " Pcs:  " .$imp_variant1[0] . " " . $variant_unit1;;//  $imp_variant1[0] //$variant_unit1;
		}*/
		if($variant_unit1!="") 
		{
			$dimentionDetails = " " .$imp_variant1[0]." ". $variant_unit1;//  $imp_variant1[0] //$variant_unit1;
		}
		//	$dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
		return $dimentionDetails;
	}

function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km')
	 {
	 	/*$latitude1 ='18.520430';
	 	$longitude1 ='73.856744';

	 	$latitude2 ='19.075984';
	 	$longitude2 ='72.877656'; Mumabi pune co-ordinates*/ 

     $theta = $longitude1 - $longitude2;
     $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
     $distance = acos($distance);
     $distance = rad2deg($distance);
     $distance = $distance * 60 * 1.1515; 
     switch($unit) {
          case 'Mi': break; case 'Km' : $distance = $distance * 1.609344;
     }
     return (round($distance,2));
}

	public function getLocation($latitude, $longitude){
		
		//$geolocation = $latitude.",".$longitude;//"18.4773911,73.9025829";//		
           $longitude1=$latitude;
           $latitude1= $longitude;
		$geolocation = $latitude1.",".$longitude1;
		//$geolocation ='48.8666667,2.3333333';
		//echo $geolocation; 
		$request = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$geolocation&sensor=false";
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);   

		//$output = file_get_contents($request);
		$json_decode = json_decode($output);
		//print"<pre>";
		//print_R($json_decode);
		$address = "";
		if(isset($json_decode->results[0])) {
			$response = array();
			$address = $json_decode->results[0]->formatted_address;
		}
		//echo $address."=="; 
		return $address;
			/*foreach($json_decode->results[0]->address_components as $addressComponet) {
				if(in_array('political', $addressComponet->types)) {
						$response[] = $addressComponet->long_name; 
				}
			}

			if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
			if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
			if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
			if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
			if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }

			if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
				echo "<br/>Address:: ".$first;
				echo "<br/>City:: ".$second;
				echo "<br/>State:: ".$fourth;
				echo "<br/>Country:: ".$fifth;
			}
			else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
				echo "<br/>Address:: ".$first;
				echo "<br/>City:: ".$second;
				echo "<br/>State:: ".$third;
				echo "<br/>Country:: ".$fourth;
			}
			else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) {
				echo "<br/>City:: ".$first;
				echo "<br/>State:: ".$second;
				echo "<br/>Country:: ".$third;
			}
			else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
				echo "<br/>State:: ".$first;
				echo "<br/>Country:: ".$second;
			}
			else if ( $first != 'null' && $second == 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
				echo "<br/>Country:: ".$first;
			}
		  }*/
	}

		
	public function getShopOrdersbyorderid($order_no) {
		
		/*$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$luser_id = $_SESSION[SESSION_PREFIX.'user_id'];
		$where="";		
		if($user_type=='Superstockist'){
			$where=" AND OA.superstockistid ='$luser_id' ";
		}
		if($user_type=='Distributor'){
			$where=" AND OA.distributorid='$luser_id' ";
		}*/
	
		  $sql = "SELECT					
					TP.id as id,	
					TP.productname as productname,
					date_format(o.order_date, '%M') as month,
					od.product_variant_weight1,
					o.shop_id,
					o.c_o_d,
					o.invoice_no,
					o.cod_percent,
					o.ordered_by,
					o.order_no,
					od.product_variant_unit1 as unit, 
					od.product_quantity as variantunit, 
					od.p_cost_cgst_sgst as unitcost
				FROM tbl_shops AS TS
				LEFT JOIN tbl_orders o ON o.shop_id = TS.id							
				LEFT JOIN tbl_order_details od ON od.order_id =o.id 
				LEFT JOIN tbl_product TP ON TP.id = od.product_id
				WHERE o.order_no = '$order_no' ";	
		
			
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}


		public function getOrders($order_status, $order_id = null){		
		extract($_POST);
		$where_clause_outer ='';
		$where_clause_inner ='';
		if($order_id != ''){
			$where_clause_outer .= " AND o.id = '".$order_id."' ";
		}
		if($frmdate != '')
		{
			$where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '".$frmdate."' ";
		}
		if($dropdownSalesPerson!="")
		{
			$where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
		}
		if($divShopdropdown !="")
		{
			$where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
		}
		if($subarea !="") {
			$where_clause_outer .= " AND s.subarea_id = " . $subarea;
		}
		if($dropdownCity !="")
		{
			$where_clause_outer .= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$where_clause_outer .= " AND s.state = " . $dropdownState;
		}
		if($dropdownCategory !="")
		{
			$where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
		}
		if($dropdownProducts !="")
		{
			$where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
		}
		 $order_sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE 1=1 $where_clause_outer
		ORDER BY shop_name, o.order_date DESC";//o.shop_order_status = ".$order_status."
		$orders_result = mysqli_query($this->local_connection,$order_sql);
		$row_orders_count = mysqli_num_rows($orders_result);
		$i = 1;
		$all_orders = array();
		$row_orders_det_count = 0;
		if($row_orders_count > 0){
			while($row_orders = mysqli_fetch_assoc($orders_result))
			{
				$all_orders[$i] = $row_orders;
				 $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				odis.discount_amount
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				WHERE od.order_id = ".$row_orders['id']." AND od.order_status = ".$order_status." $where_clause_inner";
				$orders_det_result = mysqli_query($this->local_connection,$order_details_sql);
				//$row_orders_det_count = mysqli_num_rows($orders_det_result);
				if(mysqli_num_rows($orders_det_result) > 0)
				{
					$j = 1;
					while($row_orders_det = mysqli_fetch_assoc($orders_det_result))
					{
						$all_orders[$i]['order_details'][$j] = $row_orders_det;
						$j++;
					}
					$i++;
				}
				else
				{
					unset ($all_orders[$i]); 
				}
			}
		}
		//print"<pre>";
		//print_r($all_orders);
		
		if(count($all_orders) == 0){
			$all_orders = array();
			return $all_orders;
		}
		else
			return $all_orders;
	}

public function getOrders_shopwise($order_status, $order_id = null)
{	
/*echo "hi";
exit();	*/
		extract($_POST);
        //echo $frmdate;
		$where_clause_outer ='';
		$where_clause_inner ='';
		if($order_id != ''){
			$where_clause_outer .= " AND o.id = '".$order_id."' ";
		}
		if($frmdate != '')
		{
			$where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '".$frmdate."' ";
		}
		if($dropdownSalesPerson!="")
		{
			$where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
		}
		if($divShopdropdown !="")
		{
			$where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
		}
		if($subarea !="") 
		{
			$where_clause_outer .= " AND s.subarea_id = " . $subarea;
		}
		if($dropdownCity !="")
		{
			$where_clause_outer .= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$where_clause_outer .= " AND s.state = " . $dropdownState;
		}
		if($dropdownCategory !="")
		{
			$where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
		}
		if($dropdownProducts !="")
		{
			$where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
		}

		switch($_SESSION[SESSION_PREFIX.'user_type'])
		 {
			case "Admin":
			     $sql = "SELECT o.order_date,o.order_no,s.name as shopnm,od.product_quantity,od.p_cost_cgst_sgst FROM `tbl_orders` AS o 
				 LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
                 LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
                 WHERE 1=1 $where_clause_outer AND od.order_status = ".$order_status."
				 ORDER BY o.order_date,shopnm,order_no ";
				break;
				case "Superstockist":	
				 $sql = "SELECT o.order_date,o.order_no,s.name as shopnm,od.product_quantity,od.p_cost_cgst_sgst FROM `tbl_orders` AS o 
				 LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
                 LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
                  WHERE od.status='2' AND superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ORDER BY o.order_date,shopnm,order_no";
				break;
				case "Distributor":									
				$sql = "SELECT o.order_date,o.order_no,s.name as shopnm,od.product_quantity,od.p_cost_cgst_sgst FROM `tbl_orders` AS o 
				 LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
                 LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
                  WHERE od.status = 1 AND distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ORDER BY o.order_date,shopnm,order_no ";
				break;
			}

              $orders_det_result = mysqli_query($this->local_connection,$sql);
              if(mysqli_num_rows($orders_det_result) > 0)
				{
					return $orders_det_result;
				}
				else
				{
					unset ($all_orders); 
				}             
               //return $row = mysqli_fetch_assoc($orders_det_result);
            //  var_dump( $orders_det_result);
             /* while($row1 = mysqli_fetch_array($orders_det_result)) 
									{
									  echo "<pre>";
                                      print_r($row);
									}              
              exit();*/
				if(count($all_orders) == 0)
				{
				$all_orders = array();
				return $all_orders;
				}
				else
				return $all_orders;
				

	}
	//new method for supp_chnz_status maintainance
	public function getOrdersSuppChnz($supp_chnz_status)
		{	
			  $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				odis.discount_amount
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				WHERE  od.supp_chnz_status = ".$supp_chnz_status." 
				group by od.product_id ";
			$orders_det_result = mysqli_query($this->local_connection,$order_details_sql);
			return $orders_det_result;
		}
	//new method for supp_chnz_status maintainance
	public function getOrderDetailsByproductvarId($prod_var_id){
		$sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  (SELECT s.name  FROM tbl_shops AS s WHERE s.id = o.shop_id) AS shop_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status, 
		od.id, od.order_id, 
		od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
		od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
		od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
		od.producthsn, od.product_variant_id, od.product_quantity, 
		od.product_variant_weight1, od.product_variant_unit1, 
		od.product_variant_weight2, od.product_variant_unit2, 
		od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
		od.campaign_applied, od.campaign_type, od.campaign_sale_type,
		od.order_status, od.delivery_assing_date, od.delivery_assign_to,
		od.transport_date, od.challan_no,od.vehicle_no, od.transport_mode, 
		od.date_time_supply, od.place_of_supply, od.quantity_delivered, od.delivery_date, 
		od.amount_paid, od.payment_date,
		odis.discount_amount
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
		LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
		WHERE od.product_variant_id = ".$prod_var_id;
		$orders_det_result = mysqli_query($this->local_connection,$sql);
		return $orders_det_result;
	}
	//new methods
	public function getOrderDetailsById($order_details_id){
		$sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  (SELECT s.name  FROM tbl_shops AS s WHERE s.id = o.shop_id) AS shop_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status, 
		od.id, od.order_id, 
		od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
		od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
		od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
		od.producthsn, od.product_variant_id, od.product_quantity, 
		od.product_variant_weight1, od.product_variant_unit1, 
		od.product_variant_weight2, od.product_variant_unit2, 
		od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
		od.campaign_applied, od.campaign_type, od.campaign_sale_type,
		od.order_status, od.delivery_assing_date, od.delivery_assign_to,
		od.transport_date, od.challan_no,od.vehicle_no, od.transport_mode, 
		od.date_time_supply, od.place_of_supply, od.quantity_delivered, od.delivery_date, 
		od.amount_paid, od.payment_date,
		odis.discount_amount
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
		LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
		WHERE od.id = ".$order_details_id;
		$orders_det_result = mysqli_query($this->local_connection,$sql);
		$row_orders_det_count = mysqli_num_rows($orders_det_result);
		return $row = mysqli_fetch_assoc($orders_det_result);
	}
	//new method 
	public function getOrders_toManage($order_received_status, $order_status=null, $order_id = null){		
		extract($_POST);
		$left_join = '';
		$where_clause ='';
		//print"<pre>"; print_r($_POST);
		if(isset($order_received_status) && $order_received_status != ''){			
			$where_clause.= " AND o.order_received_status = '".$order_received_status."' ";
		}
		if(isset($order_status) && $order_status != ''){	
			$status_part = explode('_',$order_status);
			$where_clause.= " AND od.order_status = '".$status_part[0]."' ";
		}
		if($order_id != ''){
			$where_clause.= " AND o.id = '".$order_id."' ";
		}
		if($frmdate != '')
		{
			$where_clause.= " AND date_format(o.order_date, '%d-%m-%Y') = '".$frmdate."' ";
		}
		if($divsstockistDropdown!="")
		{
			$where_clause .= " AND o.s_stockist = " . $divsstockistDropdown;
		}
		if($divstockistDropdown!="")
		{
			$where_clause .= " AND o.stockist = " . $divstockistDropdown;
		}
		if($dropdownSalesPerson!="")
		{
			$where_clause .= " AND o.ordered_by = " . $dropdownSalesPerson;
		}
		if($divShopdropdown !="")
		{
			$where_clause.= " AND o.shop_id = " . $divShopdropdown;
		}
		if($subarea !="") {
			$where_clause.= " AND s.subarea_id = " . $subarea;
		}
		if($dropdownCity !="")
		{
			$where_clause.= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$where_clause.= " AND s.state = " . $dropdownState;
		}
		if($dropdownCategory !="")
		{
			$where_clause.= " AND od.cat_id = " . $dropdownCategory;
		}
		if($dropdownProducts !="")
		{
			$where_clause.= " AND od.product_id = " . $dropdownProducts;
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']) {
			case "Admin":
					$sql = "SELECT od.order_id, o.order_date,o.order_no, o.shop_id, s.name as shopnm,
					od.product_quantity,od.p_cost_cgst_sgst 
					FROM `tbl_orders` AS o 
					LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
					LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
					WHERE 1=1 $where_clause
					ORDER BY o.order_date,shopnm,order_no ";
			break;
			case "Superstockist":	
				if($order_status != ''){					
					if($status_part[0] == 2){
						if($status_part[1] == 1){
							$left_join = " LEFT JOIN `tbl_order_delivery_flow` AS odf ON odf.order_id = o.id";
							$where_clause.= " AND odf.order_to_id = ".$_SESSION[SESSION_PREFIX.'user_id']." AND odf.assign_next=0";
						}else{
							$left_join = " LEFT JOIN `tbl_order_delivery_flow` AS odf ON odf.order_id = o.id";
							$where_clause.= " AND odf.order_from_id = ".$_SESSION[SESSION_PREFIX.'user_id']." AND odf.assign_next=0";
						}
					}
				}
			
					$sql = "SELECT od.order_id, o.order_date,o.order_no, o.shop_id, s.name as shopnm,
					od.product_quantity,od.p_cost_cgst_sgst 
					FROM `tbl_orders` AS o 
					LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
					LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
					$left_join
					WHERE o.s_stockist='".$_SESSION[SESSION_PREFIX.'user_id']."' 
					$where_clause
					ORDER BY o.order_date,shopnm,order_no";
			break;
			case "Distributor":		
				if($order_status != ''){					
					if($status_part[0] == 2){
						if($status_part[1] == 1){
							$left_join = " LEFT JOIN `tbl_order_delivery_flow` AS odf ON odf.order_id = o.id";
							$where_clause.= " AND odf.order_to_id = ".$_SESSION[SESSION_PREFIX.'user_id']." AND odf.assign_next=0";;
						}else{
							$left_join = " LEFT JOIN `tbl_order_delivery_flow` AS odf ON odf.order_id = o.id";
							$where_clause.= " AND odf.order_from_id = ".$_SESSION[SESSION_PREFIX.'user_id']." AND odf.assign_next=0";;
						}
					}
				}
				
					$sql = "SELECT od.order_id, o.order_date,o.order_no, o.shop_id, s.name as shopnm,
					od.product_quantity,od.p_cost_cgst_sgst 
					FROM `tbl_orders` AS o 
					LEFT JOIN `tbl_order_details` AS od ON od.order_id = o.id
					LEFT JOIN `tbl_shops` AS s ON s.id = o.shop_id
					$left_join
					WHERE o.stockist='".$_SESSION[SESSION_PREFIX.'user_id']."' 
					$where_clause
					ORDER BY o.order_date,shopnm,order_no ";
			break;
		}
		//echo $sql;
		$result1 = mysqli_query($this->local_connection,$sql);		
		return $result1;
	}
	//new method
	public function get_opening_balance($shop_id){		
		$sql_pending_amount = "SELECT ROUND(SUM(p_cost_cgst_sgst),2) AS amount_to_pay
		FROM `tbl_order_details` AS od
		LEFT JOIN `tbl_orders` AS o ON o.id = od.order_id
		WHERE o.shop_id = ".$shop_id." AND od.order_status IN (4,7,8)
		GROUP BY o.shop_id";
		$orders_payment_result = mysqli_query($this->local_connection,$sql_pending_amount);
		$row_payment_count = mysqli_num_rows($orders_payment_result);
		$row_payment = mysqli_fetch_assoc($orders_payment_result);
		
		$sql_paid_amount = "SELECT ROUND(SUM(paid_amount),2) AS amount_paid
		FROM `tbl_shop_payment` AS sp		
		WHERE sp.shop_id = ".$shop_id."
		GROUP BY sp.shop_id";
		$orders_paid_result = mysqli_query($this->local_connection,$sql_paid_amount);
		$row_paid_count = mysqli_num_rows($orders_paid_result);
		$row_paid = mysqli_fetch_assoc($orders_paid_result);
		//echo $row_payment['amount_to_pay']." - ".$row_paid['amount_paid'];
		$balance = array();
		$balance['amount_to_pay'] = $row_payment['amount_to_pay'];
		$balance['amount_paid'] =  $row_paid['amount_paid'];
		$balance['balance_amount'] = $row_payment['amount_to_pay'] - $row_paid['amount_paid'];
		if($balance['balance_amount'] > 0){
			return $balance;
		}else{
			return 0;
		}
	}
	
	public function generate_invoice_no(){
		$sql = "SELECT invoice_no FROM `tbl_orders` WHERE invoice_no != '' ORDER BY id DESC LIMIT 1 ";
		$result = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result);
		$row = mysqli_fetch_assoc($result);
		$part1 = 'SJFP';
		if(date("m") >= 4){
			$part2 = date('y').'-'.date("y",strtotime("+1 year"));
		}else{
			$part2 = date("y",strtotime("-1 year")).'-'.date('y');
		}
		$inc_number = 5000;
		if($row_count == 0){
			$inc_number = 5000;
		}else{
			$invoice_part =  explode('/',$row['invoice_no']);
			$inc_number = $invoice_part[2] + 1;
		}
		$part3 = $inc_number;
		$invoice_no = $part1.'/'.$part2.'/'.$part3;
		return $invoice_no;
	}
	//this method is used for old db orders and order details
	public function getAllOrders_old_instances() {
		extract($_POST);	
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($report_type){
			case "superstockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' $search_name
							GROUP BY u.firstname  ".$order_by.$limit;
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name
							GROUP BY u.firstname ".$order_by.$limit;
						break;
						 
						break;
					}
				break;
			case "stockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.distributorid = u.id ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Superstockist":						
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.distributorid = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' AND u.external_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						 
						break;
					}
				break;
			case "salesperson":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id, u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE u.user_type='SalesPerson' $search_name GROUP BY u.firstname ".$order_by.$limit;	
							
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (  '".$_SESSION[SESSION_PREFIX.'user_id']."') OR u.external_id IN (SELECT id FROM tbl_user where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."') ) 
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_order_app oa 
							LEFT JOIN tbl_user u  ON oa.order_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv, tbl_shops AS ss WHERE ss.id=sv.shop_id AND u.id = sv.salesperson_id AND (ss.stockist_id = ".$_SESSION[SESSION_PREFIX.'user_id'].") $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_order_app oa ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (".$_SESSION[SESSION_PREFIX.'user_id'].")  OR external_id LIKE ('%,". $_SESSION[SESSION_PREFIX.'user_id']."%'))
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_order_app oa 
							LEFT JOIN tbl_user u  ON oa.order_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name   AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
					}
				break;
			case "shop":	
					if($search != '')
						$search_name = " AND s.name LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":						
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) ".$condnsearch." 
							WHERE s.id!=0 AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE s.id!=0 AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !=''  $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_order_app oa ON shop_id = s.id  AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE s.id!=0  AND  s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
							
						break;
					}	
				break;
			case "product":	
					if($search != '')
						$search_name = " AND p.productname LIKE '%".$search."%'";
				switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":		
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid  ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))  AND vo.productid = p.id 
							WHERE p.id!=0 AND p.productname !=''  $search_name 
							GROUP BY p.productname ".$order_by.$limit;		
						break;
						case "Superstockist":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name 
							GROUP BY p.productname ".$order_by.$limit;		
						break;
						case "Distributor":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							$sql="SELECT p.productname as Name,sum(vo.variantunit * vo.totalcost) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_order_app oa ON oa.catid   = p.catid AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ".$condnsearch." 
							LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name  
							GROUP BY p.productname ".$order_by.$limit;							
						break;
					}	
				break;
			default:
				break;
		}
		
		
		
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			if($sql2 != ''){
				$result2 = mysqli_query($this->local_connection,$sql2);
				$j = 1;
				$row_count2 = mysqli_num_rows($result2);
				if($row_count2 > 0){
					$name = array_column($records,'Name') ;
					
					while($row2 = mysqli_fetch_assoc($result2))
					{
						if(!in_array($row2['Name'],$name))
						{
							$records2[$j] = $row2;
							$j++;
						}
					}				
				}else{
					$records2 =0;
				}
			}
			if($records2 != 0)
			{
				$records = array_merge($records,$records2);				
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function get_summary_for_mail(){		
		//$date = date('d-m-Y');
		$date =date('d-m-Y',strtotime("-1 days"));
					$condnsearch="  date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					
		 $sql="SELECT count(oa.id) as Todays_orders ,sum(vo.variantunit * vo.totalcost) as Todays_Sales 
				FROM tbl_order_app oa   
				LEFT JOIN tbl_variant_order vo ON (oa.id = vo.orderappid AND 
					(vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				where ".$condnsearch."";	
		$result = mysqli_query($this->local_connection,$sql);
		
		$sqltotalsale="SELECT count(oa.id) as Total_orders,sum(vo.variantunit * vo.totalcost) as Total_Sales 
				FROM tbl_variant_order vo 
				LEFT JOIN tbl_order_app oa ON vo.orderappid = oa.id";	
		$result5 = mysqli_query($this->local_connection,$sqltotalsale);
		$row5 = mysqli_fetch_assoc($result5);
		$records['Total_orders']=$row5['Total_orders'];
		$records['Total_Sales']=$row5['Total_Sales'];
		
		$sql2="select database() as manufacturer";
		$result2 = mysqli_query($this->local_connection,$sql2);
		$row2 = mysqli_fetch_assoc($result2);
		$output = str_replace("salzpoin_","",$row2['manufacturer']);
		
		$sql3="select count(s.id) as Total_shops_added_count from tbl_shops  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' ";
		$result3 = mysqli_query($this->local_connection,$sql3);
		$row3 = mysqli_fetch_assoc($result3);
		$records['Total_shops_added_count']=$row3['Total_shops_added_count'];
		
		$sql4="select count(s.id) as Total_shops_exists from tbl_shops  s ";
		$result4 = mysqli_query($this->local_connection,$sql4);
		$row4 = mysqli_fetch_assoc($result4);
		$records['Total_shops_exists']=$row4['Total_shops_exists'];
		
		//todays salesperson added
		$sql_todays_sp_count="select count(s.id) as todays_sp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='SalesPerson' ";
		$result_todays_sp_count = mysqli_query($this->local_connection,$sql_todays_sp_count);
		$row_todays_sp_count = mysqli_fetch_assoc($result_todays_sp_count);
		$records['todays_sp_count']=$row_todays_sp_count['todays_sp_count'];
		
		//total salesperson added till date
		$sql_total_sp_count="select count(s.id) as total_sp_count from tbl_user s where  user_type='SalesPerson'";
		$result_total_sp_count = mysqli_query($this->local_connection,$sql_total_sp_count);
		$row_total_sp_count = mysqli_fetch_assoc($result_total_sp_count);
		$records['total_sp_count']=$row_total_sp_count['total_sp_count'];
		
		
		//Todays_Sales
		$records['db']=$output;
		$row_count = mysqli_num_rows($result);		
		while($row = mysqli_fetch_assoc($result)){			
			$records['summary']['Todays_orders'] = $row['Todays_orders'];			
			$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}			
		return $records;
	}
	public function get_summary_for_mail_newdb(){		
		//$date = date('d-m-Y');
		$date =date('d-m-Y',strtotime("-1 days"));
		$condnsearch="  date_format(oa.order_date, '%d-%m-%Y') = '".$date."' ";
					
		 $sql="SELECT count(oa.id) as Todays_orders ,sum(vo.p_cost_cgst_sgst) as Todays_Sales 
				FROM tbl_orders oa   
				LEFT JOIN tbl_order_details vo ON oa.id = vo.order_id 
				where ".$condnsearch."";	
		$result = mysqli_query($this->local_connection,$sql);
		
		$sqltotalsale="SELECT count(oa.id) as Total_orders,sum(vo.p_cost_cgst_sgst) as Total_Sales 
				FROM tbl_order_details vo  
				LEFT JOIN tbl_orders oa ON  vo.order_id = oa.id";	
		$result5 = mysqli_query($this->local_connection,$sqltotalsale);
		$row5 = mysqli_fetch_assoc($result5);
		$records['Total_orders']=$row5['Total_orders'];
		$records['Total_Sales']=$row5['Total_Sales'];
		
		$sql2="select database() as manufacturer";
		$result2 = mysqli_query($this->local_connection,$sql2);
		$row2 = mysqli_fetch_assoc($result2);
		$output = str_replace("salzpoin_","",$row2['manufacturer']);
		
		
		$sql3="select count(s.id) as Total_shops_added_count from tbl_shops  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' ";
		$result3 = mysqli_query($this->local_connection,$sql3);
		$row3 = mysqli_fetch_assoc($result3);
		$records['Total_shops_added_count']=$row3['Total_shops_added_count'];
		
		$sql4="select count(s.id) as Total_shops_exists from tbl_shops  s ";
		$result4 = mysqli_query($this->local_connection,$sql4);
		$row4 = mysqli_fetch_assoc($result4);
		$records['Total_shops_exists']=$row4['Total_shops_exists'];
		
		//todays salesperson added
		$sql_todays_sp_count="select count(s.id) as todays_sp_count from tbl_user  s where date_format(s.added_on, '%d-%m-%Y') = '".$date."' and user_type='SalesPerson' ";
		$result_todays_sp_count = mysqli_query($this->local_connection,$sql_todays_sp_count);
		$row_todays_sp_count = mysqli_fetch_assoc($result_todays_sp_count);
		$records['todays_sp_count']=$row_todays_sp_count['todays_sp_count'];
		
		//total salesperson added till date
		$sql_total_sp_count="select count(s.id) as total_sp_count from tbl_user s where  user_type='SalesPerson'";
		$result_total_sp_count = mysqli_query($this->local_connection,$sql_total_sp_count);
		$row_total_sp_count = mysqli_fetch_assoc($result_total_sp_count);
		$records['total_sp_count']=$row_total_sp_count['total_sp_count'];
		
		$records['db']=$output;
		$row_count = mysqli_num_rows($result);		
		while($row = mysqli_fetch_assoc($result)){			
			$records['summary']['Todays_orders'] = $row['Todays_orders'];			
			$records['summary']['Todays_Sales'] = $row['Todays_Sales'];
		}			
		return $records;
	}
	public function get_distributor_address(){
		 $sql="SELECT address 
				FROM tbl_dealer where id > 16";	
		$result = mysqli_query($this->local_connection,$sql);		
		while($row = mysqli_fetch_assoc($result)){			
			$records[] = $row;
		}			
		return $records;
	}
}
?>