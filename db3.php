<?php
class Connection3{	
	private $host = DB_HOST3;
	private $database = WEBSITE_DB_DATABASE3;
	private $username = DB_USERNAME3;
	private $password = DB_PASSWORD3;
	
	private $link;
	private $result;
	public $sql;
	function __construct($database=''){
		if (!empty($database)){ $this->database = $database; }
		$this->link = mysql_connect($this->host,$this->username,$this->password);
		mysql_select_db($this->database, $this->link);
		return $this->link;  // returns false if connection could not be made.
	}
	function query($sql){
		if (!empty($sql)){
			$this->sql = $sql;
			$this->result = mysql_query($sql);
			return $this->result;
		}else{
			return false;
		}
	}
	function __destruct(){
		//mysql_close($this->link);
	}
	function close(){
		mysql_close($this->link);
	}
}
?>