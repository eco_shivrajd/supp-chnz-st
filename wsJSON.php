<?php
//error_reporting(E_ERROR | E_PARSE);
//ini_set('display_errors',1);
class wsJSON {
    private $connection;
	private $json_array = array("status" => array("responsecode"=> "1", "entity"=> "1"), "data" => array());
    function __construct($con) {
        $this->connection = $con;
    }
    function executeQuery($query) {
        $result = $this->connection->query($query) or die(mysql_error());
        return $result;
    }
    function addslashes_to_string($str) {
        $tempStr = addslashes(trim($str));
        return str_replace("\'", "", $tempStr);
        // return str_replace("\r\n","",str_replace("\'","'",$tempStr));
        //return str_replace("\r\n","",str_replace("\'","'",$tempStr));
    }	
    function GetLatitudeLongitude($geoaddress) {
        $geoaddress=urlencode($geoaddress);
        $geoaddress=str_replace("#", "", $geoaddress);
        $geoaddress=str_replace(" ", "+", $geoaddress);
        $geoaddress=str_replace("++", "+", $geoaddress);
            //echo "http://maps.googleapis.com/maps/api/geocode/json?address=".$geoaddress."&sensor=true";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/geocode/json?address=".$geoaddress."&sensor=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geocode = curl_exec($ch);
        $output2= json_decode($geocode);

        curl_close($ch);
        $latitude = $output2->results[0]->geometry->location->lat;
        $longitude = $output2->results[0]->geometry->location->lng;

        $coordinate=array($latitude,$longitude);
        return $coordinate;
    } 
	function fnSendInvitation($senderid,$receiverid){
		
		//check invitation already exists:	 
		$sqluser = "SELECT * FROM users WHERE uid='".$receiverid."'"; 
		$proRowuser = $this->executeQuery($sqluser);
		$rowuser = mysql_fetch_array($proRowuser);
		
		//contactssource
		$sql = "SELECT * from contactssource WHERE phone='".$rowuser['phone']."' AND feloze_uid='".$senderid."'";
		$result = $this->executeQuery($sql);
		$rowcount = mysql_num_rows($result);
		if ($rowcount > 0) { 
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['invitation']['msg'] = "Contact already exist in contacts.";
		}
		else 
		{
			$sql = "SELECT *  FROM tbl_invitation WHERE senderid='$senderid' AND receiverid='$receiverid' AND status!='4'";
			$proRow = $this->executeQuery($sql);
			if (mysql_num_rows($proRow) != 0) {
				$this->json_array['status']['responsecode'] = '1';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['invitation']['msg'] = "Invitation already sent.";
			}
			else
			{
				$sql = "INSERT INTO tbl_invitation(senderid,receiverid)VALUES('$senderid', '$receiverid') ";
				$proRow = $this->executeQuery($sql);
				$user_id = mysql_insert_id();
				if ($user_id) {
					$this->json_array['status']['responsecode'] = '0';
					$this->json_array['status']['entity'] = '1';
					$this->json_array['data']['invitation']['msg'] = "Invitation sent successfully.";
				}
				else
				{
					$this->json_array['status']['responsecode'] = '0';
					$this->json_array['status']['entity'] = '1';
					$this->json_array['data']['invitation']['msg'] = "Unsuccessful.";
				}
			}
		}
		return json_encode($this->json_array);
	}
	function fnplaceorder($total_order_cost,$total_order_gst_cost,$sale_count,$free_count,$total_count){
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['invitation']['msg'] = "Order Placed Successfully.";
		$this->json_array['data']['total_cost'] = "$total_order_cost";
		$this->json_array['data']['total_gst_cost'] = "$total_order_gst_cost";
		$this->json_array['data']['total_quantity'] = "$sale_count";
		$this->json_array['data']['sale_quantity'] = "$free_count";
		$this->json_array['data']['free_quantity'] = "$total_count";
		return json_encode($this->json_array);
	}
	function get_userdata($userid) {
		
		$sql = " SELECT 
		`id`, 
		`external_id`, 
		`surname`, 
		`firstname`, 
		`username`, 
		`pwd`, 
		`user_type`, 
		`address`, 
		`city`, 
		`state`, 
		`mobile`, 
		`email`, 
		`reset_key`, 
		`suburbid`,
		`state_ids`, `city_ids`, `suburb_ids`, `subarea_ids`, `sstockist_id`
		FROM  `tbl_user` 

		LEFT JOIN tbl_user_working_area ON tbl_user.id = tbl_user_working_area.user_id

		WHERE id='$userid'";
		
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '6';
			
			$super_stockist_array=$this->fngetOwners($row['sstockist_id'],'Superstockist');
			if($super_stockist_array!=0){
				$this->json_array['data']['Login']['superstockistid'] = $super_stockist_array['id'];
				$this->json_array['data']['Login']['superstockistname'] = $super_stockist_array['firstname'];
				$this->json_array['data']['Login']['superstockistmobile'] = $super_stockist_array['mobile'];
			}else{
				$this->json_array['data']['Login']['superstockistid'] = 0;
				$this->json_array['data']['Login']['superstockistname'] = '';
				$this->json_array['data']['Login']['superstockistmobile'] = '';
			}
			$external_ids = explode(',',$row['external_id']);
			$count_external_id = count($external_ids);
			$stockistname_list="";
			if($count_external_id == 1){
				$stockist_array=$this->fngetOwners($external_ids[0],'Distributor');				
				$this->json_array['data']['Login']['stockist'][0]['stockistid'] = $stockist_array['id'];
				$this->json_array['data']['Login']['stockist'][0]['stockistname'] = $stockist_array['firstname'];
				$this->json_array['data']['Login']['stockist'][0]['stockistmobile'] = $stockist_array['mobile'];
				$stockistname_list.="".$stockist_array['firstname'].", ";
			}else if($count_external_id > 1){
				for($i=0; $i < $count_external_id; $i++){
					$stockist_array=$this->fngetOwners($external_ids[$i],'Distributor');	
					$this->json_array['data']['Login']['stockist'][$i]['stockistid'] = $stockist_array['id'];
					$this->json_array['data']['Login']['stockist'][$i]['stockistname'] = $stockist_array['firstname'];
					$this->json_array['data']['Login']['stockist'][$i]['stockistmobile'] = $stockist_array['mobile'];
					$stockistname_list.="".$stockist_array['firstname'].", ";
				}
			}else{
				$this->json_array['data']['Login']['stockistid'] = 0;
				$this->json_array['data']['Login']['stockistname'] = '';
				$this->json_array['data']['Login']['stockistmobile'] = '';
			}
			
			/*
			$suparr=$this->fngetOwners($row['external_id'],'Superstockist');
			if($suparr!=0){
				$this->json_array['data']['Login']['superstockistid'] = $suparr['id'];
				$this->json_array['data']['Login']['superstockistname'] = $suparr['firstname'];
				//Get Stockist
				$sqlstockist = "SELECT external_id  FROM `tbl_user` WHERE id='".$suparr['id']."'";
				$proRowstockist = $this->executeQuery($sqlstockist);
				$stockistarr=$this->fngetOwners($proRowstockist['external_id'],'Distributor');
				
				if($stockistarr!=0){
					$this->json_array['data']['Login']['stockistid'] = $stockistarr['id'];
					$this->json_array['data']['Login']['stockistname'] = $stockistarr['firstname'];
					$super_stockist_mobile = $stockistarr['mobile'];
				}
				else{
					$this->json_array['data']['Login']['stockistid'] = 0;
					$this->json_array['data']['Login']['stockistname'] = '';
				}
				//end
			}
			else
			{
				$this->json_array['data']['Login']['superstockistid'] = 0;
				$this->json_array['data']['Login']['superstockistname'] = '';
				//Get Stockist
				$stockistarr=$this->fngetOwners($row['external_id'],'Distributor');
				if($stockistarr!=0){
					$this->json_array['data']['Login']['stockistid'] = $stockistarr['id'];
					$this->json_array['data']['Login']['stockistname'] = $stockistarr['firstname'];
					$stockist_mobile = $stockistarr['mobile'];
				}
				else {
					$this->json_array['data']['Login']['stockistid'] = 0;
					$this->json_array['data']['Login']['stockistname'] = '';
				} 
			}*/		
			$stockistname_list=rtrim($stockistname_list,', ');
			$this->json_array['data']['Login']['id'] = $row['id'];
			$this->json_array['data']['Login']['external_id'] = $row['external_id'];
			$this->json_array['data']['Login']['external_id_names'] = $stockistname_list;
			$this->json_array['data']['Login']['sstockist_id'] = $row['sstockist_id'];
			$this->json_array['data']['Login']['username'] = $row['username'];
			$this->json_array['data']['Login']['firstname'] = $row['firstname'];
			$this->json_array['data']['Login']['surname'] = $row['surname'];
			$this->json_array['data']['Login']['address'] = $row['address'];
			$this->json_array['data']['Login']['email'] = $row['email'];
			$this->json_array['data']['Login']['mobile'] = $row['mobile'];
			
			/********* get city, state, suburbid************/ 		
			
			 
			$state=$this->get_stateid($row['state']);
			$this->json_array['data']['Login']['stateid'] = $state['id'];
			$this->json_array['data']['Login']['statenm'] = $state['name'];
			$city=$this->get_cityid($row['city']);
			$this->json_array['data']['Login']['cityid'] = $city['id'];
			$this->json_array['data']['Login']['citynm'] = $city['name'];
			
			/*$suburb=$this->get_suburbid($row['suburbid']);
			$this->json_array['data']['Login']['suburbid'] = $suburb['id'];
			$this->json_array['data']['Login']['suburbnm'] = $suburb['suburbnm']; */
			
			/*$this->json_array['data']['Login']['stateid'] = $row['state'];
			$this->json_array['data']['Login']['cityid'] = $row['city'];*/
			
			
			
			
			$suburb_ids = "";
			$suburbnms = "";
			if($row['suburb_ids']!="") {
				$sql = "SELECT id , suburbnm FROM tbl_surb WHERE id in(".$row['suburb_ids'].") ";
				$proRow = $this->executeQuery($sql);
				while($row1 = mysql_fetch_array($proRow))
				{
					if($suburb_ids=="")
						$suburb_ids = $row1["id"];
					else 
						$suburb_ids .= ",". $row1["id"];
					
					if($suburbnms=="")
						$suburbnms = $row1["suburbnm"];
					else 
						$suburbnms .= ",". $row1["suburbnm"];
				}
			}
			$this->json_array['data']['Login']['suburbids'] = $suburb_ids;
			$this->json_array['data']['Login']['suburbnms'] = $suburbnms;
			
			$subareaids = "";
			$subareaname = "";
			if($row['subarea_ids']!="") {
				$sql = "SELECT subarea_id , subarea_name FROM tbl_subarea WHERE subarea_id in(".$row['subarea_ids'].") ";
				$proRow = $this->executeQuery($sql);
				while($row1 = mysql_fetch_array($proRow))
				{
					if($subareaids=="")
						$subareaids = $row1["subarea_id"];
					else 
						$subareaids .= ",". $row1["subarea_id"];
					
					if($subareaname=="")
						$subareaname = $row1["subarea_name"];
					else 
						$subareaname .= ",". $row1["subarea_name"];
				}
			}
			$this->json_array['data']['Login']['subareaids'] = $subareaids;
			$this->json_array['data']['Login']['subareaname'] = $subareaname;
		}
		else {
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['Login']['message'] = "Invalid userid.";
		}
		return json_encode($this->json_array);
	}
	function fngetOwners($id,$role){
		$sql = "SELECT *  FROM `tbl_user` WHERE id='$id' AND user_type='".$role."'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			return $row;
		}
		else{
			return 0;
		}
	}
	function get_login($uname, $password) {
		$password = md5($password);
		$sql = "SELECT *  FROM `tbl_user` WHERE username='$uname' AND pwd='$password'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			 $this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '6';
				$this->json_array['data']['Login']['id'] = $row['id'];
				$this->json_array['data']['Login']['username'] = $row['username'];
				$this->json_array['data']['Login']['firstname'] = $row['firstname'];
				$this->json_array['data']['Login']['surname'] = $row['surname'];
				$this->json_array['data']['Login']['address'] = $row['address'];
				$this->json_array['data']['Login']['email'] = $row['email'];
		}
		else {
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['Login']['message'] = "Invalid username or password.";
		}
		return json_encode($this->json_array);
	}
	function get_login_m($uname, $password) {
		$password = md5($password);
		$sql = "SELECT *  FROM `tbl_users` WHERE username='$uname' AND passwd='$password' AND level='SalesPerson'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$sqlcom = "SELECT *  FROM tbl_user_company WHERE userid='".$row['id']."'";
			$proRowcom = $this->executeQuery($sqlcom);
			$rowcom = mysql_fetch_array($proRowcom);
			 $this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '6';
				$this->json_array['data']['Login']['id'] = $row['id'];
				$this->json_array['data']['Login']['username'] = $row['username'];
				$this->json_array['data']['Login']['firstname'] = $row['firstname'];
				$this->json_array['data']['Login']['surname'] = $row['surname'];
				$this->json_array['data']['Login']['address'] = $row['address'];
				$this->json_array['data']['Login']['email'] = $row['email'];
		}
		else {
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['Login']['message'] = "Invalid username or password.";
		}
		return json_encode($this->json_array);
	}
	function fnGetProductCat(){
		$sql = "SELECT * FROM pcatbrandvariant1";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		while($row = mysql_fetch_array($proRow))
		{
			$p_array_temp['brand_name'] = $row['brand_name'];
			$p_array_temp['category_name'] = $row['category_name'];
			
			if($row['category_image']!=''){
				$p_array_temp['category_image'] = SITEURL."/templates/admin/upload/".$row['category_image'];
			} else {
				$p_array_temp['category_image'] = "";
			}
			
			$p_array_temp['product_name'] = $row['product_name'];
			$p_array_temp['product_price'] = $row['product_price'];
			$p_array_temp['product_hsn'] = $row['product_hsn'];
			$p_array_temp['product_cgst'] = $row['product_cgst'];
			$p_array_temp['product_sgst'] = $row['product_sgst'];
			if($row['product_barcode_img']!=''){
				$p_array_temp['product_barcode_img'] = SITEURL."/templates/admin/upload/".$row['product_barcode_img'];
			} else {
				$p_array_temp['product_barcode_img'] = "";
			}
			$p_array_temp['brand_id'] = $row['brand_id'];
			$p_array_temp['category_id'] = $row['category_id'];
			$p_array_temp['product_id'] = $row['product_id'];//variant row
			$p_array_temp['product_variant_rowcnt'] = $row['product_variant_id'];
			$variant1arr=explode(",",$row['product_variant1']);
			
			$weightunit=$this->fnGetUnit($variant1arr[1]);
			$p_array_temp['weight'] = $variant1arr[0];
			$p_array_temp['weightunit'] = $weightunit;

			$variant2arr=explode(",",$row['product_variant2']);
			$sizeunit=$this->fnGetUnit($variant2arr[1]);
			$p_array_temp['size'] = $variant2arr[0];
			$p_array_temp['sizeunit'] = $sizeunit;

			$this->json_array['data']['procats'][]=$p_array_temp;
		}
		$this->json_array['data']['campaigns'] = $this->get_campaigndata();
		return json_encode($this->json_array);
	}
	function fnGetUnit($id){
		$sql="SELECT unitname FROM tbl_units WHERE id='".$id."'";
		$proRow = $this->executeQuery($sql);
		$row = mysql_fetch_array($proRow);
		return $row['unitname'];
	}
	function fnProfileUpdate($profileid,$name,$address,$state,$city,$suburbids,$subareaids,$mobile,$external_id){
		
		$sql="UPDATE tbl_user SET
		firstname='$name',address='$address',mobile='$mobile', 
		external_id='$external_id'
		where id='$profileid'";
		//,state='$state',city='$city'
		
		$proRow = $this->executeQuery($sql);
		$external_ids = explode(',', $external_id);
		$stockistids = join("','",$external_ids);
		 $sql="SELECT state_ids,city_ids,suburb_ids,subarea_ids FROM tbl_user_working_area WHERE user_id in ('".$stockistids."')";
		$proRow = $this->executeQuery($sql);
		$state_ids1="";
		$city_ids1="";
		$suburb_ids1="";
		$subarea_ids1="";
		while($row = mysql_fetch_array($proRow))
		{
			$state_ids1=$row['state_ids'].', ';
			$city_ids1=$row['city_ids'].', ';
			$suburb_ids1.=$row['suburb_ids'].', ';
			$subarea_ids1.=$row['subarea_ids'].', ';
		}
		$state=rtrim($state_ids1,", ");
		$city=rtrim($city_ids1,", ");
		$suburbids=rtrim($suburb_ids1,", ");
		$subareaids=rtrim($subarea_ids1,", ");
		
		
		$state = implode(', ', array_unique(explode(', ', $state)));
		$city = implode(', ', array_unique(explode(', ', $city)));
		$suburbids = implode(', ', array_unique(explode(', ', $suburbids)));
		$subareaids = implode(', ', array_unique(explode(', ', $subareaids)));
		
		
		$sql="SELECT user_id FROM tbl_user_working_area WHERE user_id='".$profileid."'";	 
		$result = $this->executeQuery($sql);
		$rowcount = mysql_num_rows($result);

		if($rowcount  >0){
		
			$sql = "UPDATE tbl_user_working_area SET 
				state_ids = '$state_ids' , 
				city_ids = '$city_ids' , 
				suburb_ids='$suburbids' , 
				subarea_ids = '$subareaids' 
			WHERE user_id = '$profileid'";
		} else {
			$sql="INSERT INTO `tbl_user_working_area`( `user_id`, `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` ) 
			VALUES  ( '$profileid', '$state_ids', '$city_ids', '$suburbids','$subareaids' )";
		}
		// echo $sql;
		 
		$proRow = $this->executeQuery($sql);
		$sqlstockistnames="SELECT firstname FROM tbl_user WHERE id in ('".$stockistids."')";
		$resultstockistnames = $this->executeQuery($sqlstockistnames);
		$stockisnames="";
		while($rowssnames = mysql_fetch_array($resultstockistnames))
		{
			$stockisnames.=$rowssnames['firstname'].", ";
		}
		$stockisnames=rtrim($stockisnames,', ');
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '7';
		$this->json_array['data']['Profile']['id'] = $profileid;
		$this->json_array['data']['Profile']['external_id'] = $external_id;
		$this->json_array['data']['Profile']['external_id_names'] = $stockisnames;
		$this->json_array['data']['Profile']['firstname'] = $name;
		$this->json_array['data']['Profile']['address'] = $address;
		$this->json_array['data']['Profile']['state'] = $state;
		$this->json_array['data']['Profile']['city'] = $city;
		$this->json_array['data']['Profile']['suburbids'] = $suburbids;
		$this->json_array['data']['Profile']['subareaids'] = $subareaids;
		$this->json_array['data']['Profile']['mobile'] = $mobile;
		
		return json_encode($this->json_array);
	}
	function fnProfileUpdatemain($profileid,$name,$address,$state,$city,$mobile){
		
		$sql="UPDATE tbl_users SET firstname='$name',address='$address',state='$state',city='$city' , mobile='$mobile' where id='$profileid'";
		$proRow = $this->executeQuery($sql);
		 
	}
	function fnApplyLeave($id,$reasonleave,$description,$leavedt){
		//tbl_salesperson_leave
		$sql="INSERT INTO tbl_salesperson_leave(salespersonid,reason,description,leavedt)VALUES('".$id."','".$reasonleave."','".$description."','".$leavedt."')";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['leave']['msg'] = "Leave application sent successfully.";
		return json_encode($this->json_array);
	}
	function get_state(){
	 $sql = "SELECT id, name FROM tbl_state WHERE country_id='101'";
	  $proRow = $this->executeQuery($sql);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '9';
	  while($row = mysql_fetch_array($proRow)){
		  $s_array_temp['name'] = $row['name'];
		  $s_array_temp['id'] = $row['id'];
		  $this->json_array['data']['state'][]=$s_array_temp;
	  }
	  return json_encode($this->json_array);  
	}
	function get_city($stateid){
		$sqlcity = "SELECT id, name,state_id FROM  tbl_city WHERE state_id='".$stateid."'";
	  $proRowcity = $this->executeQuery($sqlcity);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '3';
	   while($rowcity = mysql_fetch_array($proRowcity)){
		  $c_array_temp['name'] = $rowcity['name'];
		  $c_array_temp['id'] = $rowcity['id'];
		  $c_array_temp['state_id'] = $rowcity['state_id'];
		  $this->json_array['data']['city'][]=$c_array_temp;
	  }
	  return json_encode($this->json_array);
	}
	function get_suburb($cityid){
		$sqlcity = "SELECT id,suburbnm FROM tbl_surb WHERE cityid='".$cityid."' AND isdeleted!='1'";
	  $proRowcity = $this->executeQuery($sqlcity);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '2';
	   while($rowcity = mysql_fetch_array($proRowcity)){
		  $c_array_temp['suburbnm'] = $rowcity['suburbnm'];
		  $c_array_temp['id'] = $rowcity['id'];
		  $this->json_array['data']['suburb'][]=$c_array_temp;
	  }
	  return json_encode($this->json_array);
	}
	/********************* City, state and suburb by id ***************************/
	function get_stateid($id){
	  $sql = "SELECT id, name FROM tbl_state WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '2';
	  $row = mysql_fetch_array($proRow);
	  return $row;  
	}
	function get_cityid($cityid){
	$sqlcity = "SELECT id, name FROM  tbl_city WHERE id='".$cityid."'";
	$proRowcity = $this->executeQuery($sqlcity);
	$rowcity = mysql_fetch_array($proRowcity);
	 return $rowcity;
	}
	function get_suburbid($suburbid){
	$sqlcity = "SELECT id,suburbnm FROM tbl_surb WHERE id='".$suburbid."'";
	$proRowcity = $this->executeQuery($sqlcity);
	$rowsuburb = mysql_fetch_array($proRowcity);
	return $rowsuburb;
	}
	/*******************************************************************************/
	function fnAddNewStore($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$distributorid,$suburbid,$subarea_id) {
		
		$sql = " SELECT 
		`external_id`		
		FROM  `tbl_user` 		
		WHERE id='$distributorid'";
		
		$proRowUser = $this->executeQuery($sql);
		$row = mysql_fetch_array($proRowUser);
		$sup_stockist = $row['external_id'] ;
		
		/*Get Stockist's State, District, Taluka assign to shop*/
		$sql_wa = " SELECT 
		 `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` 	
		FROM  `tbl_user_working_area` 		
		WHERE user_id='$distributorid'";
		
		$user_wa = $this->executeQuery($sql_wa);
		$user_wa_details = mysql_fetch_array($user_wa);
		$state = $user_wa_details['state_ids'];
		$city = $user_wa_details['city_ids'];
		$suburbid = $user_wa_details['suburb_ids'];
		$subarea_id = $user_wa_details['subarea_ids'];
		
		$sql="INSERT INTO tbl_shops(name, address, city, state, contact_person, mobile,shop_added_by,latitude,longitude,suburbid,subarea_id,sstockist_id,stockist_id)VALUES('".$store_name."','".$store_Address."','".$city."','".$state."','".$owner_name."','".$contact_no."','".$salespersonid."','".$lat."','".$lng."','".$suburbid."','".$subarea_id."','".$sup_stockist."','".$distributorid."')";
		
		$proRow = $this->executeQuery($sql);
		$last_id = mysql_insert_id();		
		
		$sql="INSERT INTO tbl_shop_assignedto_users(shop_id, user_id) VALUES ( '".$last_id."','".$distributorid."' )";
		$proRow = $this->executeQuery($sql);
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Store']['msg'] = "Store added successfully.";
		$this->json_array['data']['Store']['id'] = $last_id;
		
		return json_encode($this->json_array);
	}
	function fnAddNewStorecommon($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$shop_type_id,$distributorid,$suburbid,$subarea_id) {
		
		$sqls = " SELECT 
		`external_id`		
		FROM  `tbl_user` 		
		WHERE id='$salespersonid'";
		
		$proRowUsers = $this->executeQuery($sqls);
		$rows = mysql_fetch_array($proRowUsers);
		$stockist_id = $rows['external_id'] ;
		
		$sqlss = " SELECT 
		`external_id`		
		FROM  `tbl_user` 		
		WHERE id='$stockist_id'";
		
		$proRowUserss = $this->executeQuery($sqlss);
		$rowss = mysql_fetch_array($proRowUserss);
		$sup_stockist = $rowss['external_id'] ;
		
		/*Get Stockist's State, District, Taluka assign to shop*/
		$sql_wa = " SELECT 
		 `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` 	
		FROM  `tbl_user_working_area` 		
		WHERE user_id='$stockist_id'";
		
		//$user_wa = $this->executeQuery($sql_wa);
		//$user_wa_details = mysql_fetch_array($user_wa);
        //$state = $user_wa_details['state_ids'];
		//$city = $user_wa_details['city_ids'];
		//$suburbid = $user_wa_details['suburb_ids'];
        //$subarea_id = $user_wa_details['subarea_ids'];
		
		 $sql="INSERT INTO tbl_shops(name, address, city, state, contact_person, mobile,shop_added_by,latitude,longitude,suburbid,subarea_id,sstockist_id,stockist_id,shop_type_id)VALUES('".$store_name."','".$store_Address."','".$city."','".$state."','".$owner_name."','".$contact_no."','".$salespersonid."','".$lat."','".$lng."','".$suburbid."','".$subarea_id."','".$sup_stockist."','".$stockist_id."','".$shop_type_id."')";
		
		$proRow = $this->executeQuery($sql);
		$last_id = mysql_insert_id();		
		
		$sql="INSERT INTO tbl_shop_assignedto_users(shop_id, user_id) VALUES ( '".$last_id."','".$stockist_id."' )";
		$proRow = $this->executeQuery($sql);
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Store']['msg'] = "Store added successfully.";
		$this->json_array['data']['Store']['id'] = $last_id;
		
		return json_encode($this->json_array);
	}
	function get_shoplist($userid) {
		
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '9';
	  	  
	  $sql = "SELECT GROUP_CONCAT(external_id) AS all_external_id 
	  FROM tbl_user WHERE id = '".$userid."'";
		
	  $extIdResult 	= $this->executeQuery($sql);
	  $rowcount = mysql_num_rows($extIdResult);
	  $extIdRow 	= mysql_fetch_array($extIdResult);
		
		if($rowcount  >0 && $extIdRow['all_external_id']!=""){	
			 
		  $sql = " SELECT 
			tbl_shops.id,  tbl_shops.name,  tbl_shops.address,  
			tbl_shops.city,  tbl_shops.state,  tbl_shops.suburbid, 
			tbl_shops.contact_person,  tbl_shops.mobile,  tbl_shops.shop_added_by, 
			tbl_shops.latitude,  tbl_shops.longitude,  closedday,  
			opentime,  closetime,  
			tbl_shops.gst_number, tbl_shops.subarea_id, subarea_name, tbl_shops.state, tbl_state.name as state_name, tbl_shops.city, tbl_city.name as city_name, tbl_surb.suburbnm as suburb  , tbl_shops.stockist_id , tbl_user.firstname       
		FROM tbl_shops     
		LEFT JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id     
		LEFT JOIN tbl_state ON tbl_state.id = tbl_shops.state     
		LEFT JOIN tbl_city ON tbl_city.id = tbl_shops.city     
		LEFT JOIN  tbl_user ON  tbl_user.id = tbl_shops.stockist_id     
		LEFT JOIN tbl_surb ON tbl_surb.id = tbl_shops.suburbid  WHERE tbl_shops.stockist_id IN (".$extIdRow['all_external_id'].") AND tbl_shops.isdeleted!='1' ";
		
		$proRow = $this->executeQuery($sql);
		 while($row = mysql_fetch_array($proRow))
		  {	  
			$shop_array_temp['id'] = $row['id'];
			$shop_array_temp['name'] = $row['name'];
			$shop_array_temp['address'] = $row['address'];
			$shop_array_temp['contact_person'] = $uname;
			$shop_array_temp['lat'] = $row['latitude'];
			$shop_array_temp['lon'] = $row['longitude'];
			$shop_array_temp['closedday'] = $this->fnGetday($row['closedday']);
			$shop_array_temp['opentime'] = $row['opentime'];
			$shop_array_temp['closetime'] = $row['closetime'];
			$shop_array_temp['mobile'] = $row['mobile'];
			$shop_array_temp['gst_number'] = $row['gst_number'];	
			$shop_array_temp['city_id'] = $row['city'];
			$shop_array_temp['city'] = $row['city_name'];
			$shop_array_temp['state_id'] = $row['state'];
			$shop_array_temp['state'] = $row['state_name'];	
			$shop_array_temp['suburb_id'] = $row['suburbid'];
			$shop_array_temp['suburb'] = $row['suburb'];	
			$shop_array_temp['subarea_id'] = $row['subarea_id'];
			$shop_array_temp['subarea_name'] = $row['subarea_name'];
			$shop_array_temp['destributorid'] = $row['stockist_id'];
			$shop_array_temp['destributorname'] = $row['firstname'];
			
			
			$this->json_array['data']['shops'][]=$shop_array_temp;	
		  }
		
		}else{
			 $this->json_array['data']['shops'][]="Stockist not available";	
		}
	  return json_encode($this->json_array);
	}
	function fnGetday($dayid) {
		$str="";
		switch($dayid){
			case '1':
				$str="Monday";
			break;
			case '2':
				$str="Tuesday";
			break;
			case '3':
				$str="Wednesday";
			break;
			case '4':
				$str="Thursday";
			break;
			case '5':
				$str="Friday";
			break;
			case '6':
				$str="Saturday";
			break;
			case '7':
				$str="Sunday";
			break;
		}
		return $str;
	}
	function fngetStatenm($id){
	  $sql = "SELECT `id`,`name` FROM tbl_state WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $row = mysql_fetch_array($proRow);
	  return $row['name'];
	}
	function fngetCitynm($id){
	  $sql = "SELECT `id`,`name` FROM tbl_city WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $row = mysql_fetch_array($proRow);
	  return $row['name'];
	}
	function fngetSuburbnm($id){
	  $sql = "SELECT `suburbnm` FROM  tbl_surb WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $row = mysql_fetch_array($proRow);
	  return $row['suburbnm'];
	}
	function get_orderdata($userid,$pageid){
		$npageid=$pageid*10;
		if($pageid=='1')
			$limit=0;
		else{
			$limit=($pageid-1)*10;
		} 
		  $sql = "select shops.name as shopname,OA.shop_id, OA.order_date,  
						OA.order_no, VO.product_variant_weight1,
						VO.product_variant_unit1,
						SUM(VO.p_cost_cgst_sgst) as totalcost,
						count(VO.order_id) as variantunit, 
		IF(OA.offer_provided = 0,'Offer Applied','Offer Not Applied') AS offer_provided_val, VO.cat_id

		FROM `tbl_order_details` VO
		LEFT JOIN tbl_orders OA on OA.id=VO.order_id
		LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
		WHERE 
		OA.ordered_by='".$userid."' AND DATE_FORMAT(OA.order_date,'%Y-%m-%d')=DATE(NOW())
		GROUP BY VO.order_id limit $limit,$npageid ";//
		$proRow = $this->executeQuery($sql);

		if($rowcount = mysql_num_rows($proRow)>0){
			$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '9';
		while($row = mysql_fetch_array($proRow)){
			$tcost=$row['totalcost']; 
	 
				$sql_free_pro = "SELECT COUNT(tbl_variant_order.id) as quantity_free FROM `tbl_variant_order` INNER JOIN `tbl_order_app` ON tbl_order_app.id = tbl_variant_order.orderappid WHERE campaign_sale_type='free' AND OA.cat_id = ".$row['cat_id'];
				$result_free_pro = mysqli_query($con,$sql_free_pro);
				$row_free_pro = mysqli_fetch_assoc($result_free_pro);
				$free_quantity = 0;
				$sale_quantity = $row['variantunit'];
				if($row_free_pro['quantity_free'] > 0)
				{
					$free_quantity = $row_free_pro['quantity_free'];
					$sale_quantity = $row['variantunit'] - $free_quantity;
				}
		$ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', '');//$rowtc['tcost'];
		$ohistory_array_temp['free_quantity'] = $free_quantity;
		$ohistory_array_temp['sale_quantity'] = $sale_quantity;
		$ohistory_array_temp['quantity'] = $row['variantunit'];
		$ohistory_array_temp['orderid'] = $row['order_no'];
		$ohistory_array_temp['order_date'] = $row['order_date'];
		$ohistory_array_temp['shopid'] = $row['shop_id'];
		$ohistory_array_temp['shopnme'] = $row['shopname'];//$rowh['shopnme'];
		$ohistory_array_temp['offer_applied'] = $row['offer_provided_val'];
		$this->json_array['data']['ohistory'][]=$ohistory_array_temp;
		}
		}
		else{
			$this->json_array['status']['responsecode'] = '1';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['ohistory'][]="No records found";
		}
		return json_encode($this->json_array);
	}
	function get_campaigndata()
	{ 
		$sql_campaign = "SELECT `id` AS campaign_id, `campaign_name`, `campaign_description`, `campaign_start_date`, `campaign_end_date`, `campaign_type`, `status` FROM `tbl_campaign` WHERE deleted = 0 AND status =0 AND campaign_end_date >= '".date('Y-m-d')."'";
		
		$result_campaign = $this->executeQuery($sql_campaign);
	   
		while($row_campaign = mysql_fetch_array($result_campaign)) 
		{
			$campaign_array_temp['campaign_id'] = $row_campaign['campaign_id'];	
			$campaign_array_temp['campaign_name'] = $row_campaign['campaign_name'];
			$campaign_array_temp['campaign_description'] = $row_campaign['campaign_description'];
			$campaign_array_temp['campaign_start_date'] = $row_campaign['campaign_start_date'];
			$campaign_array_temp['campaign_end_date'] = $row_campaign['campaign_end_date'];
			$campaign_array_temp['campaign_type'] = $row_campaign['campaign_type'];
			
			$sql_campaign_area="SELECT `level`, `state_id`, `city_id`, `suburb_id`, `shop_id`, subarea_id FROM `tbl_campaign_area` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
			
			$result_campaign_area = $this->executeQuery($sql_campaign_area);
			$campaign_array_temp['campaign_area'] = array();
			$campaign_array_temp['campaign_product'] = array();
			$campaign_array_temp['campaign_product_weight'] = array();
			while($row_campaign_area = mysql_fetch_array($result_campaign_area)) {
					$campaign_array_temp['campaign_area']['level']  = $row_campaign_area['level'];	
								
				$sql_state="SELECT `name` FROM `tbl_state` WHERE id IN (".$row_campaign_area['state_id'].")";
				$result_state = $this->executeQuery($sql_state);
				$state_name_arr = array();
				
				while($row_state = mysql_fetch_array($result_state)){
					$state_name_arr[] = $row_state['name'];									
				}
				$state_name = implode(',',$state_name_arr);
				$campaign_array_temp['campaign_area']['state_id']  = $row_campaign_area['state_id'];
				$campaign_array_temp['campaign_area']['state_name']  = $state_name;					
								
				if($row_campaign_area['city_id'] != '')
				{
					$sql_city="SELECT `name` FROM `tbl_city` WHERE id IN (".$row_campaign_area['city_id'].")";
					$result_city = $this->executeQuery($sql_city);
					$city_name_arr = array();
					while($row_city = mysql_fetch_array($result_city)){
						$city_name_arr[] = $row_city['name'];									
					}
					$city_name = implode(',',$city_name_arr);
					$campaign_array_temp['campaign_area']['city_id']  = $row_campaign_area['city_id'];
					$campaign_array_temp['campaign_area']['city_name']  = $city_name;	
				} else {
					$campaign_array_temp['campaign_area']['city_id']  = "";
					$campaign_array_temp['campaign_area']['city_name']  = "";	
				}
				if($row_campaign_area['suburb_id'] != '')
				{
					$sql_suburb="SELECT `suburbnm` FROM `tbl_surb` WHERE id IN (".$row_campaign_area['suburb_id'].")";
					$result_suburb = $this->executeQuery($sql_suburb);
					$suburb_name_arr = array();
					while($row_suburb = mysql_fetch_array($result_suburb)){
						$suburb_name_arr[] = $row_suburb['suburbnm'];									
					}
					$suburb_name = implode(',',$suburb_name_arr);
					$campaign_array_temp['campaign_area']['suburb_id']  = $row_campaign_area['suburb_id'];
					$campaign_array_temp['campaign_area']['suburb_name']  = $suburb_name;	
				} else {
					$campaign_array_temp['campaign_area']['suburb_id']  = "";
					$campaign_array_temp['campaign_area']['suburb_name']  = "";	
				}
				
				// 
				if($row_campaign_area['subarea_id'] != '')
				{
					$sql_subarea="SELECT subarea_name,subarea_id FROM tbl_subarea WHERE subarea_id IN (".$row_campaign_area['subarea_id'].")";
					
					$result_subarea = $this->executeQuery($sql_subarea);
					$subarea_name_arr = array();
					
					while($row_subarea = mysql_fetch_array($result_subarea)) {
						$subarea_name_arr[] = $row_subarea['subarea_name'];									
					}
					
					$subarea_name = implode(',',$subarea_name_arr);
					
					$campaign_array_temp['campaign_area']['subarea_id']  = $row_campaign_area['subarea_id'];
					$campaign_array_temp['campaign_area']['subarea_name']  = $subarea_name;
					
				} else {
					$campaign_array_temp['campaign_area']['subarea_id']  = "";
					$campaign_array_temp['campaign_area']['subarea_name']  = "";	
				}
				
				if($row_campaign_area['shop_id'] != '')
				{
					$sql_shop="SELECT `name` FROM `tbl_shops` WHERE id IN (".$row_campaign_area['shop_id'].")";
					$result_shop = $this->executeQuery($sql_shop);
					$shop_name_arr = array();
					while($row_shop = mysql_fetch_array($result_shop)){
						$shop_name_arr[] = $row_shop['name'];									
					}
					$shop_name = implode(',',$shop_name_arr);
					$campaign_array_temp['campaign_area']['shop_id']  = $row_campaign_area['shop_id'];
					$campaign_array_temp['campaign_area']['shop_name']  = $shop_name;	
				}else{
					$campaign_array_temp['campaign_area']['shop_id']  = "";
					$campaign_array_temp['campaign_area']['shop_name']  = "";	
				}
			}
			
			switch($row_campaign['campaign_type']){
				case 'discount':				
						$sql_campaign_area_price="SELECT `product_price`, `discount_percent` FROM `tbl_campaign_area_price` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
						$result_campaign_area_price = $this->executeQuery($sql_campaign_area_price);	
						$i=1;
						$campaign_array_temp['area_price'] = array();
						while($row_campaign_area_price = mysql_fetch_array($result_campaign_area_price)){
							$record_discount = array();
							$record_discount['record_counter'] = $i;
							$record_discount['product_price'] = $row_campaign_area_price['product_price'];
							$record_discount['discount_percent'] = $row_campaign_area_price['discount_percent'];
							$campaign_array_temp['area_price'][]  = $record_discount;						
							$i++;
						}
					break;
				case 'free_product':
				
					$sql_campaign_free_product="SELECT `c_p_brand_id`, `c_p_category_id`, `c_product_id`, `c_p_quantity`, `c_p_measure`, `c_p_measure_id`,`c_p_quantity_measure`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`,`f_p_quantity_measure` FROM `tbl_campaign_product` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
					
					$result_campaign_free_product = $this->executeQuery($sql_campaign_free_product);
					$campaign_array_temp['area_price'] = array();
					$campaign_array_temp['campaign_product'] = array();
					$campaign_array_temp['campaign_product_weight'] = array();
					
					$j = 1;
					while($row_campaign_free_product = mysql_fetch_array($result_campaign_free_product)){
						$record_product = array();
						$record_product['record_counter'] = $j;
						$record_product['c_p_brand_id']  = $row_campaign_free_product['c_p_brand_id'];
						$record_product['c_p_brand_name']  = $this->getBrand($row_campaign_free_product['c_p_brand_id']);
						$record_product['c_p_category_id']  = $row_campaign_free_product['c_p_category_id'];
						$record_product['c_p_category_name']  = $this->getCategory($row_campaign_free_product['c_p_category_id']);
						$record_product['c_product_id']  = $row_campaign_free_product['c_product_id'];
						$record_product['c_product_name']  = $this->getProduct($row_campaign_free_product['c_product_id']);
						if($row_campaign_free_product['c_p_measure_id'] !='')
						{
							
							$sqlp="SELECT * from `tbl_product_variant` where id = ".$row_campaign_free_product['c_p_measure_id'];
							$result1p = $this->executeQuery($sqlp);
							$i = 1;
							while($row = mysql_fetch_array($result1p))
							{
								$exp_variant1 = $row['variant_1'];
								$imp_variant1= split(',',$exp_variant1);
								
								if($exp_variant1 != '')
								{
									$sql1p="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
									$result2p = $this->executeQuery($sql1p);
									$row_prd_variant1p = mysql_fetch_array($result2p);
									$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1p['id'];
									$combine1_display = "";
										
									$combine1_displayp = $row_prd_variant1p['unitname'] ;				
								}
							}
							
							$record_product['c_product_variant_wt_quantity']  = $row_campaign_free_product['c_p_quantity'];
							$record_product['c_product_variant_unit']  = $combine1_displayp;
							$record_product['c_product_variant']  = $row_campaign_free_product['c_p_quantity']." ".$combine1_displayp;
							$record_product['c_product_variant_unit_id']  = $row_campaign_free_product['c_p_measure_id'];
						}
						else
							$campaign_array_temp['campaign_product']['c_product_variant']  = "";
						
						$record_product['f_p_brand_id']  = $row_campaign_free_product['f_p_brand_id'];
						$record_product['f_p_brand_name']  = $this->getBrand($row_campaign_free_product['f_p_brand_id']);
						$record_product['f_p_category_id']  = $row_campaign_free_product['f_p_category_id'];
						$record_product['f_p_category_name']  = $this->getCategory($row_campaign_free_product['f_p_category_id']);
						$record_product['f_product_id']  = $row_campaign_free_product['f_product_id'];	
						$record_product['f_product_name']  = $this->getProduct($row_campaign_free_product['f_product_id']);
						if($row_campaign_free_product['f_p_measure_id'] !='')
						{
							$sql="SELECT * from `tbl_product_variant` where id = ".$row_campaign_free_product['f_p_measure_id'];
							$result1 = $this->executeQuery($sql);
							$i = 1;
							while($row = mysql_fetch_array($result1))
							{
								$exp_variant1 = $row['variant_1'];
								$imp_variant1= split(',',$exp_variant1);
								
								if($exp_variant1 != '')
								{
									$sql1="SELECT  unitname,id FROM `tbl_units` WHERE id=".$imp_variant1[1];
									$result2 = $this->executeQuery($sql1);
									$row_prd_variant1 = mysql_fetch_array($result2);
									$combine1 = $row['id']." ".$imp_variant1[0]." ".$row_prd_variant1['id'];
									$combine1_display = "";
										
									$combine1_display = $row_prd_variant1['unitname'] ;				
								}
							}
							$record_product['f_product_variant_wt_quantity']  = $row_campaign_free_product['f_p_quantity'];
							$record_product['f_product_variant_unit']  = $combine1_display;
							$record_product['f_product_variant']  = $row_campaign_free_product['f_p_quantity']." ".$combine1_display;
							$record_product['f_product_variant_unit_id']  = $row_campaign_free_product['f_p_measure_id'];
						}
						else
							$campaign_array_temp['campaign_product']['f_product_variant']  = "";		

						$campaign_array_temp['campaign_product'][] = $record_product;
						$j++;
					}
					break;
				case 'by_weight':				
					$sql_campaign_product_weight="SELECT `id`, `campaign_id`, `campaign_area_id`, `c_weight`, `c_unit`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`, `f_p_quantity_measure` FROM `tbl_campaign_product_weight` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
					
					$result_campaign_product_weight = $this->executeQuery($sql_campaign_product_weight);
					$campaign_array_temp['area_price'] = array();
					$campaign_array_temp['campaign_product'] = array();
					$campaign_array_temp['campaign_product_weight'] = array();
					$j = 1;
					while($row_campaign_product_weight = mysql_fetch_array($result_campaign_product_weight)){
						$record_product = array();
						$record_product['record_counter'] = $j;						
						$record_product['c_weight']  = $row_campaign_product_weight['c_weight'];	
						$record_product['c_unit']  = $row_campaign_product_weight['c_unit'];							
						
						$record_product['f_p_brand_id']  = $row_campaign_product_weight['f_p_brand_id'];
						$record_product['f_p_brand_name']  = $this->getBrand($row_campaign_product_weight['f_p_brand_id']);
						$record_product['f_p_category_id']  = $row_campaign_product_weight['f_p_category_id'];
						$record_product['f_p_category_name']  = $this->getCategory($row_campaign_product_weight['f_p_category_id']);
						$record_product['f_product_id']  = $row_campaign_product_weight['f_product_id'];
						$record_product['f_product_name']  = $this->getProduct($row_campaign_product_weight['f_product_id']);
						if($row_campaign_product_weight['f_p_measure_id'] !='')
						{
							$record_product['f_product_variant_wt_quantity']  = $row_campaign_product_weight['f_p_quantity'];
							$record_product['f_product_variant_unit']  = $row_campaign_product_weight['f_p_measure'];
							$record_product['f_product_variant']  = $row_campaign_product_weight['f_p_quantity']." ".$row_campaign_product_weight['f_p_measure'];
							$record_product['f_product_variant_unit_id']  = $row_campaign_product_weight['f_p_measure_id'];
						}
						else
							$campaign_array_temp['campaign_product_weight']['f_product_variant']  = "";									

						
						$campaign_array_temp['campaign_product_weight'][] = $record_product;
						$j++;
					}
					break;
			}	
			$data[]=$campaign_array_temp;
		}
		if(count($data) == 0)
			$data = [];
		
		return $data;
	}
	function getBrand($id){
		$sql_brand="SELECT `name` FROM `tbl_brand` WHERE id =".$id;
		$result_brand = $this->executeQuery($sql_brand);
		$row_brand = mysql_fetch_array($result_brand);
		return $row_brand['name'];
	}
	function getCategory($id){
		$sql_category="SELECT `categorynm` FROM `tbl_category` WHERE id =".$id;
		$result_category = $this->executeQuery($sql_category);
		$row_category = mysql_fetch_array($result_category);
		return $row_category['categorynm'];
	}
	function getProduct($id){
		$sql_product="SELECT `productname` FROM `tbl_product` WHERE id =".$id;
		$result_product = $this->executeQuery($sql_product);	
		$row_product = mysql_fetch_array($result_product);
		return $row_product['productname'];
	}
	function getVariantUnit($id){
		$sql_variant="SELECT `unitname` FROM `tbl_units` WHERE id =".$id;
		$result_variant = $this->executeQuery($sql_variant);
		$row_variant = mysql_fetch_array($result_variant);	
		return $row_variant['unitname'];
	}
	function get_subarea($suburbids){
		$sqlcity = "SELECT subarea_id,subarea_name,suburb_id FROM tbl_subarea WHERE suburb_id in ($suburbids) AND tbl_subarea.isdeleted !='1'";
		$proRowcity = $this->executeQuery($sqlcity);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '2';
		while($rowcity = mysql_fetch_array($proRowcity)) {
			$c_array_temp['subarea_name']	= $rowcity['subarea_name'];
			$c_array_temp['subarea_id'] 	= $rowcity['subarea_id'];
			$c_array_temp['suburb_id'] 	= $rowcity['suburb_id'];
			$this->json_array['data']['subarea'][]=$c_array_temp;
		}
		return json_encode($this->json_array);
	}
	
	function get_distributorlist($salespersonid) {
		
		  $sql = "SELECT external_id 
		  FROM tbl_user WHERE id = '".$salespersonid."'";
			
		  $proRow 		= $this->executeQuery($sql);
		  $row 			= mysql_fetch_array($proRow);//print_r($row );
		  /*$suburb_ids 	= $row["suburb_ids"];
		  $subarea_ids 	= $row["subarea_ids"];
		  $state 		= $row["state"];*/
		  $city 		= $row["city"];
		  $external_id 		= $row["external_id"];
		  
		  
		/*$sqlcity = "SELECT 
			`id`, 	 
			`firstname`	 	
		FROM 
			`tbl_user` 
		WHERE 
			user_type = 'Distributor' AND city= '$city'";
		$proRowcity = $this->executeQuery($sqlcity);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '2';
		if (mysql_num_rows($proRowcity) != 0) {
			while($rowcity = mysql_fetch_array($proRowcity)) {
				$c_array_temp['id']	= $rowcity['id'];
				$c_array_temp['firstname'] 	= $rowcity['firstname'];
				$this->json_array['data']['distributor'][]=$c_array_temp;
			}
		}else{*/
			$sqlstockist = "SELECT 
			`id`, 	 
			`firstname`	 	
			FROM 
				`tbl_user` 
			WHERE 
				user_type = 'Distributor' AND id IN ($external_id) AND isdeleted!='1' ";
			$proRowstockist = $this->executeQuery($sqlstockist);
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
			while($rowstockist = mysql_fetch_array($proRowstockist)) {
				$c_array_temp['id']	= $rowstockist['id'];
				$c_array_temp['firstname'] 	= $rowstockist['firstname'];
				$this->json_array['data']['distributor'][]=$c_array_temp;
			}
		//}
		return json_encode($this->json_array);
		
	}
	
	function fnsaveNoordertakenHistory($shop_id,$salesperson_id,$shop_close_reason_type,$shop_close_reason_details,$shop_visit_date_time ) {
		
		$sql="INSERT INTO tbl_shop_visit(shop_id,salesperson_id,shop_close_reason_type, shop_close_reason_details, shop_visit_date_time)VALUES('".$shop_id."' , '".$salesperson_id."' , '".$shop_close_reason_type."' , '".$shop_close_reason_details."' , '".$shop_visit_date_time."')";
		
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['leave']['msg'] = "No order taken history has been stored successfully.";
		
		$salespersonname = "";
		$sql = "SELECT firstname FROM  `tbl_user` WHERE id='".$salesperson_id ."'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$salespersonname = $row['firstname'];
		}
		
		$shopname = "";
		$sql = "SELECT name FROM  tbl_shops WHERE id='".$shop_id ."'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$shopname = $row['name'];
		}
		
		$subject = "No Order Recieved";
		$message = "Hi Admin,<br><br> Following shop not taked order.<br>";
		$message .= "<br><b>Salesperson name:</b> ". $salespersonname;
		$message .= "<br><b>Shop name:</b> ". $shopname;
		$message .= "<br><b>Reason:</b> ".$shop_close_reason_type;
		$message .= "<br><b>Reason details:</b> ". $shop_close_reason_details;
		$message .= "<br><br>BR, </br>".COMPANYNM." Team";
		
		//$FROMMAILID = FROMMAILID;
		$FROMMAILID = "arudhraz@admin.com";
		//$headers = 'From: '. $FROMMAILID  .'\r\n Reply-To: ' . $FROMMAILID;
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <'.$FROMMAILID.'>' . "\r\n";
		//$headers .= 'Cc: myboss@example.com' . "\r\n";

		$to = "arudhraz@admin.com";
		@mail($to , $subject , $message , $headers);
		return json_encode($this->json_array);
	}
	function save_userlocation($userid,$latt,$longg) {
		$last_id=0;
		 $sql_point_check="select id from `tbl_user_location` where userid='$userid' and lattitude='$latt' and longitude='$longg' and DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW()) ORDER BY id DESC LIMIT 1";
		$proRowp = $this->executeQuery($sql_point_check);
		if(mysql_num_rows($proRowp)!=0){	
			$this->json_array['data']['Location']['id'] = $last_id;
		}else{
			if($latt!='0.0'&&$longg!='0.0'){
				$sql="INSERT INTO tbl_user_location(userid, lattitude, longitude,tdate)VALUES('".$userid."','".$latt."','".$longg."',now())";
				$proRow = $this->executeQuery($sql);
				$last_id = mysql_insert_id();
				$this->json_array['data']['Location']['id'] = $last_id;
			}
		}
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Location']['msg'] = "Location added successfully.";
		
		return json_encode($this->json_array);
	}
	//count for images TADA bill
	function get_sptravel_count($data) {
		$userid =$data['userid'];
		$sqlfordistance = "SELECT max(id) as maxid FROM  tbl_sp_tadabill WHERE userid='".$userid ."' and date_format(date_tada,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
		$proRowdist = $this->executeQuery($sqlfordistance);
		$rowAttendance = mysql_fetch_assoc($proRowdist);
		return $rowAttendance['maxid'];
	}
	function save_sptravel($data) {
		$userid =$data['userid'];
		$distance_covered =$data['distance_covered'];
		$mode_of_transe =$data['mode_of_transe'];
		$food =$data['food'];
		$other =$data['other'];
		$sqlfordistance = "SELECT 	id,lattitude,longitude FROM  tbl_user_location WHERE userid='".$userid ."' and date_format(tdate,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
		$proRowdist = $this->executeQuery($sqlfordistance);
		$countpoints=mysql_num_rows($proRowdist);
		$countrec=0;
		if (mysql_num_rows($proRowdist) > 0) {
			while($rowc = mysql_fetch_array($proRowdist)) {
				$c_array_temp[$countrec]['id'] = $rowc['id'];
				$c_array_temp[$countrec]['lattitude'] = $rowc['lattitude'];
				$c_array_temp[$countrec]['longitude'] = $rowc['longitude'];
				$countrec++;
			}
		}
		//echo "".$countpoints."<pre>";print_r($c_array_temp);
		 $google_distance=0;
		for($i=0;$i<$countpoints-1;$i++){
			$google_d=$this->GetDrivingDistance($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
			//echo "<pre>";print_r($google_d);
			$google_distance=$google_distance+$google_d['distance'];
		} 
		$google_distance=bcdiv($google_distance, 1000, 3);
		
		 $sqlforrate="SELECT rupees_per_km from tbl_mode_transe where id='$mode_of_transe'";
		$prorateRow = $this->executeQuery($sqlforrate);$rupees_per_km=0;
		while($rowc = mysql_fetch_array($prorateRow)) {
			$rupees_per_km = $rowc['rupees_per_km'];
		}
		  $sql="INSERT INTO tbl_sp_tadabill(userid, distance_covered,google_distance, mode_of_transe,food,other,date_tada,Current_rate_mot)VALUES
		('".$userid."','".$distance_covered."','".$google_distance."','".$mode_of_transe."','".$food."','".$other."',now(),'".$rupees_per_km."')";
		//,Current_rate_mot
		//,'".$rupees_per_km."'
		$proRow = $this->executeQuery($sql);
		$last_id = mysql_insert_id();
		$this->json_array['data']['Travel']['msg'] = "Expense bill submitted successfully.";
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Travel']['id'] = $last_id;
		return json_encode($this->json_array);
		/* $userid =$data['userid'];
		$kmreading =$data['kmreading'];
		$travelmode =$data['travelmode'];
		$cost =$data['cost'];
		$sql="INSERT INTO tbl_sp_travel(userid, km_reading, travel_mode,cost,tdate)VALUES('".$userid."','".$kmreading."','".$travelmode."','".$cost."',now())";
		$proRow = $this->executeQuery($sql);
		$last_id = mysql_insert_id();
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Travel']['msg'] = "Travelling added successfully.";
		$this->json_array['data']['Travel']['id'] = $last_id;
		return json_encode($this->json_array); */
	}
	
	
	function get_userlocation($userid){
		$sql="select id,userid,lattitude,longitude from tbl_user_location where userid='$userid'";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '2';
		$countt=0;
		while($rowc = mysql_fetch_array($proRow)) {
						
						$c_array_temp['id']        = $rowc['id'];
						$c_array_temp['userid'] = $rowc['userid'];
						$c_array_temp['lattitude']           = $rowc['lattitude'];
						$c_array_temp['longitude']         = $rowc['longitude'];
						$this->json_array['data']['location'][]=$c_array_temp;                    
						$countt++;
		}
		$this->json_array['data']['sdpoint'][]=$this->json_array['data']['location'][0];
		$this->json_array['data']['sdpoint'][]=$this->json_array['data']['location'][$countt-1];
		for($i=0;$i<$countt;$i++){
						
						$this->json_array['data']['location'][$i]['latd']=$this->json_array['data']['location'][$i+1]['lattitude'];
						$this->json_array['data']['location'][$i]['longgd']=$this->json_array['data']['location'][$i+1]['longitude'];
		}
		$this->json_array['data']['location'][$countt-1]['latd']=$this->json_array['data']['location'][$countt-1]['lattitude'];
		$this->json_array['data']['location'][$countt-1]['longgd']=$this->json_array['data']['location'][$countt-1]['longitude'];
		
		
		return json_encode($this->json_array);                 
	}
	//new service for shop edit
	function fnEditStore($salespersonid,$store_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$suburbid,$subarea_id,$shopid) {
		/*Get Stockist id */
		$sql_shop = " SELECT 
		 `stockist_id` 	
		FROM  `tbl_shops` 		
		WHERE id='$shopid'";
		
		$shop_result = $this->executeQuery($sql_shop);
		$shop_details = mysql_fetch_array($shop_result);
		$distributorid = $shop_details['stockist_id'];
		/*Get Stockist's State, District, Taluka assign to shop*/
		$sql_wa = " SELECT 
		 `state_ids`, `city_ids`, `suburb_ids`, `subarea_ids` 	
		FROM  `tbl_user_working_area` 		
		WHERE user_id='$distributorid'";
		
		$user_wa = $this->executeQuery($sql_wa);
		$user_wa_details = mysql_fetch_array($user_wa);
		$state = $user_wa_details['state_ids'];
		$city = $user_wa_details['city_ids'];
		$suburbid = $user_wa_details['suburb_ids'];
		$subarea_id = $user_wa_details['subarea_ids'];
		
		$sql ="UPDATE tbl_shops SET 
		name='$store_name',
		address='$store_Address',
		state='$state',
		city='$city',
		mobile='$contact_no',
		shop_added_by='$salespersonid',
		latitude='$lat',                   
		longitude='$lng',
		suburbid='$suburbid',
		subarea_id='$subarea_id'
		where id='$shopid'";                      
		
		$proRow = $this->executeQuery($sql);
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Store']['msg'] = "Store updated successfully.";
		return json_encode($this->json_array);
	}

	function checkAttendance($id){
		$sql = "SELECT 
			count(sp_id) AS count, 	dayendtime
			FROM 
				`tbl_sp_attendance` 
			WHERE 
				sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '".date('d-m-Y')."'";
			$resultAttendance = $this->executeQuery($sql);
			if (mysql_num_rows($resultAttendance) != 0) {
				$rowAttendance = mysql_fetch_assoc($resultAttendance);
				return $rowAttendance;
			}else{
				return 0;
			}
	}
	function addAttendance($id){
		$attendance_count = $this->checkAttendance($id);
		if($attendance_count['count'] == 0){
			$sql="INSERT INTO tbl_sp_attendance(sp_id, tdate,presenty) VALUES ( '".$id."',now(),'1' )";
			$proRow = $this->executeQuery($sql);
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day Start Time added successfully.";
		}else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day Start Time already exist.";
		}
		return json_encode($this->json_array);
	}
	function addDayEndTime($id){
		$attendance_count = $this->checkAttendance($id);
		if($attendance_count['count'] != 0 && $attendance_count['dayendtime'] == ''){
			$sql="UPDATE tbl_sp_attendance SET dayendtime = now() WHERE sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '".date('d-m-Y')."'";
			$proRow = $this->executeQuery($sql);
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day End Time added successfully.";			
		}else if($attendance_count['dayendtime'] != ''){
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day End Time already exist.";
		}else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Please add day start time.";
		}
		return json_encode($this->json_array);
	}
	public function getOrders($order_no){		
		
		$where_clause_outer ='';
		
		if($order_no != ''){
			$where_clause_outer .= " AND o.order_no = '".$order_no."' ";
		}
		
		 $order_sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE 1=1 $where_clause_outer
		ORDER BY shop_name, o.order_date DESC";//o.shop_order_status = ".$order_status."
		
		$orders_result = $this->executeQuery($order_sql);
		$row_orders_count = mysql_num_rows($orders_result);
		$i = 1;
		$all_orders = array();
		$row_orders_det_count = 0;
		if($row_orders_count > 0){
			while($row_orders = mysql_fetch_assoc($orders_result))
			{
				$all_orders[$i] = $row_orders;
				 $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				odis.discount_amount
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				WHERE od.order_id = ".$row_orders['id']." ";//AND od.order_status = ".$order_status." 
				$orders_det_result = $this->executeQuery($order_details_sql);
				//$row_orders_det_count = mysqli_num_rows($orders_det_result);
				if(mysql_num_rows($orders_det_result) > 0)
				{
					$j = 1;
					while($row_orders_det = mysql_fetch_assoc($orders_det_result))
					{
						$all_orders[$i]['order_details'][$j] = $row_orders_det;
						$j++;
					}
					$i++;
				}
				else
				{
					unset ($all_orders[$i]); 
				}
			}
		}
		//print"<pre>";
		//print_r($all_orders);
		
		if(count($all_orders) == 0){
			$all_orders = array();
			return $all_orders;
		}
		else
			return $all_orders;
	}
function getorderdetailsbyordernumber($ordernum){
	//echo "<pre>";print_r($ordernum);die();
	$this->json_array['status']['responsecode'] = '0';
	$this->json_array['status']['entity'] = '9';
	$result1 = $this->getOrders($ordernum);
		//echo "<pre>";print_r(count($result1));die();
		$ohistory_array_temp1=array();
		//$result1 = mysqli_query($con,$sql);
			if(count($result1)>0){                                  
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '9';
				foreach($result1 as $key=>$value){
					foreach($value['order_details'] as $key1=>$value1){
						$ohistory_array_temp['product_varient_id']= $value1['product_variant_id'];
						$ohistory_array_temp['distributorid']='';
						$ohistory_array_temp['distributornm']='';
						$ohistory_array_temp['salespersonnm']=$value['order_by_name'];
						
						$ohistory_array_temp['orderid']=$value['id'];
						$ohistory_array_temp['ordernum']=$value['order_no'];
						
						$ohistory_array_temp['variant_oder_id']='';
						$ohistory_array_temp['productname']=$value1['product_name']." ".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'];
						if(!empty($value1['product_variant_weight2'])){
							$ohistory_array_temp['productname'].=" ".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'];
						}
						
						$ohistory_array_temp['order_date']=date('d-m-Y H:i:s',strtotime($value['order_date']));
						$ohistory_array_temp['unitprice']=number_format($value1['product_unit_cost'],2, '.', '');
						$ohistory_array_temp['quantity']=$value1['product_quantity'];
						$total_cost=$ohistory_array_temp['unitprice']*$ohistory_array_temp['quantity'];
						$ohistory_array_temp['totalcost']=number_format($total_cost,2, '.', '');
						$ohistory_array_temp['shop_id']=$value['shop_id'];
						$ohistory_array_temp['shopnm']=$value['shop_name'];
						
						$ohistory_array_temp['catnm']=$value1['cat_name'];
						$ohistory_array_temp['brandnm']=$value1['brand_name'];
						
						$ohistory_array_temp['totalproductcount']='';
						$ohistory_array_temp1[] = $ohistory_array_temp;
					}
					
				}
				if(!empty($ohistory_array_temp1))                                                                         
				$this->json_array['data']['orderdetails']=$ohistory_array_temp1;
											
			}else{
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['ohistory'][]="No records found";
		}
		return json_encode($this->json_array);
	}
	function get_distributorlist_by_ssid($ssid) {  
			$sqlstockist = "SELECT 
			`id`, 	 
			`firstname`	 	
			FROM 
				`tbl_user` 
			WHERE 
				user_type = 'Distributor' AND external_id =$ssid";
			$proRowstockist = $this->executeQuery($sqlstockist);
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
			while($rowstockist = mysql_fetch_array($proRowstockist)) {
				$c_array_temp['id']	= $rowstockist['id'];
				$c_array_temp['firstname'] 	= $rowstockist['firstname'];
				$this->json_array['data']['distributor'][]=$c_array_temp;
			}
		
		return json_encode($this->json_array);
		
	}
	function get_order_and_orderdetails($userid, $order_status){
		  $order_sql = "SELECT o.id as oid, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE o.shop_order_status = '1' and  o.delivery_person_id=".$userid."
		ORDER BY shop_name, o.order_date DESC";
		
		$orders_result = $this->executeQuery($order_sql);
		$row_orders_count = mysql_num_rows($orders_result);
		//$i = 1;
		$all_orders = array();
		$row_orders_det_count = 0;
		if($row_orders_count > 0){
			while($row_orders = mysql_fetch_assoc($orders_result))
			{
				$where_cond="";
				if (strpos($order_status, ',') !== false) {
					$where_cond.="  AND od.order_status in ('3','4') ";
				}else{
					$where_cond.="  AND od.order_status = '".$order_status."' ";
				}
				//$all_orders[$i] = $row_orders;
				   $order_details_sql = "SELECT od.id as odid, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date 
				FROM `tbl_order_details` AS od
				WHERE od.order_id = ".$row_orders['oid']." $where_cond";
				//die();
				
				$orders_det_result = $this->executeQuery($order_details_sql);
				$row_orders_det_count = $row_orders_det_count+ mysql_num_rows($orders_det_result);
				//echo $row_orders_det_count ;
				if(mysql_num_rows($orders_det_result) > 0){
					//$j = 1;
					while($row_orders_det = mysql_fetch_assoc($orders_det_result))
					{
						$newarr[]=array_merge ($row_orders_det, $row_orders);
						//$all_orders[$i]['order_details'][] = $newarr;
					}
				}
			}
		}
		//echo $row_orders_det_count ;
		//echo "<pre>";print_r($newarr);die();
		
		if($row_orders_det_count == 0){
			$all_orders = array();
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';	
			$this->json_array['data']['orderdetails'][]="No records";
		}
		else{
			foreach ($newarr as $key => $value) {			
				foreach ($value as $key1 => $value1) {
					if (is_null($value1)) {
						 $newarr[$key][$key1] = "";
					}
				}
			} 
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '9';
			$this->json_array['data']['orderdetails']=$newarr;
		}
		return json_encode($this->json_array);
		
	}
	function cash_on_delivery($data){		
		$count=0;
		if($data->cod_type==1){
			$cod_percent=0;
			$sql1="SELECT cod_percent FROM tbl_cod_admin where status = 'active'";
			$result = $this->executeQuery($sql1);
			$row_count = mysql_num_rows($result);			
			if($row_count > 0){
				$cod_percents = mysql_fetch_assoc($result);
				$cod_percent=$cod_percents['cod_percent'];
			}
			foreach($data->order_details as $key=>$value){	
				$count++;
				$order_id=$value->orderid;
				
				$sql2="SELECT id as oid FROM tbl_orders where order_no='$order_id' ";
				$resultoid = $this->executeQuery($sql2);
				$row_countoid = mysql_num_rows($resultoid);			
				if($row_countoid > 0){
					$oidasdh = mysql_fetch_assoc($resultoid);
					$oid=$oidasdh['oid'];
				}
				
				$sql ="UPDATE tbl_orders SET c_o_d='1',cod_percent='$cod_percent' where order_no='$order_id'";  
				$proRow = $this->executeQuery($sql);
				//tbl_order_details
				$sql ="UPDATE tbl_order_details SET order_status='4' where order_id='$oid'";  
				$proRow = $this->executeQuery($sql);
			}
		}		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		if($count>0){
			$this->json_array['data']['Delivery']['msg'] = "Cash on delivery updated successfully.";
		}else{
			$this->json_array['data']['Delivery']['msg'] = "Cash on delivery not updated.";
		}
		return json_encode($this->json_array);
	}
	function save_sp_todaystravel($data) {
		$userid =$data['userid'];
		$flag =$data['flag'];
		$comments =$data['comments'];
		$user_type =$data['user_type'];
		$visit_to_id =$data['visit_to_id'];//shop/ Trasnport office
		
		$sql = "SELECT * from tbl_sp_attendance WHERE sp_id='".$userid ."' 
		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
		$proRow = $this->executeQuery($sql);
		$c_array_temp=array();
		while($rowc = mysql_fetch_array($proRow)) {
			$c_array_temp['dayendtime']   = $rowc['dayendtime'];
			$c_array_temp['daystarttime'] = $rowc['daystarttime'];			
		}
		if (mysql_num_rows($proRow) == 0) {
				$this->json_array['data']['Track']['msg'] = "Day not started yet.";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = '';
				return json_encode($this->json_array);
		}else if($c_array_temp['dayendtime']!== NULL){
				$this->json_array['data']['Track']['msg'] = "Day already ended.";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = '';
				return json_encode($this->json_array);
		}else{
			if($user_type == 'SalesPerson' || $user_type == ''){
				$sql="INSERT INTO tbl_shop_visit(salesperson_id, shop_visit_date_time, flag, comments, is_shop_location)VALUES
				('".$userid."',now(),'".$flag."','".$comments."','1')";
				$proRow = $this->executeQuery($sql);
				$last_id = mysql_insert_id();
				$this->json_array['data']['Track']['msg'] = "Location track ".$flag.".";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = $last_id;
			}else if($user_type == 'DeliveryPerson'){
				$sql="INSERT INTO tbl_transport_off_visit(`transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`, `comments`)
				VALUES
				('".$visit_to_id."', '".$userid."',now(),'".$flag."','".$comments."','1')";
				$proRow = $this->executeQuery($sql);
				$last_id = mysql_insert_id();
				$this->json_array['data']['Track']['msg'] = "Location track ".$flag.".";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = $last_id;
			}
			return json_encode($this->json_array);
		}
	}
	function get_sp_pause_resume($userid, $user_type){
		$sql = "SELECT * from tbl_sp_attendance WHERE sp_id='".$userid ."' 
		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
		$proRow = $this->executeQuery($sql);
		$c_array_temp=array();
		while($rowc = mysql_fetch_array($proRow)) {
			$c_array_temp['dayendtime']   = $rowc['dayendtime'];
			$c_array_temp['daystarttime'] = $rowc['daystarttime'];			
		}
		if (mysql_num_rows($proRow) == 0||$c_array_temp['dayendtime']!== NULL) {
				$this->json_array['data']['flag'] = "2";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				return json_encode($this->json_array);
		}else{
			if($user_type == 'SalesPerson' || $user_type == ''){
				$sql="SELECT salesperson_id, shop_visit_date_time, flag, comments, is_shop_location 
				FROM tbl_shop_visit where salesperson_id='".$userid."' and
				is_shop_location = '1' 
				and date_format(shop_visit_date_time, '%d-%m-%Y') = '".date('d-m-Y')."'";
				$proRow = $this->executeQuery($sql);
				$count_resume=mysql_num_rows($proRow);
				$newcount=$count_resume%2;
				$this->json_array['data']['flag'] = (string)$newcount;
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
			}else if($user_type == 'DeliveryPerson'){
				$sql="SELECT `transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`
				FROM tbl_transport_off_visit where deliveryperson_id='".$userid."'
				and date_format(transport_off_visit_date_time, '%d-%m-%Y') = '".date('d-m-Y')."'";
				$proRow = $this->executeQuery($sql);
				$count_resume=mysql_num_rows($proRow);
				$newcount=$count_resume%2;
				$this->json_array['data']['flag'] = (string)$newcount;
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
			}
			return json_encode($this->json_array);
		}
	}
	function get_shop_type(){
		$sql = "SELECT * FROM tbl_shop_type WHERE status='0'";
		$proRow = $this->executeQuery($sql);
		$shop_type_array=array();
		if (mysql_num_rows($proRow) == 0) {
				$this->json_array['data']['flag'] = "2";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				return json_encode($this->json_array);
		}else{
			while($rowshoptype = mysql_fetch_array($proRow)) {
				$shop_type_array['id']				= $rowshoptype['id'];
				$shop_type_array['shop_type'] 		= $rowshoptype['shop_type'];
				$this->json_array['data']['shop_type'][]=$shop_type_array;
			}
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			return json_encode($this->json_array);
		}
	}
	function get_mode_of_transe(){
		$sqlcity = "SELECT id, van_type FROM  tbl_mode_transe";
	  $proRowcity = $this->executeQuery($sqlcity);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '3';
	   while($rowcity = mysql_fetch_array($proRowcity)){
		  $c_array_temp['id'] = $rowcity['id'];
		  $c_array_temp['van_type'] = $rowcity['van_type'];
		  $this->json_array['data']['mot'][]=$c_array_temp;
	  }
	  return json_encode($this->json_array);
	}

}